import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AuthService } from '@services/auth/auth.service';
import { User } from '@models/user';

@Component({
  selector: 'app-account-container',
  templateUrl: './account-container.component.html',
  styleUrls: ['./account-container.component.css']
})
export class AccountContainerComponent implements OnInit {
  childRoute = 'dashboard';
  title = 'You are logged in as ';
  
  user: User = null;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // console.log('Account module loaded');
    this.user = this.authService.getUser();
  }

}
