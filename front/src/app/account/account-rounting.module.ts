import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from '@services/auth-guard/auth-guard.service';

import { DashboardComponent } from '@account/dashboard/dashboard.component';
import { ProfileComponent } from '@account/profile/profile.component';
import { AccountContainerComponent } from '@account/account-container/account-container.component';
import { AccountLessonsComponent } from '@account/account-lessons/account-lessons.component';
import { StreamComponent } from '@stream/stream/stream.component';
import { BroadcastComponent } from '@stream/broadcast/broadcast.component';
import { ClassroomComponent } from '@stream/classroom/classroom.component';
import { CanDeactivateGuard } from '@services/can-deactivate/can-deactivate.guard';

const accountRoutes: Routes = [
    {
        path: 'account',
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                component: AccountContainerComponent,
                canActivateChild: [AuthGuardService],
                children: [
                    {
                        path: '',
                        redirectTo: 'dashboard',
                        pathMatch: 'full'
                    },
                    {
                        path: 'dashboard',
                        component: DashboardComponent
                    },
                    {
                        path: 'profile',
                        component: ProfileComponent
                    },
                    {
                        path: 'my-lessons',
                        component: AccountLessonsComponent
                    },
                    {
                        path: 'classroom/:lessonSlug',
                        component: ClassroomComponent,
                        canActivate: [AuthGuardService],
                        canDeactivate: [CanDeactivateGuard]
                        // component: BroadcastComponent
                    },
                    {
                        path: 'broadcast',
                        component: BroadcastComponent
                    }
                ]
            }
        ]
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(accountRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule {}
