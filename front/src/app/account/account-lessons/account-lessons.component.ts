import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';

import { AuthService } from '@services/auth/auth.service';
import { LessonService } from '@services/lesson/lesson.service';
import { StudentService } from '@services/student/student.service';
import { InstructorService } from '@services/instructor/instructor.service';

import { Enrolment } from '@models/enrolment';
import { Student } from '@models/student';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';

import { DateTimeHelper } from '@helpers/date-time-helper';
import { User } from '@models/user';


@Component({
  selector: 'app-account-lessons',
  templateUrl: './account-lessons.component.html',
  styleUrls: ['./account-lessons.component.css']
})
export class AccountLessonsComponent implements OnInit {
  enrolments: Enrolment[] = [];
  user: User = null;
  student: Student = null;
  instructor: Instructor = null;
  lessons: Lesson[] = [];
  lessonSlug: string;
  loading = true;
  dt = new DateTimeHelper();

  constructor(
      public authService: AuthService, private lessonService: LessonService,
      private studentService: StudentService,
      private instructorService: InstructorService, private router: Router,
      private route: ActivatedRoute) {}

  async ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();
      try {
        if (this.user.type === 'learner') {
          let student =
              this.studentService.getStudentById(this.user.id).toPromise();
          let enrolments =
            this.studentService.getEnrolments(this.user.id).toPromise();

          this.student = await student;
          this.enrolments = await enrolments;

          this.enrolments.forEach((enrolment) => {
            this.lessons.push(enrolment.lesson);
          });

          this.loading = false;

        } else if (this.user.type === 'instructor') {
          let instructor = this.instructorService.getInstructorById(this.user.id).toPromise();
          let lessons =
              this.instructorService.getInstructorLessons(this.user.id)
                  .toPromise();

          this.instructor = await instructor;
          this.lessons = await lessons;

          this.loading = false;
        
        } else {
          swal({ title: 'Access Denied!', type: 'error', timer: 3000 }).then(() => {
            this.router.navigate(['/']);
          });
        }
      } catch (err) {
        console.error(err);
        this.router.navigate(['/account/dashboad']);
      }
  }

}
