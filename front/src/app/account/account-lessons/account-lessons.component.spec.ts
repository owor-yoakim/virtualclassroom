import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountLessonsComponent } from './account-lessons.component';

describe('AccountLessonsComponent', () => {
  let component: AccountLessonsComponent;
  let fixture: ComponentFixture<AccountLessonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountLessonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountLessonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
