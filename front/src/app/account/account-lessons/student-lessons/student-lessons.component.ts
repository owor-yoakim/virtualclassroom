import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DateTimeHelper } from '@helpers/date-time-helper';

import { Student } from '@models/student';
import { StudentService } from '@services/student/student.service';
import { Lesson } from '@models/lesson';
import { Enrolment } from '@models/enrolment';



@Component({
  selector: 'app-student-lessons',
  templateUrl: './student-lessons.component.html',
  styleUrls: ['./student-lessons.component.css']
})
export class StudentLessonsComponent implements OnInit {
  @Input('student') student: Student;
  @Input('lessons') lessons: Lesson[];
  @Input('enrolments') enrolments: Enrolment[];
  
  dt = new DateTimeHelper();

  constructor(
    private studentService: StudentService,
    private router: Router
  ) { }

  ngOnInit() {
  }

}
