import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';
import { Student } from '@models/student';
import { User } from '@models/user';
import { Base64 } from 'js-base64';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'lang_14';
  loading = false;
  student: User = null;

  constructor(
    public authService: AuthService,
    public studentService: StudentService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;

    this.student = this.authService.getUser();

    this.loading = false;

  }

}
