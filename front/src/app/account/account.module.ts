import {AccountContainerComponent} from '@account/account-container/account-container.component';
import {AccountLessonsComponent} from '@account/account-lessons/account-lessons.component';
import {StudentLessonsComponent} from '@account/account-lessons/student-lessons/student-lessons.component';
import {AccountRoutingModule} from '@account/account-rounting.module';
import {ChangePasswordComponent} from '@account/change-password/change-password.component';
import {DashboardComponent} from '@account/dashboard/dashboard.component';
import {InstructorProfileComponent} from '@account/profile/instructor-profile/instructor-profile.component';
import {ProfileComponent} from '@account/profile/profile.component';
import {StudentProfileComponent} from '@account/profile/student-profile/student-profile.component';
import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {InstructorModule} from '@instructor/instructor.module';
import {SharedModule} from '@shared/shared.module';
import {TranslateModule} from '@translate/translate.module';
import {FlashMessageModule} from 'angular-flash-message';



@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    AccountRoutingModule,
    InstructorModule
  ],
  declarations: [
    ProfileComponent,
    ChangePasswordComponent,
    DashboardComponent,
    AccountContainerComponent,
    AccountLessonsComponent,
    StudentLessonsComponent,
    StudentProfileComponent,
    InstructorProfileComponent
  ],
  exports: [
    ProfileComponent,
    ChangePasswordComponent,
    DashboardComponent,
    StudentLessonsComponent,
    StudentProfileComponent,
    InstructorProfileComponent
  ]
})
export class AccountModule { }
