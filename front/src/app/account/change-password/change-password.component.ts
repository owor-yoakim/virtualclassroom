import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {passwordsMatcher} from '@helpers/passwords-matcher';
import {Student} from '@models/student';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {StudentService} from '@services/student/student.service';
import swal from 'sweetalert2';
import { Instructor } from '@models/instructor';
import { InstructorService } from '@services/instructor/instructor.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  @Input('student') student: Student = null;
  @Input('instructor') instructor: Instructor = null;

  changePasswordForm: FormGroup;
  loading = false;

  oldPassword = '';
  newPassword = '';
  
  constructor(
    public authService: AuthService,
    private fb: FormBuilder,
    private studentService: StudentService,
    private instructorService: InstructorService,
    private router: Router
    ) { }

  ngOnInit() {
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: this.fb.group(
          {
            password1: ['', [Validators.required]],
            password2: ['', [Validators.required]]
          },
          {validator: passwordsMatcher})
    });
  }

  async changePassword() {
    try {
      let response;
      if (this.student !== null) {
        response = await this.studentService
          .changePassword(this.student, this.oldPassword, this.newPassword)
          .toPromise();
        console.log(response);
        await swal({ title: 'Response Status', text: response, type: 'success' });
      } else if (this.instructor !== null) {
        response = await this.instructorService
          .changePassword(this.instructor, this.oldPassword, this.newPassword)
          .toPromise();
        console.log(response);
        await swal({ title: 'Response Status', text: response, type: 'success' });
      } else {
        await swal({title: 'Response Status', text: 'Invalid User', type: 'warning'});
      }
    } catch (error) {
      console.error(error);
      await swal({ title: 'Response Status', text: error.error, type: 'warning' });
    }
    this.changePasswordForm.reset();
    this.oldPassword = '';
    this.newPassword = '';
  }
  
}
