import { Component, OnInit, Input } from '@angular/core';
import { Instructor } from '@models/instructor';

@Component({
  selector: 'app-instructor-profile',
  templateUrl: './instructor-profile.component.html',
  styleUrls: ['./instructor-profile.component.css']
})
export class InstructorProfileComponent implements OnInit {
  @Input('instructor') instructor: Instructor;

  constructor() { }

  ngOnInit() {
  }

}
