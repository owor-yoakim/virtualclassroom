import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { StudentService } from '@services/student/student.service';
import { AuthService } from '@services/auth/auth.service';
import { InstructorService } from '@services/instructor/instructor.service';
import { User } from '@models/user';
import { Student } from '@models/student';
import { Enrolment } from '@models/enrolment';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loading = false;
  student: Student = null;
  user: User = null;
  instructor: Instructor = null;
  enrolments: Enrolment[] = [];
  lessons: Lesson[] = [];

  constructor(
    public authService: AuthService,
    private studentService: StudentService,
    private instructorService: InstructorService,
    private router: Router
  ) { }

  async ngOnInit() {
    this.loading = true;

    this.user = this.authService.getUser();

    try {
      if (this.user.type === 'learner') {
        let student = this.studentService.getStudentById(this.user.id).toPromise();
        let enrolments =
            this.studentService.getEnrolments(this.user.id).toPromise();

        this.student = await student;
        this.enrolments = await enrolments;

        this.enrolments.forEach((enrolment) => {
          this.lessons.push(enrolment.lesson);
        });

        this.loading = false;

      } else if (this.user.type === 'instructor') {
        let instructor = this.instructorService.getInstructorById(this.user.id).toPromise();
        let lessons = this.instructorService.getInstructorLessons(this.user.id)
                          .toPromise();

        this.instructor = await instructor;
        this.lessons = await lessons;

        this.loading = false;
        
      } else {
        swal({ title: 'Access Denied!', type: 'error', timer: 3000 }).then(() => {
          this.router.navigate(['/']);
        });
      }
    } catch (err) {
      console.error(err);
      this.router.navigate(['/account/dashboad']);
    }


  }

  changePassword() {

  }

}
