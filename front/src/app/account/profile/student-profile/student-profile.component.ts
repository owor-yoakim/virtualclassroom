import { Component, OnInit, Input } from '@angular/core';
import { Student } from '@models/student';

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.css']
})
export class StudentProfileComponent implements OnInit {
  @Input('student') student: Student;

  constructor() { }

  ngOnInit() {
  }

}
