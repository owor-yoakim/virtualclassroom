import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SettingService } from '@services/setting/setting.service';
import { PageService } from '@services/page/page.service';
import { Page } from '@models/page';





@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  title = 'lang_66';
  page: Page = null;
  loading = false;

  constructor(
    public settingService: SettingService,
    private pageService: PageService,
    private router: Router
  ) { }

  async ngOnInit() {
    this.loading = true;
    try {
      this.page = await this.pageService.getPage('about-us').toPromise();
      this.loading = false;
    } catch (err) {
      console.error(err);
      this.loading = false;
      // this.router.navigateByUrl('/');
    }
  }


}
