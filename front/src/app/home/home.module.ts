import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@translate/translate.module';
import { HomeRoutingModule } from '@home/home-rounting.module';

import { AboutComponent } from '@home/about/about.component';
import { IndexComponent } from '@home/index/index.component';
import { NotFoundComponent } from '@home/not-found/not-found.component';
import { LoginComponent } from '@home/login/login.component';
import { SignupComponent } from '@home/signup/signup.component';
import { ResetPasswordComponent } from '@home/reset-password/reset-password.component';
import { StudentLoginComponent } from '@home/login/student-login/student-login.component';
import { InstructorLoginComponent } from '@home/login/instructor-login/instructor-login.component';


@NgModule({
  imports: [CommonModule, TranslateModule, SharedModule, HomeRoutingModule],
  declarations: [
    IndexComponent, AboutComponent, NotFoundComponent, LoginComponent,
    StudentLoginComponent, InstructorLoginComponent, SignupComponent,
    ResetPasswordComponent
  ],
  exports: [
    IndexComponent, AboutComponent, LoginComponent, StudentLoginComponent,
    InstructorLoginComponent, SignupComponent, ResetPasswordComponent,
    NotFoundComponent
  ]
})
export class HomeModule { }
