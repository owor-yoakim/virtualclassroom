import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';

import swal from 'sweetalert2';

import { AuthService } from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';

import { passwordsMatcher } from '@helpers/passwords-matcher';
import { Student } from '@models/student';
import { Address } from '@models/address';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  title = 'Registration';
  signupForm: FormGroup;

  student: Student = new Student();
  address: Address = new Address();
  emailSent = false;
  emailExists = false;
  errorMessage = '';

  loading = false;

  constructor(
      public authService: AuthService, private fb: FormBuilder,
    private studentService: StudentService, private router: Router) {}

  ngOnInit() {
    this.loading = true;

    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/account/dashboard']);
      return;
    }

    this.student.gender = 'male';
    this.student.address = new Address();

    this.createForm();

    this.loading = false;
  }

  createForm() {
    this.signupForm =
        this.fb.group({
          firstName: ['', [Validators.required, Validators.minLength(3)]],
          lastName: ['', [Validators.required, Validators.minLength(3)]],
          emailAddress:
              ['', [Validators.required, Validators.email]],
          username: ['', [Validators.required, Validators.minLength(3)]],
          phoneNumber: ['', [Validators.pattern('[0-9]+')]],
          gender: ['', [Validators.required]],
          studentAddress: this.fb.group({
            addressType: ['', []],
            addressLine1: ['', [Validators.required, Validators.minLength(3)]],
            addressLine2: ['', [Validators.minLength(3)]],
            city: ['', [Validators.required, Validators.minLength(3)]],
            state: ['', [Validators.minLength(3)]],
            country: ['', [Validators.required]],
            zip: ['', [Validators.minLength(3)]]
          }),
          accountPassword:
              this.fb.group(
                  {
                    password1:
                        ['', [Validators.required, Validators.minLength(5)]],
                    password2: ['', [Validators.required]]
                  },
                  {validator: passwordsMatcher})
          // captchaCode: ['', []] // we use 'correctCaptcha' directive to
          // validate captcha code control in the template
        });
  }

  async createAccount() {
    this.loading = true;
    // console.log(this.student);
    this.student.address = this.address;
    this.student.joinDate = (new Date).toISOString();
    try {
      // tslint:disable-next-line:prefer-const
      let response =
          await this.studentService.createAccount(this.student).toPromise();
      console.log(response);
      await swal({ text: 'Registration Successfull!', type: 'success', timer: 3000 });
      this.router.navigateByUrl('/login');
    } catch (err) {
      console.log(err);
        this.errorMessage = 'Registration Failed ' + err.error;
        this.loading = false;
        // this.emailExists = true;
        await swal({title: 'Registration Failed!', text: err.error , type: 'error'});
    }

  }

}
