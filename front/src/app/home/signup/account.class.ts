export class Account {
    public fullName: string;
    public emailAddress: string;
    public phoneNumber: string;
    public accountPassword: string;

    constructor(name: string, email: string, phone: string, password: string) {
        this.fullName = name;
        this.emailAddress = email;
        this.phoneNumber = phone;
        this.accountPassword = password;
    }
}
