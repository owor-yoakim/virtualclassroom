import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SubjectService } from '@services/subject/subject.service';
import { LessonService } from '@services/lesson/lesson.service';
import { SettingService } from '@services/setting/setting.service';
import { Lesson } from '@models/lesson';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  title = 'Welcome';
  longText = 'longText';
  loading = false;
  lessons: any[] = [];


  constructor(
    public settingService: SettingService,
    private subjectService: SubjectService,
    private lessonService: LessonService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.loading = true;
    try {
      let lessons = await this.lessonService.getAllLessons().toPromise();
      this.lessons = this.lessonService.arrangeLessons(lessons);
      this.loading = false;
    } catch (err) {
      console.error(err);
      this.loading = false;
    }
    
  }

  onValueChanged(data: string) {
    console.log('Long Text', data);
  }

}
