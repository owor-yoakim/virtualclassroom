import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AppConstant} from '@config/app-constant';
import {LoginCredentials} from '@models/login-credentials';
import {AuthService} from '@services/auth/auth.service';

@Component({
  selector: 'app-instructor-login',
  templateUrl: './instructor-login.component.html',
  styleUrls: ['./instructor-login.component.css']
})
export class InstructorLoginComponent implements OnInit {
  title = 'lang_243';
  instructorLoginForm: FormGroup;
  serverUrl = AppConstant.serverUrl;
  loginError = false;
  loginErrorMessage = '';
  loggedIn = false;
  loading = false;
  // login credentials
  credentials = new LoginCredentials();

  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.instructorLoginForm = new FormGroup({
      loginName: new FormControl('', [Validators.required]),
      loginPassword: new FormControl('', [Validators.required]),
    });
  }

  async accountLogin() {
    this.loading = true;
    const redirectUrl = this.authService.redirectUrl || '/account/dashboard';
    // console.log(this.credentials);
    try {
      let result = await this.authService.instructorLogin(this.credentials);
      // console.log(result);
      if (result) {
        this.credentials = new LoginCredentials();
        this.instructorLoginForm.reset();
        // location.reload();
        this.router.navigateByUrl(redirectUrl);
      } else {
        this.loading = false;
        this.loginError = true;
        this.credentials.loginPassword = '';
        this.loginErrorMessage = 'Invalid Credentials';
      }
    } catch (err) {
      this.loading = false;
      this.loginError = true;
      this.credentials.loginPassword = '';
      this.loginErrorMessage = err.error;
      console.error(err);
    }
  }
}
