import { Component, OnInit } from '@angular/core';
import { LoginCredentials } from '@models/login-credentials';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppConstant } from '@config/app-constant';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css']
})
export class StudentLoginComponent implements OnInit {
  title = 'lang_243';
  studentLoginForm: FormGroup;

  serverUrl = AppConstant.serverUrl;
  loginError = false;
  loginErrorMessage = '';
  loggedIn = false;
  loading = false;
  // login credentials
  credentials = new LoginCredentials();

  constructor(
    public authService: AuthService,
    private router: Router
  ) { }


  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.studentLoginForm = new FormGroup({
      loginName: new FormControl('', [Validators.required]),
      loginPassword: new FormControl('', [Validators.required]),
    });
  }

  async accountLogin() {
    this.loading = true;
    const redirectUrl = this.authService.redirectUrl || '/account/dashboard';
    // console.log(this.credentials);
    try {
      let result = await this.authService.studentLogin(this.credentials);
      // console.log(result);
      if (result) {
        this.credentials = new LoginCredentials();
        this.studentLoginForm.reset();
        // location.reload();
        this.router.navigateByUrl(redirectUrl);
      } else {
        this.loading = false;
        this.loginError = true;
        this.credentials.loginPassword = '';
        this.loginErrorMessage = 'Invalid Credentials';
      }
    } catch (err) {
      this.loading = false;
      this.loginError = true;
      this.credentials.loginPassword = '';
      this.loginErrorMessage = err.error;
      console.error(err);
    }

  }
}
