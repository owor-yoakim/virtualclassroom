import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from '@home/about/about.component';
import {IndexComponent} from '@home/index/index.component';
import {LoginComponent} from '@home/login/login.component';
import {ResetPasswordComponent} from '@home/reset-password/reset-password.component';
import {SignupComponent} from '@home/signup/signup.component';
import {StreamComponent} from '@stream/stream/stream.component';
import { UdacityComponent } from '@shared/udacity/udacity.component';
import { TestComponent } from '@shared/test/test.component';

const homeRoutes: Routes = [{
  path: '',
  children: [
    {path: '', component: IndexComponent},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: SignupComponent},
    {path: 'reset-password', component: ResetPasswordComponent},
    {path: 'about-us', component: AboutComponent}
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
