import {Location} from '@angular/common';
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '@services/auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'VirtTutor';
  currentUrl: string[];
  isAdmin = false;

  constructor(public authService: AuthService,
    private route: ActivatedRoute, private location: Location) {
    this.currentUrl = this.location.path().split('/');
    this.isAdmin = this.currentUrl.includes('administration');
    // console.log(this.isAdmin);
  }
}
