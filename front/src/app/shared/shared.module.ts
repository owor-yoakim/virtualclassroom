import {LessonCommentCreateComponent} from '@admin/lesson/lesson-comment-create/lesson-comment-create.component';
import {LessonCommentListComponent} from '@admin/lesson/lesson-comment-list/lesson-comment-list.component';
import {LessonModuleCreateComponent} from '@admin/lesson/lesson-module-create/lesson-module-create.component';
import {LessonModuleUpdateComponent} from '@admin/lesson/lesson-module-update/lesson-module-update.component';
import {LessonUpdateComponent} from '@admin/lesson/lesson-update/lesson-update.component';
import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatListModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule, MatSlideToggleModule, MatSortModule, MatTableModule, MatTabsModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {LessonItemComponent} from '@lesson/lesson-item/lesson-item.component';
import {LessonsGridComponent} from '@lesson/lessons-grid/lessons-grid.component';
import {AddressComponent} from '@shared/address/address.component';
import {FooterComponent} from '@shared/footer/footer.component';
import {HeaderComponent} from '@shared/header/header.component';
import {LongtextComponent} from '@shared/longtext/longtext.component';
import {NavbarComponent} from '@shared/navbar/navbar.component';
import {PairItemsPipe} from '@shared/pipes/pair-items.pipe';
import {SendMessageComponent} from '@shared/send-message/send-message.component';
import {SpinnerComponent} from '@shared/spinner/spinner.component';
import {TestComponent} from '@shared/test/test.component';
import {TextareaInputComponent} from '@shared/textarea-input/textarea-input.component';
import {TimerComponent} from '@shared/timer/timer.component';
import {UdacityComponent} from '@shared/udacity/udacity.component';
import {CanvasComponent} from '@stream/canvas/canvas.component';
import {TranslateModule} from '@translate/translate.module';
import {DataTablesModule} from 'angular-datatables';
import {CanvasWhiteboardModule} from 'ng2-canvas-whiteboard';
import {FlashMessagesModule} from 'ngx-flash-messages';

import {FileUploadComponent} from './file-upload/file-upload.component';


@NgModule({
  imports: [
    CommonModule,
    FlashMessagesModule,
    CanvasWhiteboardModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    DataTablesModule,
    MatTableModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatListModule
  ],
  declarations: [
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    LessonCommentCreateComponent,
    LessonCommentListComponent,
    LessonUpdateComponent,
    CanvasComponent,
    LessonModuleCreateComponent,
    LessonModuleUpdateComponent,
    LongtextComponent,
    TestComponent,
    SendMessageComponent,
    AddressComponent,
    LessonsGridComponent,
    LessonItemComponent,
    PairItemsPipe,
    UdacityComponent,
    FileUploadComponent,
    TimerComponent,
    SpinnerComponent,
    TextareaInputComponent
  ],
  exports: [
    // FlashMessageModule,
    FormsModule,
    ReactiveFormsModule,
    FlashMessagesModule,
    CanvasWhiteboardModule,
    RouterModule,
    CommonModule,
    TranslateModule,
    DataTablesModule,
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    LongtextComponent,
    LessonCommentCreateComponent,
    LessonUpdateComponent,
    LessonModuleCreateComponent,
    LessonModuleUpdateComponent,
    CanvasComponent,
    UdacityComponent,
    LessonCommentListComponent,
    SendMessageComponent,
    AddressComponent,
    LessonItemComponent,
    LessonsGridComponent,
    TextareaInputComponent,
    FileUploadComponent,
    TimerComponent,
    SpinnerComponent,
    PairItemsPipe,
    MatTableModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatListModule,
    TestComponent
  ]
})
export class SharedModule {}
