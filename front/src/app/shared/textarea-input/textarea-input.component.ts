import { Component, AfterViewInit, OnInit, forwardRef, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, ControlContainer, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-textarea-input',
  templateUrl: './textarea-input.component.html',
  styleUrls: ['./textarea-input.component.css'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaInputComponent),
      multi: true
    }
  ]
})
export class TextareaInputComponent implements OnInit, AfterViewInit, ControlValueAccessor, OnDestroy {

  @Input('value') value = '';
  @Input('id') id = '';
  // @Input() formGroup: FormGroup;
  // @Output('onValueChanged') onValueChanged = new EventEmitter<string>();

  get Value() {
    return this.value;
  }

  set Value(val) {
    this.value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor(public controlContainer: ControlContainer) {}

  ngOnInit() {
  }

   ngAfterViewInit() {
   
   }
  ngOnDestroy() {
    
  }

  writeValue(value: string) {
    if (value !== undefined) {
      this.Value = value;
      this.onChange(value);
    }
  }
  registerOnChange(fn: any) {
    this.onChange = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean) {
    
  }

  onChange = (value: string) => { console.log('Value: ', this.Value); };
  onTouched = () => {};
}
