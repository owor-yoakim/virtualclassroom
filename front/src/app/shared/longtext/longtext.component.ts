import {
  Component,
  AfterViewInit,
  EventEmitter,
  OnDestroy,
  Input,
  Output,
  forwardRef
} from '@angular/core';

import { FormGroup, AbstractControl, ControlContainer, FormControl} from '@angular/forms';

import 'tinymce';
import 'tinymce/themes/modern';

import 'tinymce/plugins/table';
import 'tinymce/plugins/link';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/image';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/print';
import 'tinymce/plugins/preview';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/media';
import 'tinymce/plugins/contextmenu';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/spellchecker';
import 'tinymce/plugins/wordcount';


declare var tinymce: any;
declare var window: any;

@Component({
  selector: 'app-longtext',
  templateUrl: './longtext.component.html',
  styleUrls: ['./longtext.component.css']
})
export class LongtextComponent implements AfterViewInit, OnDestroy {
  
  @Input('id') id = '';
  @Input('value') value = '';
  @Input('placeholder') placeholder = '';
  @Input('required') required: boolean;
  @Input('formGroup') formGroup: FormGroup = null;
  // @Input('formControlName') formControlName = '';
  @Output('onValueChange') onValueChange = new EventEmitter<string>();

  constructor(
    public controlContainer: ControlContainer
  ) { }

  editor;

  ngAfterViewInit() {
    // console.log(window.tinymce);
    window.tinymce.init({
      selector: '#' + this.id,
      max_height: 250,
      min_height: 100,
      menubar: true,
      plugins: ['advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen hr',
        'insertdatetime media table contextmenu paste spellchecker',
        'wordcount'],
      toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image spellchecker code',
      table_toolbar: 'tableprops cellprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol',
      powerpaste_allow_local_images: true,
      powerpaste_word_import: 'prompt',
      powerpaste_html_import: 'prompt',
      spellchecker_language: 'en',
      spellchecker_dialog: true,
      skin_url: '/assets/vendor/tinymce/skins/lightgray',
      setup: (editor) => {
        this.editor = editor;
        editor.on('init', () => {
          this.value && this.editor.setContent(this.value, { format: 'raw' });
        });
        editor.on('keyup change', () => {
          const content = <string>editor.getContent();
          this.onValueChange.emit(content);
          console.log(content);
        });
      }
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }

  

}
