import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { User } from '@models/user';
import { UserMessage } from '@models/user-message';
import { UserMessageService } from '@services/user-message/user-message.service';

@Component({
  selector: 'app-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.css']
})
export class SendMessageComponent implements OnInit {
  @Input('userFrom') userFrom: any = null;
  @Input('userTo') userTo: any = null;
  loading = false;
  longTextId = 'message';
  placeholderText = 'lang_225 | translate';
  userMessage = new UserMessage();
  messageForm: FormGroup;
  msgErr = 'lang_226 | translate';

  constructor(
    private userMessageService: UserMessageService
  ) { }

  ngOnInit() {
    this.messageForm = new FormGroup({
      fromUser: new FormControl('', []),
      toUser: new FormControl('', []),
      subject: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(255)]),
      message: new FormControl('', [Validators.required, Validators.minLength(5)])
    });

    this.userMessage.senderId = this.userFrom.id;
    this.userMessage.senderEmail = this.userFrom.emailAddress;
    this.userMessage.receiverId = this.userTo.id;
    this.userMessage.receiverEmail = this.userTo.emailAddress;

  }


  onValueChanged(message: string) {
    if (message.length < 10) {
      this.messageForm.setErrors({ messageError: this.msgErr });
    }

    this.messageForm.get('message').setValue(message);
    this.userMessage.message = message;
    // console.log(this.messageForm.get('message').invalid);
    // console.log(this.messageForm.value);
    console.log(this.userMessage);
  }

  async onSendMessage() {
    this.loading = true;
    if (this.userMessage.message.length < 10) {
      this.messageForm.setErrors({ messageError: this.msgErr });
      this.loading = false;
      swal({ title: 'Invalid message entered. Your message must be atleast 10 characters in length.', text: this.msgErr, type: 'error' });
      return;
    }

    try {
      let response = await this.userMessageService.sendMessage(this.userMessage);
      console.log(response);
      await swal({ title: 'Message sent successfully!', text: 'Message sent successfully!', type: 'success' });
      this.messageForm.reset();
      this.userMessage.subject = '';
      this.userMessage.message = '';
      this.loading = false;

    } catch (err) {
      console.error(err);
      await swal({ title: 'Request Denied', type: 'error' });
      this.loading = false;
    }

    
  }

}
