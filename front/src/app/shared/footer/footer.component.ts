import { Component, OnInit } from '@angular/core';

import { SettingService } from '@services/setting/setting.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  isHidden = false;

  constructor(public settingService: SettingService) { }

  ngOnInit() {
  }

  onCollapse() {
    this.isHidden = !this.isHidden;
  }

}
