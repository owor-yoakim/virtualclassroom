import { Component, OnInit } from '@angular/core';

import { SocketService } from '@services/socket/socket.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  gcanvas: any;
  containerEl: HTMLElement;
  readdFlag = 0;
  eraserFlag = false;
  reflag = false;
  eraserArray = [];
  eraserColor = '';
  objectCount = 0;
  totCount = 0;
  totArray = [];
  shapeArry =  [];
  shapenum = 1;
  txtNum = 1;




  constructor(private socketService: SocketService) { }

  ngOnInit() {
    this.containerEl = document.getElementById('center');
    this.gcanvas = new fabric.Canvas('canvasBoard', {
      isDrawingMode: true,
      backgroundColor: '#ffffff'
    });
    console.log(this.gcanvas);
    fabric.Object.prototype.transparentCorners = false;
    this.gcanvas.setHeight(this.containerEl.scrollHeight);
    this.gcanvas.setWidth(this.containerEl.scrollWidth);
    this.gcanvas.freeDrawingBrush.color = '#000000';
    this.gcanvas.freeDrawingBrush.width = 2;

    setInterval(() => {
      let screenshot = this.gcanvas.toDataURL('image/jpeg', 0.5);
      let dataToSend = {
        screenshot: screenshot,
        screenImage: true
      };
      this.socketService.emit('screenshot', dataToSend);
      console.log(dataToSend);
    }, 500);

    this.socketService.on('screenshot', (data) => {
      // if user receives the image from instructor screen that he is sharing
      if (data.screenImage) {
        let sharedPartOfScreenPreview = document.getElementById('sharedPartOfScreenPreview');
        sharedPartOfScreenPreview.setAttribute('src', data.screenshot);
        console.log(data);
        return;
      }
    });

    this.socketService.on('draw', (data) => {
      // if user receives the image from instructor screen that he is sharing
      if (data.screenImage) {
        let sharedPartOfScreenPreview = document.getElementById('sharedPartOfScreenPreview');
        sharedPartOfScreenPreview.setAttribute('src', data.screenshot);
        console.log(data);
        return;
      }
    });
  }

  selectShape() {
    if (this.shapenum > 1 || this.txtNum > 1) {
      this.gcanvas.isDrawingMode = false;
    }

    this.gcanvas.forEachObject((obj) => {
      if (typeof obj !== 'object' || obj.hasOwnProperty('path')) {
        obj.set({
          hasControls: false,
          hasBorders: false,
          selectable: false
        });
      } else {
        obj.set({
          hasControls: true,
          hasBorders: true,
          selectable: true
        });
        this.gcanvas.setActiveObject(obj);
      }
    });

    this.gcanvas.renderAll(); 
  }

  drawShape() {
    this.readdFlag = 0;
    this.eraserFlag = false;
    this.gcanvas.isDrawingMode = true;
    // this.gcanvas.freeDrawingBrush.color = $('#txtPencilClrVal').val();
    // this.gcanvas.freeDrawingBrush.width = $('#lineSlide').val();
    this.gcanvas.forEachObject(function (obj) {
      obj.set({
        hasControls: false,
        hasBorders: false,
        selectable: false
      });
    });

    this.gcanvas.renderAll();

  }

  eraseCanvas() {
    this.eraserFlag = true;
    if (typeof this.gcanvas.backgroundColor === 'object') {
      this.eraserColor = '#ffffff';
    } else {
      this.eraserColor = this.gcanvas.backgroundColor;
    }
    this.eraserArray.push(this.eraserColor);
    this.gcanvas.freeDrawingBrush.color = this.eraserColor;
    this.gcanvas.freeDrawingBrush.width = $('#eraserSlide').val();
    this.gcanvas.isDrawingMode = true;
    this.gcanvas.forEachObject(function (obj) {
      obj.set({
        hasControls: false,
        hasBorders: false,
        selectable: false
      });
    });
    this.gcanvas.renderAll();

  }

  addObject(opt) {
    this.totArray[this.objectCount] = opt.target;
    this.objectCount++;
  }

  undoCanvas() {
    if (this.objectCount > 0) {
    this.reflag = true;
    this.gcanvas.remove(this.totArray[this.objectCount - 1]);
    this.objectCount--;
  }
}


redoCanvas() {
  if (this.objectCount < this.totArray.length) {
    this.gcanvas.add(this.totArray[this.objectCount]);
    if (!this.reflag) {
      this.objectCount++;
    }
  }
  }


}
