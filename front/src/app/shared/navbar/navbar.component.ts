import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { Subject } from '@models/subject';
import { StudentService } from '@services/student/student.service';
import { SettingService } from '@services/setting/setting.service';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { AddressService } from '@services/address/address.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  loggedIn = false;
  subjects: Subject[] = [];

  constructor(
      public authService: AuthService, public settingService: SettingService,
      private studentService: StudentService,
      private subjectService: SubjectService,
      private addressService: AddressService, private router: Router) {}

  async ngOnInit() {
    this.loggedIn = this.authService.isAuthenticated();
    try {
      this.subjects = await this.subjectService.getAllSubjects().toPromise();
    } catch (err) {
      console.error(err);
    }

  }


  accountLogout() {
    // this.authService.logout();
    // this.router.navigate(['/']);
  }

}
