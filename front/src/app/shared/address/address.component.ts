import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, ControlContainer, FormGroup} from '@angular/forms';
import {Address} from '@models/address';
import {Country} from '@models/country';
import {AddressService} from '@services/address/address.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  @Input('address') address: Address;
  countries$: Observable<Country[]>;

  constructor(
      public controlContainer: ControlContainer,
      private addressService: AddressService) {}

  ngOnInit() {
    this.countries$ = this.addressService.getAllCountries();
  }
}
