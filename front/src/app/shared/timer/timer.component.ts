import { Component, OnInit, OnDestroy } from '@angular/core';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit, OnDestroy {

  currentTime = null;
  timer = null;
  subscription: Subscription;

  constructor() { }

  ngOnInit() {
    let timer = TimerObservable.create(2000, 1000);
    this.subscription = timer.subscribe(t => {
      this.getTime();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getTime() {
    this.currentTime = (new Date()).toLocaleString();
  }

}
