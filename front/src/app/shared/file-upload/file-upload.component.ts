import {HttpEventType, HttpResponse} from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Lesson} from '@models/lesson';
import {LessonFile} from '@models/lesson-file';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {FileUploadService} from '@services/file-upload/file-upload.service';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  @Input('lesson') lesson: Lesson;
  selectedFiles: FileList = null;
  currentFileUpload: File = null;
  progressPercentage = 0;
  lessonFile: LessonFile = null;
  user: User = null;
  description = '';
  fileUploadForm: FormGroup;


  constructor(
      public authService: AuthService,
      public uploadService: FileUploadService) {}

  ngOnInit() {
    this.user = this.authService.getUser();
    this.fileUploadForm = new FormGroup({
      description: new FormControl(
          '',
          [
            Validators.required, Validators.minLength(10),
            Validators.maxLength(255)
          ]),
      inputFile: new FormControl('', [Validators.required])
    });
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    if (this.selectedFiles !== null && this.selectedFiles.length > 0) {
      this.progressPercentage = 0;
      this.currentFileUpload = this.selectedFiles.item(0);

      this.lessonFile = new LessonFile();
      this.lessonFile.studentId = null;
      this.lessonFile.instructorId = null;

      if (this.authService.isStudentAuthenticated()) {
        this.lessonFile.studentId = this.user.id;
      } else if (this.authService.isInstructorAuthenticated()) {
        this.lessonFile.instructorId = this.user.id;
      }
      this.lessonFile.lessonId = this.lesson.id;
      this.lessonFile.name = this.currentFileUpload.name;
      this.lessonFile.type = this.currentFileUpload.type;
      this.lessonFile.size = this.currentFileUpload.size;
      this.lessonFile.description = this.description;

      this.uploadService
          .uploadFile(
              '/lesson/upload', this.lessonFile, this.currentFileUpload,
              this.lesson)
          .subscribe((response) => {
            if (response.type === HttpEventType.UploadProgress) {
              this.progressPercentage =
                  Math.round(100 * response.loaded / response.total);
            } else if (response instanceof HttpResponse) {
              console.log('File uploaded!', JSON.parse(<any>response.body));
              this.fileUploadForm.reset();
            }
          });
      this.selectedFiles = null;
    }
  }
}
