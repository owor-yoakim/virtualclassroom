import { Injectable, Inject } from '@angular/core';

// import our injection token
import { TRANSLATIONS } from '@translate/translation';

@Injectable()
export class TranslationService {
    private currentLang: string;
    private fallbackLang = 'en';
    private supportedLangs = [
        {
          display: 'English',
          value: 'en'
        },
        {
          display: 'Spanish',
          value: 'es'
        }
    ];

    public getCurrentLanguage() {
        return this.currentLang;
    }

    // inject the translations
    constructor( @Inject(TRANSLATIONS) private translations: any) {
        if (localStorage.getItem('lang') === null) {
            const lang = navigator.language;
            this.use((lang === 'en' || lang === 'en-US') ? 'en' : lang);
            localStorage.setItem('lang', this.currentLang);
        } else {
            this.use(localStorage.getItem('lang'));
        }
    }

    public use(lang: string) {
        // set current language
        this.currentLang = lang;
        localStorage.setItem('lang', this.currentLang);
    }

    public setFallbackLanguage(lang: string) {
        this.fallbackLang = lang;
    }

    // perform translation
    private translate(key: string): string {
        const translation = key;

        if (this.translations[this.currentLang] && this.translations[this.currentLang][key]) {
            return this.translations[this.currentLang][key];
        } else if (this.translations[this.fallbackLang]) {
            return this.translations[this.fallbackLang][key];
        } else {
            return translation;
        }
    }

    public instant(key: string) {
        // call translation
        return this.translate(key);
    }

    public getSupportedLanguages() {
        return this.supportedLangs;
    }
}
