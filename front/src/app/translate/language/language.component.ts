import { Component, OnInit } from '@angular/core';

import { TranslationService } from '@translate/services/translation.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
  currentLanguage = '';

  supportedLanguages: any[] = [];

  constructor(private translateService: TranslationService) { }

  ngOnInit() {
    this.currentLanguage = this.translateService.getCurrentLanguage();
    this.supportedLanguages = this.translateService.getSupportedLanguages();
  }

  changeLanguage(lang: string) {
    // set current lang;
    // console.log(lang);
    this.translateService.use(lang);
    // location.reload();
  }

}
