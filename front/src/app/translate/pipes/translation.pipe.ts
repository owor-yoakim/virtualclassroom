import { Pipe, PipeTransform } from '@angular/core';

// our translation service
import { TranslationService } from '@translate/services/translation.service';

@Pipe({
    name: 'translate',
    pure: false // add in this line, update value when we change language
})

export class TranslationPipe implements PipeTransform {

    constructor(private translate: TranslationService) { }

    transform(value: string, args: any[]): any {
        if (!value) {
            return;
        }

        return this.translate.instant(value);
    }
}
