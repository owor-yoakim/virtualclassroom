import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TRANSLATION_PROVIDERS } from '@translate/translation';
import { TranslationService } from '@translate/services/translation.service';
import { TranslationPipe } from '@translate/pipes/translation.pipe';
import { LanguageComponent } from '@translate/language/language.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TranslationPipe, LanguageComponent],
  exports: [TranslationPipe, LanguageComponent]
})
export class TranslateModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TranslateModule,
      providers: [
        TRANSLATION_PROVIDERS,
        TranslationService
      ]
    };
  }
}
