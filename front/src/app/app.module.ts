import {AccountModule} from '@account/account.module';
import {AdminModule} from '@admin/admin.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Component, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {CoreModule} from '@core-module/core.module';
import {HomeModule} from '@home/home.module';
import {InstructorModule} from '@instructor/instructor.module';
import {LessonModule} from '@lesson/lesson.module';
import {SharedModule} from '@shared/shared.module';
import {StreamModule} from '@stream/stream.module';
import {TranslateModule} from '@translate/translate.module';
import {FlashMessagesModule} from 'ngx-flash-messages';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AuthInterceptor} from './auth-interceptor';



@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, HttpModule, FormsModule, ReactiveFormsModule,
    HttpClientModule, BrowserAnimationsModule, CoreModule.forRoot(),
    TranslateModule.forRoot(), SharedModule, HomeModule, InstructorModule,
    LessonModule, AccountModule, StreamModule, AdminModule, AppRoutingModule
  ],
  providers:
      [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}

      ],
  exports: [BrowserAnimationsModule],
  bootstrap: [AppComponent]
})

export class AppModule {}
