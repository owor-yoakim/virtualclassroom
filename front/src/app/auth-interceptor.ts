import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpRequest,
    HttpHandler,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const studentToken = localStorage.getItem('studentAuthToken');
        const instructorToken = localStorage.getItem('instructorAuthToken');
        const adminToken = localStorage.getItem('adminAuthToken');
        if (studentToken) {
            const clonedReq = req.clone({
                headers: req.headers.set('authorization', studentToken)
            });
            return next.handle(clonedReq);
        } else if (instructorToken) {
            const clonedReq = req.clone({
                headers: req.headers.set('authorization', instructorToken)
            });
            return next.handle(clonedReq); 
        } else if (adminToken) {
          const clonedReq = req.clone({headers: req.headers.set('authorization', adminToken)});
          return next.handle(clonedReq);
        } else {
          return next.handle(req);
        }
    }
}
