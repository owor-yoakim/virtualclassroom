import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InstructorDetailsComponent} from '@instructor/instructor-details/instructor-details.component';
import {InstructorsContainerComponent} from '@instructor/instructors-container/instructors-container.component';
import {InstructorsListComponent} from '@instructor/instructors-list/instructors-list.component';
import {StreamComponent} from '@stream/stream/stream.component';

const instructorRoutes: Routes = [{
  path: 'instructors',
  children: [{
    path: '',
    component: InstructorsContainerComponent,
    children: [
      {path: '', redirectTo: ':subjectSlug', pathMatch: 'full'},
      {path: ':subjectSlug', component: InstructorsListComponent},
      {path: 'show/:instructorId', component: InstructorDetailsComponent}
    ]
  }]
}

];

@NgModule({
  imports: [RouterModule.forChild(instructorRoutes)],
  exports: [RouterModule]
})
export class InstructorRoutingModule {}
