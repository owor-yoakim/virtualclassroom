// import 'rxjs/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Instructor} from '@models/instructor';
import {Lesson} from '@models/lesson';
import {LessonModule} from '@models/lesson-module';
import {Subject} from '@models/subject';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {InstructorService} from '@services/instructor/instructor.service';
import {Observable} from 'rxjs/Observable';
// import 'rxjs/add/operator/filter';
import {from} from 'rxjs/observable/from';
import {filter} from 'rxjs/operators';
import swal from 'sweetalert2';

@Component({
  selector: 'app-instructor-details',
  templateUrl: './instructor-details.component.html',
  styleUrls: ['./instructor-details.component.css']
})
export class InstructorDetailsComponent implements OnInit {
  instructor: Instructor = null;
  subjects: Subject[] = [];
  lessons: Lesson[] = [];
  loading = false;
  user: User = null;

  constructor(
      public authService: AuthService,
      private instructorService: InstructorService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();
    this.route.params.subscribe(async (params) => {
      // console.log(params);
      const instructorId = params.instructorId || null;
      console.log(instructorId);
      if (instructorId !== null) {
        try {
          let instructor =
              this.instructorService.getInstructorById(instructorId)
                  .toPromise();
          let lessons =
              this.instructorService.getInstructorLessons(instructorId)
                  .toPromise();

          this.instructor = await instructor;
          this.lessons = await lessons;

          this.subjects = this.getInstructorSubjects();
          this.loading = false;

        } catch (err) {
          console.error(err);
          this.router.navigateByUrl('/instructors');
        }
      } else {
        console.error('Unknown instructor');
        this.router.navigateByUrl('/instructors');
      }
    });
  }

  getInstructorSubjects() {
    const subjects: Subject[] = [];
    this.lessons.forEach((lesson) => {
      if (subjects
              .filter((subject) => {
                return subject.id === lesson.subject.id;
              })
              .length === 0) {
        subjects.push(lesson.subject);
      }
    });
    return subjects;
  }

  countLessonsBySubject(subjectId: number) {
    return this.lessons
        .filter((lesson) => {
          return lesson.subject.id === subjectId;
        })
        .length;
  }

  onSendEmail() {}

  onCall() {}
}
