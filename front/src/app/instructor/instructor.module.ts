import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';
import { InstructorDetailsComponent } from '@instructor/instructor-details/instructor-details.component';
import {InstructorLessonsComponent} from '@instructor/instructor-lessons/instructor-lessons.component';
import {InstructorRoutingModule} from '@instructor/instructor-rounting.module';
import {InstructorsListComponent} from '@instructor/instructors-list/instructors-list.component';
import {SharedModule} from '@shared/shared.module';
import {TranslateModule} from '@translate/translate.module';
import { InstructorsContainerComponent } from '@instructor/instructors-container/instructors-container.component';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    InstructorRoutingModule
  ],
  declarations: [
    InstructorsContainerComponent,
    InstructorsListComponent,
    InstructorDetailsComponent,
    InstructorLessonsComponent
  ],
  exports: [
    InstructorsContainerComponent,
    InstructorsListComponent,
    InstructorDetailsComponent,
    InstructorLessonsComponent
  ],
  providers: [

  ]
})
export class InstructorModule { }
