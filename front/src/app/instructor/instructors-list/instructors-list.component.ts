import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Subject } from '@models/subject';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';
import { User } from '@models/user';

import { InstructorService } from '@services/instructor/instructor.service';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';



@Component({
  selector: 'app-instructors-list',
  templateUrl: './instructors-list.component.html',
  styleUrls: ['./instructors-list.component.css']
})
export class InstructorsListComponent implements OnInit {
  loading = false;
  instructors: Instructor[] = [];
  user: User = null;
  subjects: Subject[] = [];
  currentSubject: Subject = null;
  instructorLessons: Lesson[] = [];
  subjectSlug = null;

  constructor(
    public authService: AuthService,
    private instructorService: InstructorService,
    private subjectService: SubjectService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();
    this.route.params.subscribe(async (params) => {
      // console.log(params);
      this.subjectSlug = params.subjectSlug || 'all';
      console.log(this.subjectSlug);
      try {
        let subjects = this.subjectService.getAllSubjects().toPromise();
        // tslint:disable-next-line:max-line-length
        let instructors: Promise<Instructor[]>;
        if (this.subjectSlug !== 'all') {
          instructors = this.instructorService.getInstructorsBySubjectSlug(this.subjectSlug).toPromise();
        } else {
          instructors = this.instructorService.getAllInstructors().toPromise();
        }

        this.subjects = await subjects;
        this.instructors = await instructors;
        this.currentSubject = this.subjects.find((subject) => {
          return subject.slug === this.subjectSlug;
        }) || null;

        this.loading = false;
      } catch (err) {
        console.error(err);
      }

    });

  }



  // get instructor's comma seperated subject titles
  getSubjects(lessons: Lesson[]) {
    const subjects: string[] = [];
    lessons.forEach((x) => {
      if (subjects.indexOf(x.subject.title) === -1) {
        subjects.push(x.subject.title);
      }
    });
    return subjects.sort().toLocaleString();
  }


  // get instructor's number of completed lessons
  completedLessons(lessons: Lesson[]) {
   return lessons.filter((x) => {
      return x.status === 'completed';
    }).length;
  }
  // get instructor's number of upcoming lessons
  upcomingLessons(lessons: Lesson[]) {
   return  lessons.filter((x) => {
      return x.status === 'upcoming';
    }).length;
  }

  viewTutorProfile(instructor: Instructor) {
    this.router.navigateByUrl('/instructors/show/' + instructor.id);
  }

}
