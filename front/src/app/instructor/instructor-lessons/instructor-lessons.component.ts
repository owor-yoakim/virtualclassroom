import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { InstructorService } from '@services/instructor/instructor.service';
import { DateTimeHelper } from '@helpers/date-time-helper';

import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';


@Component({
  selector: 'app-instructor-lessons',
  templateUrl: './instructor-lessons.component.html',
  styleUrls: ['./instructor-lessons.component.css']
})
export class InstructorLessonsComponent implements OnInit {
  @Input('instructor') instructor: Instructor;
  @Input('lessons') lessons: Lesson[];
  title = 'Instructor Lessons';
  dt = new DateTimeHelper();

  constructor(
    private instructorService: InstructorService,
    private router: Router
  ) { }

  ngOnInit() {

  }

}
