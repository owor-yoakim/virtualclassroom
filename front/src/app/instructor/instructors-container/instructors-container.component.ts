import { Component, OnInit } from '@angular/core';

import { Subject } from '@models/subject';
import { SubjectService } from '@services/subject/subject.service';
import { AuthService } from '@services/auth/auth.service';


@Component({
  selector: 'app-instructors-container',
  templateUrl: './instructors-container.component.html',
  styleUrls: ['./instructors-container.component.css']
})
export class InstructorsContainerComponent implements OnInit {
  title = 'lang_73';
  loading = false;
  subjects: Subject[] = [];

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
  ) { }

  async ngOnInit() {
    try {
      this.subjects = await this.subjectService.getAllSubjects().toPromise();
    } catch (err) {
      console.error(err);
    }
  }


}
