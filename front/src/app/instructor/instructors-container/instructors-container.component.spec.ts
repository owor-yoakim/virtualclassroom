import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsContainerComponent } from './instructors-container.component';

describe('TutorsContainerComponent', () => {
  let component: InstructorsContainerComponent;
  let fixture: ComponentFixture<InstructorsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InstructorsContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
