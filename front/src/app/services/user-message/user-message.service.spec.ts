import { TestBed, inject } from '@angular/core/testing';

import { UserMessageService } from './user-message.service';

describe('UserMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserMessageService]
    });
  });

  it('should be created', inject([UserMessageService], (service: UserMessageService) => {
    expect(service).toBeTruthy();
  }));
});
