import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { UserMessage } from '@models/user-message';

@Injectable()
export class UserMessageService {
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
  }

  sendMessage(message: UserMessage) {
    this.url = this.serverUrl + '/user-message';
    return this.http.post(this.url, message, {headers: this.headers});
  }

  getUserMessages(receiverEmail: string) {
    this.url = this.serverUrl + '/user-message/getByReceiverEmail/' + receiverEmail;
    return this.http.get(this.url);
  }

}
