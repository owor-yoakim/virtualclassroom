import { Injectable } from '@angular/core';

import swal from 'sweetalert2';

import { AppConstant } from '@config/app-constant';
import { Student } from '@models/student';
import { Instructor } from '@models/instructor';
import { SocketService } from '@services/socket/socket.service';


@Injectable()
export class StreamService {
  public static hasStarted = false;
  public static stream: MediaStream = null;
  private serverUrl = AppConstant.serverUrl;
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private iceServers = AppConstant.iceServers;

  private student: Student = null;
  private instructor: Instructor = null;
  private isConnected = false;
  private videoElement: HTMLVideoElement = null;
  // this peer's connection
  private peerConnection: RTCPeerConnection = null;
  // used for any advertisings while the video is playing
  private arrayOfWebPImages = [];
  // SimplePeer Configurations
  private peerConfig: RTCConfiguration = {
    iceServers: this.iceServers
  };

  constructor(private socketService: SocketService) { }

  setInstructor(instructor: Instructor) {
    this.instructor = instructor;
  }

  getInstructor() {
    return this.instructor;
  }

  setStudent(student: Student) {
    this.student = student;
  }

  getStudent() {
    return this.student;
  }

  
  setVideoElement(videoElement: HTMLVideoElement) {
    this.videoElement = videoElement;
  }

  // creates a new peer connection
  createPeerConnection() {
    // create the peer
    this.peerConnection = new RTCPeerConnection(this.peerConfig);
    // send ice candidate to signaling channel
    this.peerConnection.onicecandidate = this.onIceCandidate;
    // listen to incoming streams
    this.peerConnection.onaddstream = this.onAddStream;
  }

  signalHandler() {
    this.socketService.on('offer', this.onOffer);
    this.socketService.on('answer', this.onAnswer);
    this.socketService.on('candidate', this.addIceCandidate);
  }

  addStream(stream: MediaStream) {
    if (StreamService.stream === null && this.instructor !== null) {
      StreamService.stream = stream;
      // add the stream to this peer connection
      this.peerConnection.addStream(stream);
      // create an offer
      this.createOffer();
      console.log('Stream Added to peer Connection');
    }

    this.peerConnection.addStream(stream);
  }

  // render the video on the video element
  private renderStream() {
    this.videoElement.srcObject = StreamService.stream;
    this.videoElement.play().then(() => {
      this.videoElement.controls = true;
      this.videoElement.muted = false;
      console.log('Playing Remote Video');
    }).catch((error) => {
      console.log('Failed to play remote video: ' + error.message);
    });
  }

  private createOffer() {
    // create offer
    this.peerConnection.createOffer().then((sdp) => {
      console.log('Offer Created');
      // set local sdp
      this.setLocalSDP(sdp);
      // send offer to signaling server
      this.socketService.emit('offer', sdp);
      console.log('Offer Signal Sent');
    }).catch((error) => {
      console.log('createOffer() Error: ' + error.message);
    });
  }

  private onIceCandidate(event) {
    if (event.candiadate) {
      this.socketService.emit('candidate', event.candidate);
    }
  }

  private addIceCandidate(candidate) {
    const newCandidate = new RTCIceCandidate(candidate);
    this.peerConnection.addIceCandidate(newCandidate).then(() => {
      console.log('Remote Ice Candidate Added');
    }).catch((error) => {
      console.log('onCandidate() Error: ' + error.message);
    });
  }

  private setLocalSDP(sdp) {
    this.peerConnection.setLocalDescription(sdp).then(() => {
      console.log('Local SDP Set');
    }).catch((error) => {
      console.log('setLocalSDP() Error: ' + error.message);
    });
  }

  // an offer received
  private onOffer(offer) {
    // create a new connection
    if (this.peerConnection === null) {
      this.createPeerConnection();
    }
    // set remote sdp
    this.setRemoteSDP(offer);
    // create answer
    this.createAnswer();
  }

  // set remote sdp
  private setRemoteSDP(sdp) {
    const newSdp = new RTCSessionDescription(sdp);
    this.peerConnection.setRemoteDescription(newSdp).then(() => {
      console.log('Remote SDP Set');
    }).catch((error) => {
      console.log('setRemoteSDP() Error: ' + error.message);
    });
  }


  private createAnswer() {
    this.peerConnection.createAnswer().then(sdp => {
      // send local sdp
      this.setLocalSDP(sdp);
      // send answer with local sdp
      this.socketService.emit('answer', this.peerConnection.localDescription);
      console.log('Answer Signal Sent with Local SDP');
    }).catch((error) => {
      console.log('createAnswer() Error: ' + error.message);
    });
  }

  // handle on answer event
  private onAnswer(answer) {
    this.setRemoteSDP(answer);
  }

  // triggered if there is stream
  private onAddStream(event) {
    if (this.student !== null) {
      StreamService.stream = event.stream;
      StreamService.hasStarted = true;
      console.log('Received Remote Stream');
      this.videoElement.poster = '/assets/images/spinner.gif';
      this.renderStream();
    }
  }

  // if stream is removed
  private onRemoveStream(event: MediaStreamEvent) {
    if (this.student !== null) {
      this.videoElement.poster = '/assets/images/spinner.gif';
      this.videoElement.src = '';
      this.videoElement.poster = '/assets/images/logo.png';
      swal({ text: 'The Instructor Has Ended the class. Check again later.', type: 'info' });
    }
  }

}
