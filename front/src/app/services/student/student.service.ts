import { Injectable } from '@angular/core';
import {Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '@services/auth/auth.service';
import { Student } from '@models/student';
import { AppConstant } from '@config/app-constant';
import { Enrolment } from '@models/enrolment';
import { Lesson } from '@models/lesson';
import { User } from '@models/user';


@Injectable()
export class StudentService {
  serverUrl = AppConstant.serverUrl + '/site/api';
  adminBaseUrl = AppConstant.adminBaseUrl;
  headers = new HttpHeaders();
  url = '';

  user: User = null;
  enrolments: Enrolment[] = [];

  constructor(
    public authService: AuthService,
    private http: HttpClient,
    private router: Router) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
    this.user = this.authService.getUser();
    this.refreshEnrolments();

  }

  async refreshEnrolments() {
    if (this.authService.isStudentAuthenticated()) {
      try {
        this.enrolments = await this.getEnrolments(this.user.id).toPromise();
      } catch (error) {
        console.error(error);
      }
    }
    
  }

  getStudentById(studentId: number) {
    if (this.authService.isAdminAuthenticated()) {
      this.url = this.adminBaseUrl + '/student/getById/' + studentId; 
    } else {
      this.url = this.serverUrl + '/account/student/getById/' + studentId; 
    }
    return this.http.get<Student>(this.url);
  }

  createAccount(student: Student) {
    this.url = this.serverUrl + '/register';
    console.log(student);
    return this.http.post(this.url, student, {headers: this.headers});
  }

  resetPassword() {
    this.url = this.serverUrl + '/resetPassword';
    return this.http.post(this.url, '', { headers: this.headers });
  }

  updateProfile(student: Student) {
    this.url = this.serverUrl + '/updateProfile';
    return this.http.post(this.url, student, { headers: this.headers });
  }

  changePassword(student: Student, oldPassword: string, newPassword: string) {
    this.url = this.serverUrl + '/account/student/changePassword';
    let formData = {
      studentId: student.id,
      oldPassword: oldPassword,
      newPassword: newPassword
    };
    return this.http.put(this.url, formData, { headers: this.headers });
  }


  enrollForLesson(enrolment: Enrolment) {
    this.url = this.serverUrl + '/enrolment/create';
    return this.http.post(this.url, enrolment, { headers: this.headers });
  }

  getEnrolments(studentId: number) {
    this.url = this.serverUrl + '/enrolment/getByStudent/' + studentId;
    return this.http.get<Enrolment[]>(this.url);
  }

 
  isEnrolledForLesson(lessonId: number) {
    const enrolments = this.enrolments.filter((enrolment) => {
      return enrolment.lessonId === lessonId;
    });

    return enrolments.length > 0;
  }

  canEnrolForLesson(lesson: Lesson) {
    if (this.authService.isAdminAuthenticated()) {
      return false;
    }

    if (this.authService.isInstructorAuthenticated()) {
      return false;
    }

    if (!this.authService.isStudentAuthenticated()) {
      return true;
    }

    if (this.isEnrolledForLesson(lesson.id)) {
      return false;
    }
    return true;
  }

  // Admin actions
  createStudent(student: Student) {
    this.url = this.adminBaseUrl + '/student/create';
    return this.http.post(this.url, student, {headers: this.headers});
  }

  updateStudent(student: Student) {
    this.url = this.adminBaseUrl + '/student/update/' + student.id;
    return this.http.put(this.url, student, {headers: this.headers});
  }

  getStudentByEmail(email: string) {
    this.url = this.adminBaseUrl + '/student/getByEmail/' + email;
    return this.http.get<Student>(this.url);
  }

  getAllStudents() {
    this.url = this.adminBaseUrl + '/student/all';
    return this.http.get<Student[]>(this.url);
  }

  deleteStudent(id: number) {
    this.url = this.adminBaseUrl + '/student/delete/' + id;
    return this.http.delete(this.url, {responseType: 'text' });
  }

  activateStudent(student: Student) {
    this.url = this.adminBaseUrl + '/student/activate/' + student.id;
    return this.http.put(this.url, student, {headers: this.headers});
  }

  deactivateStudent(student: Student) {
    this.url = this.adminBaseUrl + '/student/deactivate/' + student.id;
    return this.http.put(this.url, student, {headers: this.headers});
  }
  
}
