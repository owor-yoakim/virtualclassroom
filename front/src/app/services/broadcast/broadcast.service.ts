import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AppConstant} from '@config/app-constant';
import {Student} from '@models/student';
import {Instructor} from '@models/instructor';
import {Lesson} from '@models/lesson';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {StudentService} from '@services/student/student.service';
import {InstructorService} from '@services/instructor/instructor.service';
import * as DetectRTC from 'detectrtc';
import * as io from 'socket.io-client';
import swal from 'sweetalert2';

declare const RTCMultiConnection;

@Injectable()
export class BroadcastService {
  serverUrl = AppConstant.serverUrl;
  socket = null;
  user: User = null;
  student: Student = null;
  instructor: Instructor = null;
  lesson: Lesson = null;
  connection = null;

  constructor(
    public authService: AuthService,
    private studentService: StudentService,
    private instructorService: InstructorService,
    private router: Router,
    private route: ActivatedRoute) {

    this.user = this.authService.getUser();

    this.connection = new RTCMultiConnection();

    this.connection.socketURL = AppConstant.socketURL;

    // this.connection.sessionid = this.lesson.slug;

    // set this line to close room as soon as owner leaves
    // this.connection.autoCloseEntireSession = true;

    // this.connection.session = AppConstant.session;
    this.connection.session = {
      video: false,
      audio: false,
      data: true,
      oneway: false
      };
    // this.connection.bandwidth = AppConstant.bandwidth;
    // this.connection.bandwidth = {
    //  video: 256,   // 256kbps
    //  screen: 300,  // 300kbps
    //  audio: 128    // 128kbps
    // };

    this.connection.maxParticipantsAllowed = AppConstant.maxParticipantsAllowed;

    this.connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };

    this.connection.mediaConstraints.video = AppConstant.videoConstraints;
    // this.connection.mediaConstraints.video.mandatory.chromeMediaSource = DetectRTC.screen.chromeMediaSource;
    
  }

  setUser(user: User) {
    this.user = user;
    this.connection.userid = this.user.id;
  }
  setLesson(lesson: Lesson) {
    this.lesson = lesson;
    this.connection.extra = {
      user: this.user,
      lesson: this.lesson
    };
  }

  openClassroom() {
    this.connection.open(this.lesson.slug);
    this.connection.isInitiator = true;
  }

  startBroadcast() {
    this.connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };

    this.connection.mediaConstraints = {
      video: AppConstant.videoConstraints,
      audio: true
    };
    this.connection.addStream({
      audio: true,
      video: true,
      oneway: true
    });
  }

  numberConnected() {
    return this.connection.getAllParticipants().length;
  }

  removeParticipant(id: number) {
    this.connection.peers[id].sendCustomMessage({
      ejected: true
    });
  }

  joinClassroom() {
    this.connection.mediaConstraints = {
      audio: false,
      video: false
    };

    this.connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };
    this.connection.dontCaptureUserMedia = true;
    this.connection.join(this.lesson.slug);
    this.connection.isInitiator = false;
  }

  shareScreen(studentId?: number) {
    this.connection.sdpConstraints.mandatory = {
      OfferToReceiveAudio: true,
      OfferToReceiveVideo: true
    };
    if (studentId) {
      this.connection.peers[studentId].addStream({
        audio: true,
        screen: true,
        oneway: true
      });
    } else {
      this.connection.addStream({audio: true, screen: true, oneway: true});
    }
  }

  removeVideo(studentId?: number) {
    if (studentId) {
      this.connection.peers[studentId].removeStream('video');
    } else {
      this.connection.removeStream('video');
    }
  }

  removeAudio(studentId?: number) {
    if (studentId) {
      this.connection.peers[studentId].removeStream('audio');
    } else {
      this.connection.removeStream('audio');
    }
  }

  removeScreen(studentId?: number) {
    if (studentId) {
      this.connection.peers[studentId].removeStream('screen');
    } else {
      this.connection.removeStream('screen');
    }
  }

  stopStream() {
    this.connection.attachStreams.forEach((localStream) => {
      localStream.stop();
    });
  }

  disconnectWithAll() {
    this.connection.getAllParticipants().forEach((participant) => {
      this.connection.disconnectWith(participant);
    });
  }
}
