import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';

import { User } from '@models/user';
import { Role } from '@models/role';
import { Permission } from '@models/permission';


@Injectable()
export class UserService {
  
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
    // this.headers.set('responseType', 'text');
  }

  /* Permission Actions  */
  getAllPermissions() {
    this.url = this.adminBaseUrl + '/permission/all';
    return this.http.get(this.url);
  }

  getPermissionById(id: number) {
    this.url = this.adminBaseUrl + '/permission/getById/' + id;
    return this.http.get<Permission>(this.url);
  }

  getPermissionBySlug(slug: string) {
    this.url = this.adminBaseUrl + '/permission/getBySlug/' + slug;
    return this.http.get<Permission>(this.url);
  }

  createPermission(permission: Permission) {
    this.url = this.adminBaseUrl + '/permission/create';
    return this.http.post(this.url, permission, { headers: this.headers });
  }

  updatePermission(permission: Permission) {
    this.url = this.adminBaseUrl + '/permission/update/' + permission.id;
    return this.http.put(this.url, permission, { headers: this.headers });
  }

  deletePermission(id: number) {
    this.url = this.adminBaseUrl + '/permission/delete/' + id;
    return this.http.delete(this.url);
  }

  /* Role Actions  */
  getAllRoles() {
    this.url = this.adminBaseUrl + '/role/all';
    return this.http.get<Role[]>(this.url);
  }

  getRoleById(id: number) {
    this.url = this.adminBaseUrl + '/role/getById/' + id;
    return this.http.get<Role>(this.url);
  }

  getRoleBySlug(slug: string) {
    this.url = this.adminBaseUrl + '/role/getBySlug/' + slug;
    return this.http.get<Role>(this.url);
  }

  createRole(role: Role, permissions: number[]) {
    this.url = this.adminBaseUrl + '/role/create';
    return this.http.post(this.url, {
      role: role, permissions: permissions
    }, {
      headers: this.headers
    });
  }

  updateRole(role: Role, permissions: number[]) {
    this.url = this.adminBaseUrl + '/role/update/' + role.id;
    return this.http.put(this.url, {
      role: role, permissions: permissions
    }, {
        headers: this.headers
      });
  }

  activateRole(id: number) {
    this.url = this.adminBaseUrl + '/role/activate/' + id;
    return this.http.put(this.url, '');
  }

  deactivateRole(id: number) {
    this.url = this.adminBaseUrl + '/role/deactivate/' + id;
    return this.http.put(this.url, '');
  }

  deleteRole(id: number) {
    this.url = this.adminBaseUrl + '/role/delete/' + id;
    return this.http.delete(this.url);
  }

  /* User Actions  */
  getAllUsers() {
    this.url = this.adminBaseUrl + '/user/all';
    return this.http.get<User[]>(this.url);
  }

  getUserById(id: number) {
    this.url = this.adminBaseUrl + '/user/getById/' + id;
    return this.http.get<User>(this.url);
  }

  getUserByEmail(email: string) {
    this.url = this.adminBaseUrl + '/user/getByEmail/' + email;
    return this.http.get<User>(this.url);
  }

  getUsersByRole(roleId: number) {
    this.url = this.adminBaseUrl + '/user/getByRole/' + roleId;
    return this.http.get<User[]>(this.url);
  }

  createUser(user: User) {
    this.url = this.adminBaseUrl + '/user/create';
    return this.http.post(this.url, user, { headers: this.headers });
  }

  updateUser(user: User) {
    this.url = this.adminBaseUrl + '/user/update/' + user.id;
    return this.http.put(this.url, user, {headers: this.headers});
  }

  activateOrDeactivateUser(id: number) {
    this.url = this.adminBaseUrl + '/user/activateOrDeactivate/' + id;
    return this.http.put(this.url, '');
  }

  deleteUser(id: number) {
    this.url = this.adminBaseUrl + '/user/delete/' + id;
    return this.http.delete(this.url);
  }

  changePassword(user: User, oldPassword: string, newPassword: string) {
    this.url = this.adminBaseUrl + '/changePassword';
    let formData = {
      userId: user.id,
      oldPassword: oldPassword,
      newPassword: newPassword
    };
    return this.http.put(this.url, formData, { responseType: 'text' });
  }

}
