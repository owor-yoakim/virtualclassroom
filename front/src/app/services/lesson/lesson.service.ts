import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { Lesson } from '@models/lesson';
import { Instructor } from '@models/instructor';
import { LessonModule } from '@models/lesson-module';
import { AdminAppConstant } from '@admin/admin-app-constant';

@Injectable()
export class LessonService {
  private serverUrl: string = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AdminAppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) { 
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getAllLessons() {
    this.url = this.serverUrl + '/lesson/all';
    return this.http.get<Lesson[]>(this.url);
  }

  getLessonById(id: number) {
    this.url = this.serverUrl + '/lesson/getById/' + id;
    return this.http.get<Lesson>(this.url);
  }

  getLessonBySlug(slug: string) {
    this.url = this.serverUrl + '/lesson/getBySlug/' + slug;
    return  this.http.get<Lesson>(this.url);
  }

  getLessonsBySubject(subjectId: number) {
    this.url = this.serverUrl + '/lesson/getBySubject/' + subjectId;
    return this.http.get<Lesson[]>(this.url);
  }

  getLessonPrerequisites(lessonId: number) {
      this.url = this.serverUrl + '/lesson/getPrerequisites/' + lessonId;
      return this.http.get<Lesson[]>(this.url);
  }

  updateInstructorLesson(lesson: Lesson) {
      this.url = this.serverUrl + '/lesson/update/' + lesson.id;
      return this.http.put<Lesson>(this.url, lesson, {headers: this.headers});
  }

  getLessonModules(lessonId: number) {
    this.url = this.serverUrl + '/lesson/' + lessonId + '/module/all';
    return this.http.get<LessonModule[]>(this.url);
  }
  

  getLessonInstructors(lessonId: number) {
    this.url = this.serverUrl + '/lesson/getLessonInstructors/' + lessonId;
    return this.http.get<Instructor[]>(this.url);
  }

  numberOfLessons() {
    this.url = this.serverUrl + '/lesson/count';
    return this.http.get<number>(this.url);
  }

  countBySubject(subjectId: number) {
    this.url = this.serverUrl + '/lesson/countBySubject/' + subjectId;
    return this.http.get<number>(this.url);
  }

  arrangeLessons(lessons: Lesson[]): any[] {
    const arrangedLessons: any[] = [];
    let index = 0;
    let len = lessons.length;
    while (len) {
      const row: Lesson[] = [];
      if (len >= 3) {
        for (let i = 0; i < 3; i++) {
          row.push(lessons[index++]);
        }
        arrangedLessons.push(row);
        len -= 3;
      } else {
        for (; len > 0; len--) {
          row.push(lessons[index++]);
        }
        arrangedLessons.push(row);
      }
    }

    return arrangedLessons;
  }

  // admin actions
  createLesson(lesson: Lesson) {
    this.url = this.adminBaseUrl + '/lesson/create';
    return this.http.post(this.url, lesson, { headers: this.headers });
  }

  updateLesson(lesson: Lesson) {
    this.url = this.adminBaseUrl + '/lesson/update/' + lesson.id;
    return this.http.put(this.url, lesson, { headers: this.headers });
  }
  
  deleteLesson(lesson: Lesson) {
    this.url = this.adminBaseUrl + '/lesson/delete/' + lesson.id;
    return this.http.delete(this.url, {responseType: 'text' });
  }

}
