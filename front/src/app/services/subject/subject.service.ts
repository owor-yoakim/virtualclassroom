import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { Subject } from '@models/subject';

@Injectable()
export class SubjectService {
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
   }

  getAllSubjects() {
    this.url = this.serverUrl + '/subject/all';
    return this.http.get<Subject[]>(this.url);
  }

  getSubjectById(id: number) {
    this.url = this.serverUrl + '/subject/getById/' + id;
    return this.http.get<Subject>(this.url);
  }

  getSubjectBySlug(slug: string) {
    // console.log("Slug: ", slug);
    this.url = this.serverUrl + '/subject/getBySlug/' + slug;
    return this.http.get<Subject>(this.url);
  }

  createSubject(subject: Subject) {
    this.url = this.adminBaseUrl + '/subject/create';
    return this.http.post(this.url, subject, {headers: this.headers});
  }

  updateSubject(subject: Subject) {
    this.url = this.adminBaseUrl + '/subject/update/' + subject.id;
    return this.http.put(this.url, subject, { headers: this.headers });
  }

  deleteSubject(id: number) {
    this.url = this.adminBaseUrl + '/subject/delete/' + id;
    return this.http.delete(this.url);
  }

  activateSubject(subject: Subject) {
    this.url = this.adminBaseUrl + '/subject/activate/' + subject.id;
    return this.http.put(this.url, subject, {headers: this.headers});
  }

  deactivateSubject(subject: Subject) {
    this.url = this.adminBaseUrl + '/subject/deactivate/' + subject.id;
    return this.http.put(this.url, subject, {headers: this.headers});
  }


  numberOfSubjects() {
    this.url = this.serverUrl + '/subject/count';
    return this.http.get<number>(this.url);
  }
  
}
