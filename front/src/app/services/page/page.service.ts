import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { Page } from '@models/page';


@Injectable()
export class PageService {
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) { }

  getPage(pageSlug: string) {
    this.url = this.serverUrl + '/page/' + pageSlug;
    return this.http.get<Page>(this.url);
  }

}
