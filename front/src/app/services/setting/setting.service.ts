import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Setting } from '@models/setting';
import { AppConstant } from '@config/app-constant';

@Injectable()
export class SettingService {
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';
  private settings: Setting[] = [];

  constructor(private http: HttpClient) {
    this.getAll()
        .toPromise()
        .then((settings) => {
          this.settings = settings;
          // console.log("Settings: ", this.settings);
        })
        .catch((err) => {
          console.error(err);
        });
  }


  private getByKey(key: string) {
    const setting = this.settings.find(x => {
      return x.key === key;
    });
    return setting || null;

  }

  private getAll() {
    this.url = this.serverUrl + '/setting';
    return this.http.get<Setting[]>(this.url);
  }

  // get the setting value for the given key
  get(key: string) {
    const setting = this.getByKey(key);
    return (setting !== null ) ? setting.value : null;
  }

  getValue(key: string) {
    return new Promise((resolve, reject) => {
      const setting = this.getByKey(key);
      setTimeout(() => resolve((setting !== null) ? setting.value : null), 5000);
    });
  }

  // get the setting value for the given key
  getNumeric(key: string) {
    let setting = this.getByKey(key);
    if (setting !== null) {
      // console.log("Setting value: ", setting.value);
      return Number(setting.value);
    }
    return null;
  }

  set(key: string, value: string) {

  }

  remove(key: string) {

  }

  // admin actions
  createSetting(setting: Setting) {
    this.url = this.adminBaseUrl + 'setting/create';
    return this.http.post(this.url, setting, { headers: this.headers });
  }
  updateSetting(setting: Setting) {
    this.url = this.adminBaseUrl + 'setting/update/' + setting.id;
    return this.http.put(this.url, setting, { headers: this.headers });
  }

  deleteSetting(setting: Setting) {
    this.url = this.adminBaseUrl + 'setting/delete/' + setting.id;
    return this.http.delete(this.url);
  }

}
