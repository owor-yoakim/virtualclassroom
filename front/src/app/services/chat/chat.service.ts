import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { Chat } from '@models/chat';
import { Observable } from 'rxjs/Observable';
import { Lesson } from '@models/lesson';

@Injectable()
export class ChatService {
  serverUrl = AppConstant.serverUrl + '/site/api';
  adminBaseUrl = AppConstant.adminBaseUrl;
  headers = new HttpHeaders();
  url = '';
  chats: Chat[] = [];

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');

    this.getAll();
  }

  private async getAll() {
    this.url = this.serverUrl + '/chat/all';
    try {
      this.chats = await this.http.get<Chat[]>(this.url).toPromise();
    } catch (err) {
      console.error(err);
    }
  }

  getAllChats() {
    // this.getAll();
    return this.chats;
  }

  getChatById(id: number) {
    // this.getAll();
    let chat = this.chats.find((x) => {
      return x.id === id;
    });
    return chat || null;
  }

  getChatsByStudentId(studentId: number) {
    // this.getAll();
    return this.chats.filter((chat) => {
      return chat.studentId === studentId;
    });
  }

  getChatsByLesson(lesson: Lesson) {
    this.url = this.serverUrl + '/chat/getByLesson/' + lesson.id;
    return this.http.get<Chat[]>(this.url);
  }

  createChat(chat: Chat) {
    this.url = this.serverUrl + '/chat/create';
    delete chat.student;
    delete chat.lesson;
    delete chat.instructor;
    delete chat.createdAt;
    delete chat.updatedAt;
    delete chat.deletedAt;
    console.log(chat);
    return this.http.post<Chat>(this.url, chat, { headers: this.headers });
  }
}
