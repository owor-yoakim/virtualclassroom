import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AppConstant} from '@config/app-constant';
import {Instructor} from '@models/instructor';
import {Lesson} from '@models/lesson';

@Injectable()
export class InstructorService {
  
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
  }

  getAllInstructors() {
    this.url = this.serverUrl + '/instructor/all';
    return this.http.get<Instructor[]>(this.url);
  }


  getInstructorById(instructorId) {
    this.url = this.serverUrl + '/instructor/getById/' + instructorId;
    return this.http.get<Instructor>(this.url);
  }

  getInstructorProfile(id: number) {
    this.url = this.serverUrl + '/account/instructor/getById/' + id;
    return this.http.get<Instructor>(this.url);
  }

  changePassword(instructor: Instructor, oldPassword: string, newPassword: string) {
    this.url = this.serverUrl + '/account/instructor/changePassword';
    let formData = {
      instructorId: instructor.id,
      oldPassword: oldPassword,
      newPassword: newPassword
    };
    return this.http.put(this.url, formData, {headers: this.headers});
  }

  getInstructorLessons(instructorId: number) {
    this.url = this.serverUrl + '/instructor/getLessons/' + instructorId;
    return this.http.get<Lesson[]>(this.url);
  }

  getInstructorsBySubjectSlug(slug: string) {
    this.url = this.serverUrl + '/instructor/getInstructorsBySubjectSlug/' + slug;
    return this.http.get<Instructor[]>(this.url);
  }

  // admin actions
  createInstructor(instructor: Instructor) {
    this.url = this.adminBaseUrl + '/instructor/create';
    return this.http.post(this.url, instructor, {headers: this.headers});
  }

  updateInstructor(instructor: Instructor) {
    this.url = this.adminBaseUrl + '/instructor/update/' + instructor.id;
    return this.http.put(this.url, instructor, {headers: this.headers});
  }

  activateOrDeactivateInstructor(instructorId: number) {
    this.url = this.adminBaseUrl + '/instructor/activateOrDeactivate/' + instructorId;
    return this.http.put(this.url, '');
  }

  deleteInstructor(instructorId: number) {
    this.url = this.adminBaseUrl + '/instructor/delete/' + instructorId;
    return this.http.delete(this.url, {responseType: 'text' });
  }

  assignLesson(instructor: Instructor, lesson: Lesson) {
    this.url =
      this.adminBaseUrl + '/instructor/' + instructor.id + '/assignLesson/' + lesson.id;
    return this.http.put(this.url, '', {headers: this.headers});
  }

  removeLesson(instructor: Instructor, lesson: Lesson) {
    this.url = this.adminBaseUrl + '/instructor/' + instructor.id +
        '/removeLesson/' + lesson.id;
    return this.http.put(this.url, '', {headers: this.headers});
  }

}
