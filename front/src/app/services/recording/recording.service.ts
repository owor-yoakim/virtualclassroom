import { Injectable } from '@angular/core';

import * as RecordRTC from 'recordrtc';
import { AppConstant } from '@config/app-constant';

@Injectable()
export class RecordingService {

  private recorderOptions = AppConstant.recorderOptions;
  private recorder = null;

  constructor() { }

  initRecorder() {
    this.recorder.initRecorder();
  }

  startRecording(stream: MediaStream) {
    this.recorder = RecordRTC(stream, this.recorderOptions);
    this.recorder.startRecording();
  }

  stopRecording(fileName: string) {
    const recorder = this.recorder;
    recorder.stopRecording((dataURL) => {
      const recordedBlob = this.recorder.getBlob();
      // this.saveRecording(fileName);
      this.recorder.save(fileName + '.webm');
    });
  }

  pauseRecording() {
    // pause the recording
    this.recorder.pauseRecording();
  }


  resumeRecording() {
    // resume recording
    this.recorder.resumeRecording();
  }

  saveRecording(fileName: string) {
    this.recorder.save(fileName + '.mp4');
  }

  resetRecorder() {
    // this.recorder.clearRecordedData();
    this.recorder.reset();
  }

}
