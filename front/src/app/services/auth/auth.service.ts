import 'rxjs/add/operator/map';

import {AdminAppConstant} from '@admin/admin-app-constant';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AppConstant} from '@config/app-constant';
import {Instructor} from '@models/instructor';
import {LoginCredentials} from '@models/login-credentials';
import {User} from '@models/user';
import {tokenNotExpired} from 'angular2-jwt';
import {Base64} from 'js-base64';
import {Observable} from 'rxjs/Observable';



@Injectable()
export class AuthService {
  loggedIn = false;
  redirectUrl: string;
  private serverUrl: string = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AdminAppConstant.adminBaseUrl;
  private url = '';
  private headers = new HttpHeaders();


  constructor(private http: HttpClient, private router: Router) {
    this.headers.append('Content-Type', 'application/json');
  }


  isAuthenticated() {
    if (this.isStudentAuthenticated()) {
      return true;
    }
    if (this.isInstructorAuthenticated()) {
      return true;
    }
    return false;
  }


  getToken() {
    if (this.isStudentAuthenticated()) {
      return this.getStudentToken();
    } else if (this.isInstructorAuthenticated()) {
      return this.getInstructorToken();
    } else {
      return null;
    }
  }

  getUser() {
    if (this.isStudentAuthenticated()) {
      return this.getStudentUser();
    } else if (this.isInstructorAuthenticated()) {
      return this.getInstructorUser();
    } else {
      return null; 
    }
  }

  logout() {
    if (this.isStudentAuthenticated()) {
      this.studentLogout();
    } else if (this.isInstructorAuthenticated()) {
      this.instructorLogout();
    }
  }
  
  /*  Begin Student Auth Services */
// set student session data
  setStudentToken(token: string) {
    localStorage.setItem('studentAuthToken', token);
  }

  setStudentUser(SESSIONID: string) {
    localStorage.setItem('STUDENT_SESSION', SESSIONID);
  }

// get student session data
  getStudentToken() {
    return localStorage.getItem('studentAuthToken');
  }

  getStudentUser() {
    const data = localStorage.getItem('STUDENT_SESSION');
    if (data === null) {
      return null;
    }
    return <User>JSON.parse(Base64.decode(data));
  }

  // clear student session data
  removeStudentToken() {
    localStorage.removeItem('studentAuthToken');
  }

  removeStudentUser() {
    localStorage.removeItem('STUDENT_SESSION');
  }


  studentLogin(credentials: LoginCredentials) {
    this.url = this.serverUrl + '/login';
    const encodedData = Base64.encode(JSON.stringify(credentials));
    // console.log(encodedData);

    return this.http
        .post(this.url, {data: encodedData}, {headers: this.headers})
        .map((res: any) => {
          // console.log(res);
          if (res && res.token) {
            // login successful if there's a jwt token in the response
            const token = res.token;
            const SESSIONID = <string>res.SESSIONID;
            // console.log("Token: ", token);
            this.setStudentToken(token);
            this.setStudentUser(SESSIONID);
            return true;
          }
          return false;
        })
        .toPromise();
  }

  checkStudentSession() {
    this.url = this.serverUrl + '/checkStudentSession';
    let token = this.getStudentToken();
    return this.http.post(this.url, {token: token}, {headers: this.headers});
  }

  isStudentAuthenticated() {
    return tokenNotExpired('studentAuthToken');
  }

  studentLogout() {
    location.reload();
    this.removeStudentToken();
    this.removeStudentUser();
  }

  /*  End Student Auth Services */



  /*       Begin Instructor Auth Services  */

  setInstructorToken(token: string) {
    localStorage.setItem('instructorAuthToken', token);
  }

  setInstructorUser(SESSIONID: string) {
    localStorage.setItem('INSTRUCTOR_SESSION', SESSIONID);
  }

  getInstructorToken() {
    return localStorage.getItem('instructorAuthToken');
  }

  getInstructorUser() {
    const data = localStorage.getItem('INSTRUCTOR_SESSION');
    if (data === null) {
      return null;
    }
    return <User>JSON.parse(Base64.decode(data));
  }

  removeInstructorToken() {
    localStorage.removeItem('instructorAuthToken');
  }

  removeInstructorUser() {
    localStorage.removeItem('INSTRUCTOR_SESSION');
  }

  instructorLogin(credentials: LoginCredentials) {
    this.url = this.serverUrl + '/instructorLogin';
    const encodedData = Base64.encode(JSON.stringify(credentials));
    // console.log(encodedData);

    return this.http
        .post(this.url, {data: encodedData}, {headers: this.headers})
        .map((res: any) => {
          // console.log(res);
          if (res && res.token) {
            // login successful if there's a jwt token in the response
            const token = res.token;
            const SESSIONID = <string>res.SESSIONID;
            // console.log("Token: ", token);
            this.setInstructorToken(token);
            this.setInstructorUser(SESSIONID);
            return true;
          }
          return false;
        })
        .toPromise();
  }

  instructorLogout() {
    // clear session data
    location.reload();
    this.removeInstructorToken();
    this.removeInstructorUser();
  }


  checkInstructorSession() {
    this.url = this.serverUrl + '/checkInstructorSession';
    let token = this.getInstructorToken();
    return this.http.post(this.url, { token: token }, { headers: this.headers });
  }

  isInstructorAuthenticated() {
    return tokenNotExpired('instructorAuthToken');
  }

  /*       End Instructor Auth services  */



  /*       Begin Admin Auth Services  */

  /**
   * Admin login
   *
   * @param {string} email
   * @param {string} password
   * @returns
   * @memberof AuthService
   */
  adminLogin(credentials: LoginCredentials) {
    this.url = this.adminBaseUrl + '/login';
    return this.http
        .post(
            this.url,
            {email: credentials.loginName, password: credentials.loginPassword},
            {headers: this.headers})
        .map((res: any) => {
          console.log(res);
          if (res && res.token) {
            // login successful if there's a jwt token in the response
            const token = res.token;
            const SESSION = <string>res.SESSION;
            // console.log("Token: ", token);
            this.setAdminToken(token);
            this.setAdminUser(SESSION);
            return true;
          }
          return false;
        })
        .toPromise();
  }

  adminLogout() {
    // clear session data
    this.removeAdminToken();
    this.removeAdminUser();
    // only if  database is used for storage of session data
    // this.url = this.adminBaseUrl + '/logout';
    // return this.http.post(this.url, '');
    location.reload();
  }

  getAdminToken() {
    return localStorage.getItem('adminAuthToken');
  }

  setAdminToken(token: string) {
    localStorage.setItem('adminAuthToken', token);
  }

  getAdminUser() {
    const data = localStorage.getItem('ADMIN_SESSION');
    if (data === null) {
      return null;
    }
    return <User>JSON.parse(Base64.decode(data));
  }

  setAdminUser(SESSION: string) {
    localStorage.setItem('ADMIN_SESSION', SESSION);
  }

  removeAdminToken() {
    localStorage.removeItem('adminAuthToken');
  }

  removeAdminUser() {
    localStorage.removeItem('ADMIN_SESSION');
  }

  isAdminAuthenticated() {
    return tokenNotExpired('adminAuthToken');
  }

  checkAdminSession() {
    this.url = this.adminBaseUrl + '/checkSession';
    let token = this.getAdminToken();
    return this.http.post(this.url, {token: token}, {headers: this.headers});
  }


}
