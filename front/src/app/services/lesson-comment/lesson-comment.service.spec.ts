import { TestBed, inject } from '@angular/core/testing';

import { LessonCommentService } from './lesson-comment.service';

describe('LessonCommentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LessonCommentService]
    });
  });

  it('should be created', inject([LessonCommentService], (service: LessonCommentService) => {
    expect(service).toBeTruthy();
  }));
});
