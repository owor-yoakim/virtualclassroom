import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppConstant } from '@config/app-constant';
import { LessonComment } from '@models/lesson-comment';
import { LessonModule } from '@models/lesson-module';

@Injectable()
export class LessonCommentService {

  private serverUrl = AppConstant.serverUrl + '/site/api';
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
   }


  getLessonComments(lessonId: number) {
    this.url = this.serverUrl + '/lesson/' + lessonId + '/comment/all';
    return this.http.get<LessonComment[]>(this.url);
  }

  getLessonModuleComments(lessonModule: LessonModule) {
    this.url = this.serverUrl + '/lesson/' + lessonModule.lessonId + '/module/' + lessonModule.id + '/comment/all';
    return this.http.get<LessonComment[]>(this.url);
  }

  addComment(comment: LessonComment) {
    this.url = this.serverUrl + '/lesson/' + comment.lessonId + '/comment/create';
    return this.http.post(this.url, comment, { headers: this.headers });
  }

}
