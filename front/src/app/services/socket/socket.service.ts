import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { AppConstant } from '@config/app-constant';

@Injectable()
export class SocketService {

  private serverUrl: string = AppConstant.serverUrl;
  private socket: SocketIOClient.Socket = null;

  constructor() {
    this.socket = io.connect(this.serverUrl);
  }

  on(eventName: string, listener: Function) {
    if (this.socket) {
      this.socket.on(eventName, (data) => {
        listener(data);
      });
      return true;
    }
    return false;
  }

  emit(eventName: string, data: any) {
    if (this.socket) {
      this.socket.emit(eventName, data);
      return true;
    }
    return false;
  }

  sendToOne() {

  }

  removeListener(eventName: string, callback?: Function) {
    if (this.socket) {
      if (callback) {
        this.socket.removeListener(eventName, callback);
      } else {
        this.socket.removeListener(eventName);
      }
      return true;
    }
    return false;
  }

  disconnect() {

  }

}
