import { Injectable } from '@angular/core';
import { AppConstant } from '@config/app-constant';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Enrolment } from '@models/enrolment';

@Injectable()
export class EnrolmentService {
 
  adminBaseUrl = AppConstant.adminBaseUrl;
  serverUrl = AppConstant.serverUrl + '/site/api';
  headers = new HttpHeaders();
  url = '';

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Accept', 'application/json');
   }

   getEnrolment(studentId: number, lessonId: number) {
     this.url = this.adminBaseUrl + '/enrolment/getOne/' + studentId + '/' + lessonId;
     return this.http.get<Enrolment>(this.url);
   }

   getAllEnrolments() {
     this.url = this.adminBaseUrl + '/enrolment/all';
     return this.http.get<Enrolment[]>(this.url);
   }

  getByStudent(studentId: number) {
    this.url = this.adminBaseUrl + '/enrolment/getByStudent/' + studentId;
    return this.http.get<Enrolment[]>(this.url);
  }

  getByLesson(lessonId: number) {
    this.url = this.adminBaseUrl + '/enrolment/getByLesson/' + lessonId;
    return this.http.get<Enrolment[]>(this.url);
  }

  createEnrolment(enrolment: Enrolment) {
    this.url = this.adminBaseUrl + '/enrolment/create';
    return this.http.post(this.url, enrolment, { headers: this.headers });
  }

  updateEnrolment(enrolment: Enrolment){
    this.url = this.adminBaseUrl + '/enrolment/update/' + enrolment.studentId + '/' + enrolment.lessonId;
    return this.http.put(this.url, enrolment, { headers: this.headers });
  }

  deleteEnrolment(enrolment: Enrolment) {
    this.url = this.adminBaseUrl + '/enrolment/delete/' + enrolment.studentId +
      '/' + enrolment.lessonId;
    return this.http.delete(this.url);
  }

}
