import { Injectable } from '@angular/core';
import {
  Router,
  Route,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { AuthService } from '@services/auth/auth.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  checkLogin(redirectUrl: string): boolean {
    if (this.authService.isAdminAuthenticated()) {
      // Navigate to the administration
      this.router.navigateByUrl('/administration');
      return false;
    }

    if (this.authService.isAuthenticated()) {
      return true;
    }
    // Store the attempted URL for redirecting
    this.authService.redirectUrl = redirectUrl;

    // Navigate to the login page with extras
    this.router.navigateByUrl('/login');
    return false;
  }

  canLoad(route: Route): boolean {
    const url = `/${route.path}`;
    return this.checkLogin(url);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const redirectUrl: string = state.url;
    return this.checkLogin(redirectUrl);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

}
