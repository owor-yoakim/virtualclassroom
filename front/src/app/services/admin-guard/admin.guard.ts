import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '@services/auth/auth.service';

@Injectable()
export class AdminGuard implements CanActivate {
   constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  checkLogin(redirectUrl: string): boolean {
    if (this.authService.isAdminAuthenticated()) {
      return true;
    }
    // Store the attempted URL for redirecting
    this.authService.redirectUrl = redirectUrl;

    // Navigate to the login page with extras
    this.router.navigate(['/administration/login']);
    return false;
  }

  canLoad(route: Route): boolean {
    const url = `/${route.path}`;
    return this.checkLogin(url);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const redirectUrl: string = state.url;
    return this.checkLogin(redirectUrl);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
