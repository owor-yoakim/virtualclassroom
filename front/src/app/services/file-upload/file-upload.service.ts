import { Injectable } from '@angular/core';
import { 
  HttpClient, 
  HttpHeaders, 
  HttpRequest } from '@angular/common/http';
import { AuthService } from '@services/auth/auth.service';
import { LessonFile } from '@models/lesson-file';
import { AppConstant } from '@config/app-constant';
import { Lesson } from '@models/lesson';


@Injectable()
export class FileUploadService {
  private url = AppConstant.serverUrl + '/site/api';
  private headers: HttpHeaders;

  constructor(
    public authService: AuthService,
    private http: HttpClient,
  ) {
    this.headers = new HttpHeaders();
    this.headers.append('Content-Type', 'multipart/form-data');
   }

  private prepareUpload(lessonFile: LessonFile) {
    let formData = new FormData();
    formData.append('name', lessonFile.name);
    formData.append('description', lessonFile.description);
    formData.append('type', lessonFile.type);
    formData.append('size', '' + lessonFile.size);
    formData.append('path', lessonFile.path);
    formData.append('instructorId', '' + lessonFile.instructorId);
    formData.append('lessonId', '' + lessonFile.lessonId);
    formData.append('studentId', '' + lessonFile.studentId);

    return formData;
  }

  
  uploadFile(uploadUrl: string, lessonFile: LessonFile, file: File, lesson: Lesson) {
    let formData = this.prepareUpload(lessonFile);
    formData.append('fileInput', file);
    formData.append('lessonSlug', '' + lesson.slug);

    const httpRequest = new HttpRequest(
        'POST', this.url + uploadUrl, formData,
        {headers: this.headers, reportProgress: true, responseType: 'text'});

    return this.http.request(httpRequest);
    
  }

}
