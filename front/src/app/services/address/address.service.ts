import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AppConstant} from '@config/app-constant';
import {Country} from '@models/country';

@Injectable()
export class AddressService {
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private countries: Country[] = [];

  constructor(private http: HttpClient) {
    this.getAll();
  }

  private async getAll() {
    try {
      this.countries = await this.getAllCountries().toPromise();
      // console.log("Countries: ", this.countries);
    } catch (err) {
      console.error(err);
    }
  }

  getAllCountries() {
    const url = this.serverUrl + '/country/all';
    return this.http.get<Country[]>(url);
  }

  getById(id: number) {
    const country = this.countries.find(x => {
      return x.id === id;
    });
    return country || null;
  }

  getByCode(code: string) {
    const country = this.countries.find(x => {
      return x.code === code;
    });
    return country || null;
  }
}
