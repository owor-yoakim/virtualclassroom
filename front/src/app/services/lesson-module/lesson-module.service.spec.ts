import { TestBed, inject } from '@angular/core/testing';

import { LessonModuleService } from './lesson-module.service';

describe('LessonModuleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LessonModuleService]
    });
  });

  it('should be created',
     inject([LessonModuleService], (service: LessonModuleService) => {
       expect(service).toBeTruthy();
     }));
});
