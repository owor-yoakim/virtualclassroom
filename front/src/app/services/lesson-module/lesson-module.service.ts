import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LessonModule } from '@models/lesson-module';
import { AppConstant } from '@config/app-constant';

@Injectable()
export class LessonModuleService {
  private adminBaseUrl = AppConstant.adminBaseUrl;
  private serverUrl = AppConstant.serverUrl + '/site/api';
  private headers = new HttpHeaders();
  private url = '';

  constructor(private http: HttpClient) {
    this.headers.set('Content-Type', 'application/json');
    this.headers.set('Accept', 'application/json');
  }

  getLessonModuleById(lessonId: number, lessonModuleId: number) {
    this.url = this.adminBaseUrl + '/lesson/' + lessonId + '/module/' + lessonModuleId;
    return this.http.get<LessonModule>(this.url);
  }

  getLessonModules(lessonId: number) {
    this.url = this.adminBaseUrl + '/lesson/' + lessonId + '/module/all';
    return this.http.get<LessonModule[]>(this.url);
  }

  createLessonModule(lessonModule: LessonModule) {
    this.url = this.adminBaseUrl + '/lesson/' + lessonModule.lessonId + '/module/create';
    return this.http.post(this.url, lessonModule, {headers: this.headers});
  }

  updateLessonModule(lessonModule: LessonModule) {
    this.url = this.adminBaseUrl + '/lesson/' + lessonModule.lessonId +
        '/module/update/' + lessonModule.id;
    return this.http.put(this.url, lessonModule, {headers: this.headers});
  }

  deleteLessonModule(lessonModule: LessonModule) {
    this.url = this.adminBaseUrl + '/lesson/' + lessonModule.lessonId +
      '/module/delete/' + lessonModule.id;
    return this.http.delete(this.url, {responseType: 'text' });
  }

}
