import {AdminContainerComponent} from '@admin/admin-container/admin-container.component';
import {AdminNotFoundComponent} from '@admin/admin-not-found/admin-not-found.component';
import {AdminRoutingModule} from '@admin/admin-rounting.module';
import {AdminInstructorModule} from '@admin/instructor/admin-instructor.module';
import {AdminSubjectModule} from '@admin/subject/admin-subject.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {SharedModule} from '@shared/shared.module';
import { AdminLoginComponent } from '@admin/login/admin-login.component';
import { AdminNavbarComponent } from '@admin/admin-navbar/admin-navbar.component';
import { AdminEnrolmentModule } from '@admin/enrolment/admin-enrolment.module';
import { AdminStudentModule } from '@admin/student/admin-student.module';
import { AdminUserAccountModule } from '@admin/admin-user-account/admin-user-account.module';
import { AdminUserModule } from '@admin/user/admin-user.module';



@NgModule({
  imports: [
    SharedModule,
    AdminRoutingModule,
    AdminUserAccountModule,
    AdminSubjectModule,
    AdminInstructorModule,
    AdminStudentModule,
    AdminEnrolmentModule,
    AdminUserModule
  ],
  declarations: [
    AdminNavbarComponent,
    AdminContainerComponent,
    AdminLoginComponent,
    AdminNotFoundComponent
  ],
  exports: [AdminNavbarComponent]
})
export class AdminModule { }
