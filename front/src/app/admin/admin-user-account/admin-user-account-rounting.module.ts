import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { AdminUserAccountContainerComponent } from '@admin/admin-user-account/admin-user-account-container/admin-user-account-container.component';
import { AdminGuard } from '@services/admin-guard/admin.guard';
import { AdminUserProfileComponent } from '@admin/admin-user-account/admin-user-profile/admin-user-profile.component';


const adminUserAccountRoutes: Routes = [{
  path: '',
  component: AdminUserAccountContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    { path: '', redirectTo: 'profile', pathMatch: 'full' },
    { path: 'profile', component: AdminUserProfileComponent }
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(adminUserAccountRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminUserAccountRoutingModule { }
