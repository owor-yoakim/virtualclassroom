import { Component, OnInit } from '@angular/core';
import { User } from '@models/user';
import { AuthService } from '@services/auth/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { passwordsMatcher } from '@helpers/passwords-matcher';
import { UserService } from '@services/user/user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-admin-change-password',
  templateUrl: './admin-change-password.component.html',
  styleUrls: ['./admin-change-password.component.css']
})
export class AdminChangePasswordComponent implements OnInit {
  user: User = null;
  changePasswordForm: FormGroup;

  oldPassword = '';
  newPassword = '';
  
  constructor(public authService: AuthService, private userService: UserService, private fb: FormBuilder) { }

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: this.fb.group(
        {
          password1: ['', [Validators.required, Validators.maxLength(6)]],
          password2: ['', [Validators.required]]
        },
        { validator: passwordsMatcher })
    });
  }


  async changePassword() {
    try {
      let response;
      if (this.user !== null) {
        response = await this.userService
          .changePassword(this.user, this.oldPassword, this.newPassword)
          .toPromise();
        console.log(response);
        await swal({ title: 'Response Status', text: response, type: 'success' });
      } else {
        await swal({ title: 'Error', text: 'Invalid User', type: 'warning' });
      }
    } catch (error) {
      console.error(error);
      await swal({ title: 'Response Status', text: error.error, type: 'error' });
    }
    this.changePasswordForm.reset();
    this.oldPassword = '';
    this.newPassword = '';
  }

}
