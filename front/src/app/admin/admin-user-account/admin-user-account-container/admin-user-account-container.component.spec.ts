import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserAccountContainerComponent } from './admin-user-account-container.component';

describe('AdminUserAccountContainerComponent', () => {
  let component: AdminUserAccountContainerComponent;
  let fixture: ComponentFixture<AdminUserAccountContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUserAccountContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserAccountContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
