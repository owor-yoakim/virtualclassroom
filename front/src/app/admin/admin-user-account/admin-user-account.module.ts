import {AdminChangePasswordComponent} from '@admin/admin-user-account/admin-change-password/admin-change-password.component';
// tslint:disable-next-line:max-line-length
import {AdminUserAccountContainerComponent} from '@admin/admin-user-account/admin-user-account-container/admin-user-account-container.component';
import {AdminUserAccountRoutingModule} from '@admin/admin-user-account/admin-user-account-rounting.module';
import {AdminUserProfileComponent} from '@admin/admin-user-account/admin-user-profile/admin-user-profile.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  imports: [CommonModule, SharedModule, AdminUserAccountRoutingModule],
  declarations: [
    AdminUserAccountContainerComponent, AdminUserProfileComponent,
    AdminChangePasswordComponent
  ],
  exports: [AdminChangePasswordComponent]
})
export class AdminUserAccountModule {}
