import { Component, OnInit } from '@angular/core';
import { User } from '@models/user';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-admin-user-profile',
  templateUrl: './admin-user-profile.component.html',
  styleUrls: ['./admin-user-profile.component.css']
})
export class AdminUserProfileComponent implements OnInit {
  user$: Observable<User>;
  loggedInUser: User = null;

  constructor(public authService: AuthService, private userService: UserService) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getAdminUser();
    this.user$ = this.userService.getUserById(this.loggedInUser.id);
  }

}
