import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { User } from '@models/user';
import { Role } from '@models/role';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users$: Observable<User[]>;
  filteredUsers$: Observable<User[]>;
  roles$: Observable<Role[]>;
  loggedInUser: User = null;
  roleId = 0;
  

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getAdminUser();
    this.filteredUsers$ = this.users$ = this.userService.getAllUsers();
    this.roles$ = this.userService.getAllRoles();
  }

  async onActivateOrDeactivateUser(user: User) {
    try {
      let response: any = await this.userService.activateOrDeactivateUser(user.id).toPromise();
      await swal({
        title: 'Response Status',
        text: 'Status Updated Successfully',
        type: 'success',
        timer: 3000
      });
      this.users$ = this.userService.getAllUsers();
      this.onFilterUsersByRole();
    } catch (error) {
      console.error(error);
    }
  }

  onFilterUsersByRole() {
    this.roleId = Number(this.roleId);
    console.log(this.roleId);
    if (this.roleId > 0) {
      // this.users$ = this.userService.getUsersByRole(this.roleId);
      this.filteredUsers$ = this.users$.map(users => users.filter(user => user.roleId === this.roleId));
    } else {
      this.filteredUsers$ = this.users$ = this.userService.getAllUsers();
    }
  }

  createUser() {
    this.router.navigateByUrl('/administration/users/create');
  }

  editUser(user: User) {
    this.router.navigateByUrl('/administration/users/update/' + user.id);
  }

  viewUser(user: User) {
    this.router.navigateByUrl('/administration/users/show/' + user.id);
  }

  async deleteUser(user: User) {
    if (user.id !== this.loggedInUser.id) {
      try {
        let confirm = await swal({
          title: 'Are you sure?',
          text: 'The changes can\'t be undone.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Delete',
          cancelButtonText: 'Cancel'
        });
        if (confirm.value) {
          let response = await this.userService.deleteUser(user.id).toPromise();
          await swal({
            title: 'Response Status',
            text: 'User deleted successfully.',
            type: 'success'
          });
          this.users$ = this.userService.getAllUsers();
          this.onFilterUsersByRole();
        }
      } catch (error) {
        console.error(error);
        await swal({ title: 'Response Status', text: error.error, type: 'error' });
      }
    }
  }

}
