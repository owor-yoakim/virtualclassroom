import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';

@Component({
  selector: 'app-permission-show',
  templateUrl: './permission-show.component.html',
  styleUrls: ['./permission-show.component.css']
})
export class PermissionShowComponent implements OnInit {
  loading = true;
  title = 'Permission Details';

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;
    this.loading = false;
  }

  backToList() {
    this.router.navigateByUrl('/administration/users/permissions');
  }

}
