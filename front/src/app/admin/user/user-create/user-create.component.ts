import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {passwordsMatcher} from '@helpers/passwords-matcher';
import {Address} from '@models/address';
import {Role} from '@models/role';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {UserService} from '@services/user/user.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  user: User;
  loggedInUser: User = null;
  emailSent = false;
  emailExists = false;

  roles$: Observable<Role[]>;
  now = (new Date).toISOString();
  userForm: FormGroup;

  constructor(
      private fb: FormBuilder, public authService: AuthService,
      private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.user = new User();
    this.user.active = false;
    this.user.address = new Address();
    this.loggedInUser = this.authService.getAdminUser();
    this.roles$ = this.userService.getAllRoles();
    this.createForm();
  }

  createForm() {
    this.userForm =
        this.fb.group({
          roleId: ['', [Validators.required]],
          firstName: ['', [Validators.required, Validators.minLength(3)]],
          lastName: ['', [Validators.required, Validators.minLength(3)]],
          emailAddress: ['', [Validators.required, Validators.email]],
          username: ['', [Validators.required, Validators.minLength(5)]],
          joinDate: ['', [Validators.required]],
          userStatus: ['', []],
          phoneNumber: ['', [Validators.pattern('[0-9]+')]],
          userAddress: this.fb.group({
            addressType: ['', [Validators.required]],
            addressLine1: ['', [Validators.required, Validators.minLength(5)]],
            addressLine2: ['', [Validators.required, Validators.minLength(5)]],
            city: ['', [Validators.required, Validators.minLength(3)]],
            state: ['', [Validators.required, Validators.minLength(3)]],
            country: ['', [Validators.required, Validators.minLength(2)]],
            zip: ['', [Validators.required, Validators.minLength(3)]]
          }),
          userPassword:
              this.fb.group(
                  {
                    password1:
                        ['', [Validators.required, Validators.minLength(5)]],
                    password2: ['', [Validators.required]]
                  },
                  {validator: passwordsMatcher})
        });
  }


  async createUser() {
    console.log(this.user);
    try {
      let response = await this.userService.createUser(this.user).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'User Created Successfully',
        type: 'success',
        timer: 3000
      });
      this.user = new User();
      this.user.active = false;
      this.user.address = new Address();
      this.userForm.reset();
    } catch (error) {
      swal({
        title: 'Response Status',
        text: error.error,
        type: 'error'
      });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/users');
  }
}
