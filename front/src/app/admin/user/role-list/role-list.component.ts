import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';
import { Role } from '@models/role';
import { Permission } from '@models/permission';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';


@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {

  loading = true;
  roles: Role[] = [];
  permissions: Permission[] = [];
  title = 'User Roles';

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.getAllRoles();

    this.loading = false;
  }

  getAllRoles() {
    this.userService.getAllRoles().subscribe(
      (data) => {
        this.roles = <Role[]>data;
      },
      (err) => {
        console.error(err.text());
        this.router.navigateByUrl('/administration/users');
      }
    );
  }

  createRole() {
    this.router.navigateByUrl('/administration/users/roles/create');
  }

  viewRole(id: number) {
    this.router.navigateByUrl('/administration/users/roles/show/' + id);
  }

  editRole(id: number) {
    this.router.navigateByUrl('/administration/users/roles/update/' + id);
  }

  deleteRole(id: number) {
    console.log(id);
  }

}
