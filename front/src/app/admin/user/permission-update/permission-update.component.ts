import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { Permission } from '@models/permission';
import { UserService } from '@services/user/user.service';
import { AuthService } from '@services/auth/auth.service';

@Component({
  selector: 'app-permission-update',
  templateUrl: './permission-update.component.html',
  styleUrls: ['./permission-update.component.css']
})
export class PermissionUpdateComponent implements OnInit {
  title = 'Permission: ';
  loading = true;
  permission: Permission = new Permission();
  permissionForm: FormGroup;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;

    this.route.paramMap.subscribe((params) => {
      let id = Number(params.get('permissionId')) || 0;
      this.getPermissionById(id);
    });


    this.permissionForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', [])
    });

    this.loading = false;
  }

  getPermissionById(id: number) {
    this.userService.getPermissionById(id).subscribe(
        (permission) => {
          console.log(permission);
          this.permission = permission;
        },
        (err) => {
          console.error(err.text());
        });
  }

  updatePermission() {
    this.userService.updatePermission(this.permission).subscribe(
      (data) => {
        console.log(data);
        this.permission = <Permission>data;
      }, (err) => {
        console.error(err.text());
      }
    );
  }

  onTitleKeyup() {
    let str: string = this.permission.title;
    this.permission.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/permission');
  }

}
