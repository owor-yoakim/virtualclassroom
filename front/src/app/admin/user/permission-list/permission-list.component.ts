import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { Permission } from '@models/permission';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';


@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.css']
})
export class PermissionListComponent implements OnInit {
  loading = true;
  permissions: Permission[] = [];
  title = 'Role Permissions';

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.getAllPermissions();

    this.loading = false;
  }

  getAllPermissions() {
    this.userService.getAllPermissions().subscribe(
      (data) => {
        this.permissions = <Permission[]>data;
      },
      (err) => {
        console.error(err.text());
        this.router.navigateByUrl('/');
      }
    );
  }

  viewPermission(id: number) {
    this.router.navigateByUrl('/administration/users/permissions/show/' + id);
  }

  editPermission(id?: number) {
    if (id) {
      this.router.navigateByUrl('/administration/users/permissions/create/' + id);
    } else {
      this.router.navigateByUrl('/administration/users/permissions/create');
    }
  }

  deletePermission(id: number) {
    console.log(id);
  }

}
