import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { Role } from '@models/role';
import { Permission } from '@models/permission';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';

@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.css']
})
export class RoleCreateComponent implements OnInit {
  title = 'Create User Role';
  loading = true;
  role: Role = new Role();
  permissions: Permission[] = [];
  rolePerms: number[] = [];
  roleForm: FormGroup;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router
  ) { }


  ngOnInit() {
    this.loading = true;

    this.getAllPermissions();

    this.roleForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      permission: new FormControl('', [Validators.required])
    });

    this.loading = false;
  }

  getAllPermissions() {
    this.userService.getAllPermissions().subscribe(
      (data) => {
        this.permissions = <Permission[]>data;
      },
      (err) => {
        console.error(err.text());
      }
    );
  }

  createRole() {
    this.userService.createRole(this.role, this.rolePerms).subscribe(
      (data) => {
        console.log(data);
        this.role = new Role();
        this.rolePerms = [];
      }, (err) => {
        console.error(err.text());
      }
    );
  }

  onTitleKeyup() {
    let str: string = this.role.title;
    this.role.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/users/roles');
  }

  permissionChanged(id: number) {
    // console.log("Before: ", this.rolePerms);
    let len = this.rolePerms.length;
    this.rolePerms = this.rolePerms.filter((value) => {
      // permission found
      return (value !== id)
    });
    // permission not found
    if (len === this.rolePerms.length) {
      // add the permission
      this.rolePerms.push(id);
    }
    // console.log("After: ", this.rolePerms);
  }

}
