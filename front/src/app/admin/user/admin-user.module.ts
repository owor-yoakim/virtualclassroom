import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';

import { ProfileComponent } from './profile/profile.component';

import { UserListComponent } from './user-list/user-list.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserShowComponent } from './user-show/user-show.component';

import { PermissionListComponent } from './permission-list/permission-list.component';
import { PermissionCreateComponent } from './permission-create/permission-create.component';
import { PermissionUpdateComponent } from './permission-update/permission-update.component';
import { PermissionShowComponent } from './permission-show/permission-show.component';

import { RoleListComponent } from './role-list/role-list.component';
import { RoleCreateComponent } from './role-create/role-create.component';
import { RoleUpdateComponent } from './role-update/role-update.component';
import { RoleShowComponent } from './role-show/role-show.component';
import { AdminUserRoutingModule } from '@admin/user/admin-user-rounting.module';
import { AdminUserContainerComponent } from '@admin/user/admin-user-container/admin-user-container.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminUserRoutingModule
  ],
  declarations: [
    AdminUserContainerComponent,
    ProfileComponent,
    UserListComponent,
    UserCreateComponent,
    UserUpdateComponent,
    UserShowComponent,
    RoleListComponent,
    RoleCreateComponent,
    RoleUpdateComponent,
    RoleShowComponent,
    PermissionListComponent,
    PermissionCreateComponent,
    PermissionUpdateComponent,
    PermissionShowComponent
  ],
  providers: [],
  exports: []
})
export class AdminUserModule { }
