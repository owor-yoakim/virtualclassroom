import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { AuthService } from '@services/auth/auth.service';

@Component({
  selector: 'app-role-show',
  templateUrl: './role-show.component.html',
  styleUrls: ['./role-show.component.css']
})
export class RoleShowComponent implements OnInit {
  loading = true;

  constructor(
    public authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.loading = false;
  }

  backToList(){
    this.router.navigateByUrl('/administration/users/roles');
  }

}
