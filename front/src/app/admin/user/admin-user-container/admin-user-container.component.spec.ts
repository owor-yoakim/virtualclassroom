import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserContainerComponent } from './admin-user-container.component';

describe('AdminUserContainerComponent', () => {
  let component: AdminUserContainerComponent;
  let fixture: ComponentFixture<AdminUserContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUserContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
