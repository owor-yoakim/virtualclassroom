import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { Role } from '@models/role';
import { Permission } from '@models/permission';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';

@Component({
  selector: 'app-role-update',
  templateUrl: './role-update.component.html',
  styleUrls: ['./role-update.component.css']
})
export class RoleUpdateComponent implements OnInit {

  title = 'Update User Role';
  loading = true;

  role: Role = null;
  permissions: Permission[] = [];
  rolePerms: number[] = [];

  roleForm: FormGroup;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }


  ngOnInit() {
    this.loading = true;

    this.route.paramMap.subscribe((params) => {
      let id = Number(params.get('roleId')) || 0;
      this.getRole(id);
    });

    this.getAllPermissions();

    this.roleForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      permission: new FormControl('', [Validators.required])
    });

    this.loading = false;
  }

  getRole(id: number) {
    this.userService.getRoleById(id).subscribe(
      (role) => {
        this.role = role;
        this.role.permissions.forEach((permission) => {
          this.rolePerms.push(permission.id);
        });
      },
      (err) => {
        console.error(err.text());
      }
    );
  }

  getAllPermissions() {
    this.userService.getAllPermissions().subscribe(
      (data) => {
        this.permissions = <Permission[]>data;
      },
      (err) => {
        console.error(err.text());
      }
    );
  }

  updateRole() {
    this.userService.updateRole(this.role, this.rolePerms).subscribe(
      (data) => {
        console.log(data);
        this.role = <Role>data;
        // this.rolePerms = [];
      }, (err) => {
        console.error(err.text());
      }
    );
  }

  onTitleKeyup() {
    let str: string = this.role.title;
    this.role.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/users/roles');
  }

  permissionChanged(id: number) {
    // console.log("Before: ", this.rolePerms);
    let len = this.rolePerms.length;
    this.rolePerms = this.rolePerms.filter((value) => {
      // permission found
      return (value !== id);
    });
    // permission not found
    if (len === this.rolePerms.length) {
      // add the permission
      this.rolePerms.push(id);
    }
    // console.log("After: ", this.rolePerms);
  }

}
