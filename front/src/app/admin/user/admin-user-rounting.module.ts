import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AdminGuard } from '@services/admin-guard/admin.guard';
import { AdminUserContainerComponent } from '@admin/user/admin-user-container/admin-user-container.component';
import { UserListComponent } from '@admin/user/user-list/user-list.component';
import { UserCreateComponent } from '@admin/user/user-create/user-create.component';
import { UserUpdateComponent } from '@admin/user/user-update/user-update.component';
import { UserShowComponent } from '@admin/user/user-show/user-show.component';
import { RoleListComponent } from '@admin/user/role-list/role-list.component';
import { RoleCreateComponent } from '@admin/user/role-create/role-create.component';
import { RoleUpdateComponent } from '@admin/user/role-update/role-update.component';
import { RoleShowComponent } from '@admin/user/role-show/role-show.component';
import { PermissionListComponent } from '@admin/user/permission-list/permission-list.component';
import { PermissionCreateComponent } from '@admin/user/permission-create/permission-create.component';
import { PermissionShowComponent } from '@admin/user/permission-show/permission-show.component';
import { PermissionUpdateComponent } from '@admin/user/permission-update/permission-update.component';


const adminUserRoutes: Routes = [{
  path: '',
  component: AdminUserContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: UserListComponent},
    {path: 'create', component: UserCreateComponent},
    {path: 'update/:userId', component: UserUpdateComponent},
    { path: 'show/:userId', component: UserShowComponent },
    {
      path: 'roles',
      component: RoleListComponent,
      children: [
      {path: '', component: RoleListComponent},
        {path: 'create', component: RoleCreateComponent},
        {path: 'update/:roleId', component: RoleUpdateComponent},
        {path: 'show/:roleId', component: RoleShowComponent},
      ]
    },
    {
      path: 'permissions',
      component: PermissionListComponent,
      children: [
        {path: '', component: PermissionListComponent},
        {path: 'create', component: PermissionCreateComponent},
        {path: 'update/:permissionId', component: PermissionUpdateComponent},
        {path: 'show/:permissionId', component: PermissionShowComponent},
      ]
    }
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminUserRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminUserRoutingModule { }
