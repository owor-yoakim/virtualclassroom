import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { Permission } from '@models/permission';
import { AuthService } from '@services/auth/auth.service';
import { UserService } from '@services/user/user.service';
import { User } from '@models/user';


@Component({
  selector: 'app-permission-create',
  templateUrl: './permission-create.component.html',
  styleUrls: ['./permission-create.component.css']
})
export class PermissionCreateComponent implements OnInit {
  title = 'Permission Info';
  user: User = null;
  loading = true;
  sending = false;
  permission: Permission = new Permission();
  permissionForm: FormGroup;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;
    this.permissionForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', [])
    });

    this.route.paramMap.subscribe((params) => {
      // Defaults to 0 if no query param provided.
      let id = Number(params.get('permissionId')) || 0;
      if (id > 0) {
        this.getPermission(id);
      }
    });

    this.user = this.authService.getUser();

    this.loading = false;
  }

  onSubmit() {
    this.sending = true;
    // console.log(this.permission);
    if (this.permission.id !== undefined) {
      // uppdate
      this.updatePermission();
    } else {
      // insert
      this.createPermission();
    }
    this.sending = false;
  }

  createPermission() {
    this.userService.createPermission(this.permission).subscribe(
      (data) => {
        console.log(data);
        this.permission = new Permission();
      }, (err) => {
        console.error(err.text());
      }
    );
  }

  updatePermission() {
    this.userService.updatePermission(this.permission).subscribe(
      (data) => {
        console.log(data);
        this.permission = <Permission>data;
      }, (err) => {
        console.error(err.text());
      }
    );
  }

  getPermission(id: number) {
    this.userService.getPermissionById(id).subscribe(
      (permission) => {
        console.log(permission);
        this.permission = permission;
      },
      (err) => {
        console.error(err.text());
      });
  }

  onTitleKeyup() {
    let str: string = this.permission.title;
    this.permission.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/users/permissions');
  }

}
