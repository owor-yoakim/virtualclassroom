import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {UserService} from '@services/user/user.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';


@Component({
  selector: 'app-user-show',
  templateUrl: './user-show.component.html',
  styleUrls: ['./user-show.component.css']
})
export class UserShowComponent implements OnInit {
  user$: Observable<User>;
  loggedInUser: User = null;
  userId = 0;

  constructor(
      public authService: AuthService, private userService: UserService,
      private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.loggedInUser = this.authService.getAdminUser();
    this.route.paramMap.subscribe((params) => {
      this.userId = Number(params.get('userId')) || 0;
      if (this.userId > 0) {
        this.user$ = this.userService.getUserById(this.userId);
      } else {
        swal({title: 'Error', text: 'Unknown User', type: 'error'});
        this.backToList();
      }
    });
  }

  editUser(user: User) {
    this.router.navigateByUrl('/administration/users/update/' + user.id);
  }

  backToList() {
    this.router.navigateByUrl('/administration/users');
  }

}
