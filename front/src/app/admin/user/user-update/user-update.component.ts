import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { passwordsMatcher } from '@helpers/passwords-matcher';
import { User } from '@models/user';
import { Role } from '@models/role';
import { Address } from '@models/address';
import { UserService } from '@services/user/user.service';
import { AuthService } from '@services/auth/auth.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {
  user: User = null;
  loggedInUser: User = null;
  roles$: Observable<Role[]>;
  userId = 0;
  emailSent = false;
  emailExists = false;
  userForm: FormGroup;
  now = (new Date).toISOString();

  constructor(
    private fb: FormBuilder,
    public userService: UserService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loggedInUser = this.authService.getAdminUser();
    this.createForm();
    this.route.paramMap.subscribe(async (params) => {
      this.userId = Number(params.get('userId')) || 0;
      if (this.userId > 0) {
        try {
          this.user = await this.userService.getUserById(this.userId).toPromise();
          this.roles$ = this.userService.getAllRoles();
          if (this.user.address === null) {
            this.user.address = new Address();
          }
          if (this.user.active === null) {
            this.user.active = false;
          }
        } catch (error) {
          console.error(error);
          await swal({
            title: 'Error',
            text: error.error,
            type: 'error'
          });
          this.backToList();
        }
      } else {
        await swal({
          title: 'Error',
          text: 'Unknown User',
          type: 'error'
        });
        this.backToList();
      }
    });
  }

  createForm() {
    this.userForm = this.fb.group({
      roleId: ['', [Validators.required]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      emailAddress: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(5)]],
      joinDate: ['', [Validators.required]],
      userStatus: ['', []],
      phoneNumber: ['', [Validators.pattern('[0-9]+')]],
      userAddress: this.fb.group({
        addressType: ['', [Validators.required]],
        addressLine1: ['', [Validators.required, Validators.minLength(5)]],
        addressLine2: ['', [Validators.required, Validators.minLength(5)]],
        city: ['', [Validators.required, Validators.minLength(3)]],
        state: ['', [Validators.required, Validators.minLength(3)]],
        country: ['', [Validators.required, Validators.minLength(2)]],
        zip: ['', [Validators.required, Validators.minLength(3)]]
      })
    });
  }

  async updateUser() {
    console.log(this.user);
    this.user.password = null;
    try {
      let response = await this.userService.updateUser(this.user).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'User Updated Successfully',
        type: 'success'
      });
      this.user = <User>response;
    } catch (error) {
      console.error(error);
      await swal({ title: 'Response Status', text: error.error, type: 'error' });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/users');
  }

}
