import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminContainerComponent } from '@admin/admin-container/admin-container.component';
import { AdminNotFoundComponent } from '@admin/admin-not-found/admin-not-found.component';
import { AdminLoginComponent } from '@admin/login/admin-login.component';
import { AdminGuard } from '@services/admin-guard/admin.guard';


const adminRoutes: Routes = [{
  path: '',
  component: AdminContainerComponent,
  children: [
    {path: '', redirectTo: 'subjects', pathMatch: 'full'},
    {path: 'login', component: AdminLoginComponent}, {
      path: 'subjects',
      loadChildren: 'app/admin/subject/admin-subject.module#AdminSubjectModule'
    },
    {
      path: 'lessons',
      loadChildren: 'app/admin/lesson/admin-lesson.module#AdminLessonModule'
    },
    {
      path: 'students',
      loadChildren: 'app/admin/student/admin-student.module#AdminStudentModule'
    },
    {
      path: 'enrolments',
      loadChildren:
          'app/admin/enrolment/admin-enrolment.module#AdminEnrolmentModule'
    },
    {
      path: 'instructors',
      loadChildren:
          'app/admin/instructor/admin-instructor.module#AdminInstructorModule'
    },
    {
      path: 'users',
      loadChildren: 'app/admin/user/admin-user.module#AdminUserModule'
    },
    {
      path: 'account',
      loadChildren:
          'app/admin/admin-user-account/admin-user-account.module#AdminUserAccountModule'
    }
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
