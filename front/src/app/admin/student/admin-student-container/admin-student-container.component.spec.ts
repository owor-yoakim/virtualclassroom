import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStudentContainerComponent } from './admin-student-container.component';

describe('AdminStudentContainerComponent', () => {
  let component: AdminStudentContainerComponent;
  let fixture: ComponentFixture<AdminStudentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStudentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStudentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
