import { Component, OnInit } from '@angular/core';
import { Student } from '@models/student';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';
import { passwordsMatcher } from '@helpers/passwords-matcher';
import swal from 'sweetalert2';
import { Address } from '@models/address';
import { User } from '@models/user';

@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.css']
})
export class StudentCreateComponent implements OnInit {
  errorMessage: string;
  student: Student = null;
  user: User = null;
  studentForm: FormGroup;
  now = (new Date).toISOString();

  constructor(
      public authService: AuthService, private fb: FormBuilder,
      private studentService: StudentService, private router: Router) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.student = new Student();
    this.student.address = new Address();
    this.createForm();

  }

  createForm() {
    this.studentForm =
      this.fb.group({
        firstName: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required, Validators.minLength(3)]],
        emailAddress:
          ['', [Validators.required, Validators.email]],
        username: ['', [Validators.required, Validators.minLength(3)]],
        phoneNumber: ['', [Validators.pattern('[0-9]+')]],
        joinDate: ['', [Validators.required]],
        gender: ['', [Validators.required]],
        studentAddress: this.fb.group({
          addressType: ['', []],
          addressLine1: ['', [Validators.required, Validators.minLength(3)]],
          addressLine2: ['', [Validators.minLength(3)]],
          city: ['', [Validators.required, Validators.minLength(3)]],
          state: ['', [Validators.minLength(3)]],
          country: ['', [Validators.required]],
          zip: ['', [Validators.minLength(3)]]
        }),
        studentPassword:
          this.fb.group(
            {
              password1:
                ['', [Validators.required, Validators.minLength(5)]],
              password2: ['', [Validators.required]]
            },
            { validator: passwordsMatcher })
        // captchaCode: ['', []] // we use 'correctCaptcha' directive to
        // validate captcha code control in the template
      });
  }

  async createStudent() {
    // console.log(this.student);
    this.student.userId = this.user.id;
    try {
      // tslint:disable-next-line:prefer-const
      let response =
        await this.studentService.createStudent(this.student).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Registration Successfull!',
        type: 'success',
        timer: 3000
      });
      this.studentForm.reset();
      this.student = new Student();
      this.student.address = new Address();
    } catch (err) {
      console.log(err);
      this.errorMessage = 'Registration Failed ' + err.error;
      // this.emailExists = true;
      await swal({
        title: 'Registration Failed!',
        text: err.error,
        type: 'error'
      });
    }

  }

  backToList() {
    this.router.navigateByUrl('/administration/students');
  }

}
