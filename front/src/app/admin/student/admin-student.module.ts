import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { AdminStudentRoutingModule } from '@admin/student/admin-student-rounting.module';
import { AdminStudentContainerComponent } from '@admin/student/admin-student-container/admin-student-container.component';
import { StudentListComponent } from '@admin/student/student-list/student-list.component';
import { StudentCreateComponent } from '@admin/student/student-create/student-create.component';
import { StudentUpdateComponent } from '@admin/student/student-update/student-update.component';
import { StudentShowComponent } from '@admin/student/student-show/student-show.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminStudentRoutingModule
  ],
  declarations: [
    AdminStudentContainerComponent,
    StudentListComponent,
    StudentCreateComponent,
    StudentUpdateComponent,
    StudentShowComponent
  ]
})
export class AdminStudentModule { }
