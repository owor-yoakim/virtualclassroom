import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {passwordsMatcher} from '@helpers/passwords-matcher';
import {Address} from '@models/address';
import {Student} from '@models/student';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {StudentService} from '@services/student/student.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit {
  errorMessage: string;
  student: Student = null;
  user: User = null;
  studentId = 0;
  studentForm: FormGroup;
  now = (new Date).toISOString();

  constructor(
      public authService: AuthService, private fb: FormBuilder,
      private studentService: StudentService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.createForm();
    this.route.paramMap.subscribe(async (params) => {
      this.studentId = Number(params.get('studentId')) || 0;
      if (this.studentId > 0) {
        try {
          this.student =
              await this.studentService.getStudentById(this.studentId)
                  .toPromise();
          if (this.student.address === null) {
            this.student.address = new Address();
          }
        } catch (error) {
          console.error(error);
          await swal({title: 'Error', text: error.error, type: 'error'});
          this.backToList();
        }
      } else {
        await swal({title: 'Error', text: 'Unknown Student', type: 'error'});
        this.backToList();
      }
    });
  }

  createForm() {
    this.studentForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      emailAddress: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(3)]],
      phoneNumber: ['', [Validators.pattern('[0-9]+')]],
      joinDate: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      studentAddress: this.fb.group({
        addressType: ['', []],
        addressLine1: ['', [Validators.required, Validators.minLength(3)]],
        addressLine2: ['', [Validators.minLength(3)]],
        city: ['', [Validators.required, Validators.minLength(3)]],
        state: ['', [Validators.minLength(3)]],
        country: ['', [Validators.required]],
        zip: ['', [Validators.minLength(3)]]
      })
    });
  }

  async updateStudent() {
    this.student.userId = this.user.id;
    this.student.password = null;
    try {
      let response =
          await this.studentService.updateStudent(this.student).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Student Updated Successfully',
        type: 'success'
      });
      this.student = <Student>response;
    } catch (error) {
      console.error(error);
      await swal({title: 'Response Status', text: error.error, type: 'error'});
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/students');
  }
  
}
