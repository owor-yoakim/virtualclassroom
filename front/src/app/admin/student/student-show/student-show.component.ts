import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Enrolment} from '@models/enrolment';
import {Student} from '@models/student';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {EnrolmentService} from '@services/enrolment/enrolment.service';
import {StudentService} from '@services/student/student.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-student-show',
  templateUrl: './student-show.component.html',
  styleUrls: ['./student-show.component.css']
})
export class StudentShowComponent implements OnInit {
  student$: Observable<Student>;
  enrolments$: Observable<Enrolment[]>;
  user: User = null;
  studentId = 0;

  constructor(
      public authService: AuthService, private studentService: StudentService,
      private enrolmentService: EnrolmentService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();

    this.route.paramMap.subscribe((params) => {
      this.studentId = Number(params.get('studentId')) || 0;
      if (this.studentId > 0) {
        this.student$ = this.studentService.getStudentById(this.studentId);
        this.enrolments$ = this.studentService.getEnrolments(this.studentId);
      } else {
        swal({title: 'Error', text: 'Unknown Student', type: 'error'});
        this.backToList();
      }
    });
  }

  editStudent(student: Student) {
    this.router.navigateByUrl('/administration/students/update/' + student.id);
  }

  viewAllEnrolments() {
    this.router.navigateByUrl('/administration/enrolments');
  }

  async deleteEnrolment(enrolment: Enrolment) {
    console.log(enrolment);
  }

  backToList() {
    this.router.navigateByUrl('/administration/students');
  }
}
