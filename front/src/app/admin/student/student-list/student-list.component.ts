import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Student} from '@models/student';
import {StudentService} from '@services/student/student.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  students$: Observable<Student[]>;
  constructor(private router: Router, private studentService: StudentService) {}

  ngOnInit() {
    this.students$ = this.studentService.getAllStudents();
  }

  createStudent() {
    this.router.navigateByUrl('/administration/students/create');
  }
  editStudent(studentId: number) {
    this.router.navigateByUrl('/administration/students/update/' + studentId);
  }
  showStudent(studentId: number) {
    this.router.navigateByUrl('/administration/students/show/' + studentId);
  }
  deleteStudent(studentId: number) {
    console.log(studentId);
  }
}
