import {AdminStudentContainerComponent} from '@admin/student/admin-student-container/admin-student-container.component';
import {StudentCreateComponent} from '@admin/student/student-create/student-create.component';
import {StudentListComponent} from '@admin/student/student-list/student-list.component';
import {StudentShowComponent} from '@admin/student/student-show/student-show.component';
import {StudentUpdateComponent} from '@admin/student/student-update/student-update.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from '@services/admin-guard/admin.guard';

const adminStudentRoutes: Routes = [{
  path: '',
  component: AdminStudentContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: StudentListComponent},
    {path: 'create', component: StudentCreateComponent},
    { path: 'update/:studentId', component: StudentUpdateComponent},
    {path: 'show/:studentId', component: StudentShowComponent}
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminStudentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminStudentRoutingModule { }
