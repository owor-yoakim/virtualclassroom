export class AdminAppConstant {
    public static serverUrl = 'http://localhost:3300';
    public static adminBaseUrl = 'http://localhost:3300/admin/api';
    public static pageSizeOptions = [5, 10, 15, 20, 25, 50, 100];
    public static pageSize = 5;
}
