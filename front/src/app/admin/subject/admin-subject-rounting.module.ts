import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminContainerComponent } from '@admin/admin-container/admin-container.component';
import {AdminNotFoundComponent} from '@admin/admin-not-found/admin-not-found.component';
import {AdminSubjectContainerComponent} from '@admin/subject/admin-subject-container/admin-subject-container.component';
import { SubjectListComponent } from '@admin/subject/subject-list/subject-list.component';
import { SubjectCreateComponent } from '@admin/subject/subject-create/subject-create.component';
import { SubjectShowComponent } from '@admin/subject/subject-show/subject-show.component';
import { SubjectUpdateComponent } from '@admin/subject/subject-update/subject-update.component';
import { AdminGuard } from '@services/admin-guard/admin.guard';



const adminSubjectRoutes: Routes = [{
  path: '',
  component: AdminSubjectContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: SubjectListComponent},
    {path: 'create', component: SubjectCreateComponent},
    {path: 'update/:subjectId', component: SubjectUpdateComponent},
    {path: 'show/:subjectId', component: SubjectShowComponent}
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminSubjectRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminSubjectRoutingModule { }
