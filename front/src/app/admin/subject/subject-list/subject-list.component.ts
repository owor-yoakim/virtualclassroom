import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';
import { Subject } from '@models/subject';
import { User } from '@models/user';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {
  subjects$: Observable<Subject[]>;
  title = 'Subjects List';
  user: User = null;

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.user = this.authService.getUser();
    this.subjects$ = this.subjectService.getAllSubjects();
  }

  createSubject() {
    this.router.navigateByUrl('/administration/subjects/create');
  }

  editSubject(id: number) {
    this.router.navigateByUrl('/administration/subjects/update/' + id);
  }

  viewSubject(id: number) {
    this.router.navigateByUrl('/administration/subjects/show/' + id);
  }

  async deleteSubject(id: number) {
    try {
      let confirm = await swal({
        title: 'Are you sure?',
        text: 'The changes cannot be undone.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        cancelButtonText: 'Cancel'
      });
      if (confirm.value) {
        let response = await this.subjectService.deleteSubject(id).toPromise();
        this.subjects$ = this.subjectService.getAllSubjects();
        await swal({
          title: 'Response Status',
          text: 'Subject Deleted Successfully',
          type: 'success',
          timer: 3000
        });
      }
      
    } catch (err) {
      console.error(err.text());
      await swal({
        title: 'Response Status',
        text: 'Failed to delete subject ' + err.error,
        type: 'error',
        timer: 3000
      });
    }
  }

}
