import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router/';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { Subject } from '@models/subject';
import { User } from '@models/user';
import { DateTimeHelper } from '@helpers/date-time-helper';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-subject-update',
  templateUrl: './subject-update.component.html',
  styleUrls: ['./subject-update.component.css']
})
export class SubjectUpdateComponent implements OnInit {
  user: User = null;
  subject: Subject = null;
  subjectForm: FormGroup;
  subjectId = 0;
  ready = false;

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.ready = true;
    this.user = this.authService.getAdminUser();
    this.createForm();
    this.route.paramMap.subscribe((params) => {
      this.subjectId = Number(params.get('subjectId')) || 0;
      if (this.subjectId > 0) {
        this.subjectService.getSubjectById(this.subjectId).subscribe((subject) => {
          this.subject = subject;
          this.ready = false;
        }, (error) => {
          console.error(error);
          swal({
            title: 'Response Status',
            text: 'Invalid Subject',
            type: 'error',
            timer: 3000
          });
          this.router.navigateByUrl('/administration/subjects');
        });
      } else {
        swal({
          title: 'Response Status',
          text: 'Invalid Subject',
          type: 'error',
          timer: 3000
        });
        this.router.navigateByUrl('/administration/subjects');
      }
    });
  }

  createForm() {
    this.subjectForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });
  }

  async updateSubject() {
    this.subject.userId = this.user.id;
    try {
      let response =
        await this.subjectService.updateSubject(this.subject).toPromise();
      // console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Changes Saved successfully',
        type: 'success',
        timer: 3000
      });
      this.router.navigateByUrl('/administration/subjects');
    } catch (error) {
      await swal({
        title: 'Response Status',
        text: 'Failed to save data ' + error.toString(),
        type: 'error',
        timer: 3000
      });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/subjects');
  }

  onTitleKeyup() {
    let str: string = this.subject.title;
    this.subject.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

}
