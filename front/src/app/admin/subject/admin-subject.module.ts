import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import {AdminSubjectRoutingModule} from '@admin/subject/admin-subject-rounting.module';

import { AdminSubjectContainerComponent } from '@admin/subject/admin-subject-container/admin-subject-container.component';
import {SubjectCreateComponent} from '@admin/subject/subject-create/subject-create.component';
import {SubjectListComponent} from '@admin/subject/subject-list/subject-list.component';
import {SubjectShowComponent} from '@admin/subject/subject-show/subject-show.component';
import {SubjectUpdateComponent} from '@admin/subject/subject-update/subject-update.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminSubjectRoutingModule
  ],
  declarations: [
    AdminSubjectContainerComponent,
    SubjectListComponent,
    SubjectCreateComponent,
    SubjectUpdateComponent,
    SubjectShowComponent
  ],
  exports: [ ]
})
export class AdminSubjectModule { }
