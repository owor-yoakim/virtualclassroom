import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { User } from '@models/user';
import { Subject } from '@models/subject';

@Component({
  selector: 'app-subject-create',
  templateUrl: './subject-create.component.html',
  styleUrls: ['./subject-create.component.css']
})
export class SubjectCreateComponent implements OnInit {
  title = 'New Subject';
  subject: Subject;
  subjectForm: FormGroup;
  user: User = null;

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private router: Router
  ) { }

  async ngOnInit() {
    this.subject = new Subject();
    this.user = this.authService.getAdminUser();
    this.createForm();
  }

  createForm() {
    this.subjectForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required])
    });
  }

  async createSubject() {
    this.subject.userId = this.user.id;
   try {
     let response =
         await this.subjectService.createSubject(this.subject).toPromise();
     await swal({
       title: 'Response Status',
       text: 'Subject Created Successfully',
       type: 'success',
       timer: 3000
     });
     this.subject = new Subject();
     this.subjectForm.reset();
   } catch (error) {
     swal({
       title: 'Response Status',
       text: 'Failed to save data ' + error.toString(),
       type: 'error',
       timer: 3000
     });
   } 
    
  }

  onTitleKeyup() {
    let str: string = this.subject.title;
    this.subject.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/subjects');
  }

}
