import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router/';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { User } from '@models/user';
import { Subject } from '@models/subject';
import { Lesson } from '@models/lesson';
import { DateTimeHelper } from '@helpers/date-time-helper';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { LessonService } from '@services/lesson/lesson.service';
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'app-subject-show',
  templateUrl: './subject-show.component.html',
  styleUrls: ['./subject-show.component.css']
})
export class SubjectShowComponent implements OnInit {
  user: User = null;
  subject$: Observable<Subject>;
  lessons$: Observable<Lesson[]>;
  subjectForm: FormGroup;
  loading = false;
  subjectId = 0;

  dtHelper = new DateTimeHelper();

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private lessonService: LessonService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();

    this.route.paramMap.subscribe(async (params) => {
      // console.log(params);
      this.subjectId = Number(params.get('subjectId')) || 0;
      if (this.subjectId > 0) {
        this.subject$ = this.subjectService.getSubjectById(this.subjectId);
        this.lessons$ = this.lessonService.getLessonsBySubject(this.subjectId);
      } else {
        swal({ title: 'Error', text: 'Unknown Subject', type: 'error', timer: 3000 });
        this.router.navigateByUrl('/administration/subjects');
      }
    });
  }

  backToList() {
    this.router.navigateByUrl('/administration/subjects');
  }


  allLessons() {
     this.router.navigateByUrl('/administration/lessons');
  }

  editSubject(id: number) {
     this.router.navigateByUrl('/administration/subjects/update/' + id);
  }

  viewLesson(id: number) {
    this.router.navigateByUrl('/administration/lessons/show/' + id);
  }

  editLesson(id: number) {
     this.router.navigateByUrl('/administration/lessons/update/' + id);
  }


}
