import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSubjectContainerComponent } from './admin-subject-container.component';

describe('AdminSubjectContainerComponent', () => {
  let component: AdminSubjectContainerComponent;
  let fixture: ComponentFixture<AdminSubjectContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSubjectContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSubjectContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
