import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import swal from 'sweetalert2';
import { LoginCredentials } from '@models/login-credentials';


@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  loading = false;
  credentials = new LoginCredentials();

  hasError = false;
  loginErrorMessage = 'Invalid Credentials!';

  adminLoginForm: FormGroup;

  constructor(
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = false;
    if (this.authService.isAdminAuthenticated()) {
      this.router.navigateByUrl('/administration');
      return;
    }

    if (this.authService.isAuthenticated()) {
      this.router.navigateByUrl('/');
      return;
    }
    this.adminLoginForm = new FormGroup({
      loginEmail: new FormControl('', [Validators.email, Validators.required]),
      loginPassword: new FormControl('', [Validators.required])
    });
  }

  async login() {
    this.loading = true;
    const redirectUrl = this.authService.redirectUrl || '/administration';
    try {
      let response = await this.authService.adminLogin(this.credentials);
      if (response) {
        this.adminLoginForm.reset();
        this.hasError = false;
        this.router.navigateByUrl(redirectUrl);
      } else {
        this.loading = false;
        this.hasError = true;
        this.credentials.loginPassword = '';
      }
    } catch (error) {
      this.hasError = true;
      this.loading = false;
      this.credentials.loginPassword = '';
      await swal({title: error.text(), type: 'error', timer: 3000});
    }
  }

}
