import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLessonContainerComponent } from '@admin/lesson/admin-lesson-container/admin-lesson-container.component';
import { LessonListComponent } from '@admin/lesson/lesson-list/lesson-list.component';
import { LessonCreateComponent } from '@admin/lesson/lesson-create/lesson-create.component';
import { LessonUpdateComponent } from '@admin/lesson/lesson-update/lesson-update.component';
import { LessonShowComponent } from '@admin/lesson/lesson-show/lesson-show.component';
import { AdminGuard } from '@services/admin-guard/admin.guard';
import { LessonModuleListComponent } from '@admin/lesson/lesson-module-list/lesson-module-list.component';
import { LessonModuleCreateComponent } from '@admin/lesson/lesson-module-create/lesson-module-create.component';
import { LessonModuleShowComponent } from '@admin/lesson/lesson-module-show/lesson-module-show.component';
import { LessonModuleUpdateComponent } from '@admin/lesson/lesson-module-update/lesson-module-update.component';


const adminLessonRoutes: Routes = [{
  path: '',
  component: AdminLessonContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: LessonListComponent},
    {path: 'create', component: LessonCreateComponent},
    {path: 'update/:lessonId', component: LessonUpdateComponent},
    {path: 'show/:lessonId', component: LessonShowComponent},
    {path: ':lessonSlug/modules', component: LessonModuleListComponent},
    {path: ':lessonSlug/modules/create', component: LessonModuleCreateComponent},
    {path: ':lessonSlug/modules/update/:lessonModuleId', component: LessonModuleUpdateComponent},
    {path: ':lessonSlug/modules/show/:lessonModuleId', component: LessonModuleShowComponent}
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminLessonRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminLessonRoutingModule { }
