import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonModuleUpdateComponent } from './lesson-module-update.component';

describe('LessonModuleUpdateComponent', () => {
  let component: LessonModuleUpdateComponent;
  let fixture: ComponentFixture<LessonModuleUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonModuleUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonModuleUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
