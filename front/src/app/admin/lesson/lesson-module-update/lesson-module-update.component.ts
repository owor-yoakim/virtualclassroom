import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {AuthService} from '@services/auth/auth.service';
import {LessonModuleService} from '@services/lesson-module/lesson-module.service';
import { LessonService } from '@services/lesson/lesson.service';
import { Lesson } from '@models/lesson';
import { LessonModule } from '@models/lesson-module';
import { User } from '@models/user';
import swal from 'sweetalert2';


@Component({
  selector: 'app-lesson-module-update',
  templateUrl: './lesson-module-update.component.html',
  styleUrls: ['./lesson-module-update.component.css']
})
export class LessonModuleUpdateComponent implements OnInit {
  title = 'Update Lesson Module';
  user: User = null;
  lesson: Lesson = null;
  lessonModule: LessonModule = null;
  lessonSlug = null;
  lessonModuleId = 0;
  lessonModuleForm: FormGroup;


  constructor(public authService: AuthService, private lessonService: LessonService,
              private lessonModuleService: LessonModuleService,
              private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.createForm();
    this.route.paramMap.subscribe(async (params) => {
      this.lessonModuleId = Number(params.get('lessonModuleId')) || 0;
      this.lessonSlug = params.get('lessonSlug') || null;
      if (this.lessonSlug !== null && this.lessonModuleId > 0) {
        this.lesson = await this.lessonService.getLessonBySlug(this.lessonSlug).toPromise();
        this.lessonModule = await this.lessonModuleService.getLessonModuleById(this.lesson.id, this.lessonModuleId).toPromise();
      } else {
        swal({ title: 'Error', text: 'Unknown Request', type: 'warning', timer: 3000 });
        this.router.navigateByUrl('/administration/lessons');
      }
    });
  }

  createForm() {
    this.lessonModuleForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      scheduleDate: new FormControl('', [Validators.required]),
      startTime: new FormControl('', [Validators.required]),
      duration: new FormControl('', [Validators.required]),
      lessonId: new FormControl('', [Validators.required])
    });
  }

  async updateLessonModule() {
    this.lessonModule.userId = this.user.id;
    delete this.lessonModule.lesson;
    try {
      let response =
          await this.lessonModuleService.updateLessonModule(this.lessonModule)
              .toPromise();
      await swal({
        title: 'Response Status',
        text: 'Lesson Module Updated Successfully',
        type: 'success',
        timer: 3000
      });
      // this.lessonModuleForm.reset();
    } catch (error) {
      console.error(error);
      await swal({
        title: 'Response Status',
        text: error.error,
        type: 'error',
        timer: 3000
      });
    }
  }

  backToList(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/show/' + this.lesson.id);
  }
}
