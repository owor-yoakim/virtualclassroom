import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonModuleShowComponent } from './lesson-module-show.component';

describe('LessonModuleShowComponent', () => {
  let component: LessonModuleShowComponent;
  let fixture: ComponentFixture<LessonModuleShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonModuleShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonModuleShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
