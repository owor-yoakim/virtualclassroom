import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Lesson} from '@models/lesson';
import {LessonComment} from '@models/lesson-comment';
import {LessonModule} from '@models/lesson-module';
import {LessonCommentService} from '@services/lesson-comment/lesson-comment.service';
import {LessonModuleService} from '@services/lesson-module/lesson-module.service';
import {LessonService} from '@services/lesson/lesson.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lesson-module-show',
  templateUrl: './lesson-module-show.component.html',
  styleUrls: ['./lesson-module-show.component.css']
})
export class LessonModuleShowComponent implements OnInit {
  lesson$: Observable<Lesson>;
  lessonModule$: Observable<LessonModule>;
  
  constructor(
      private lessonService: LessonService,
      private lessonModuleService: LessonModuleService,
      private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      let lessonSlug = params.get('lessonSlug') || null;
      let lessonModuleId = Number(params.get('lessonModuleId')) || 0;
      if (lessonSlug !== null && lessonModuleId > 0) {
        this.lesson$ = this.lessonService.getLessonBySlug(lessonSlug);
        this.lesson$.subscribe((lesson) => {
          this.lessonModule$ = this.lessonModuleService.getLessonModuleById(
            lesson.id, lessonModuleId);
        }, (error) => {
          console.error(error);
          swal({title: 'Error', text: 'Unknown Lesson', type: 'error', timer: 3000});
          this.router.navigateByUrl('/administration/lessons');
        });
      } else {
        
      }
    });
  }

  backToList(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/show/' + lesson.id);
  }
}
