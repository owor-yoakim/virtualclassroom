import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subject } from '@models/subject';
import { Lesson } from '@models/lesson';
import { User } from '@models/user';
import { DateTimeHelper } from '@helpers/date-time-helper';
import { SubjectService } from '@services/subject/subject.service';
import { LessonService } from '@services/lesson/lesson.service';
import { AuthService } from '@services/auth/auth.service';
import { Observable } from 'rxjs/Observable';
import { Instructor } from '@models/instructor';
import { Enrolment } from '@models/enrolment';
import { LessonCommentService } from '@services/lesson-comment/lesson-comment.service';
import { EnrolmentService } from '@services/enrolment/enrolment.service';

@Component({
  selector: 'app-lesson-show',
  templateUrl: './lesson-show.component.html',
  styleUrls: ['./lesson-show.component.css']
})
export class LessonShowComponent implements OnInit {
  lesson$: Observable<Lesson>;
  lessonInstructors$: Observable<Instructor[]>;
  prerequisites$: Observable<Lesson[]>;
  lessonEnrolments$: Observable<Enrolment[]>;
  lessonId = 0;

  constructor(
    private lessonService: LessonService,
    private enrolmentService: EnrolmentService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      // console.log(params);
      this.lessonId = Number(params.get('lessonId')) || 0;
      if (this.lessonId > 0) {
        this.lesson$ = this.lessonService.getLessonById(this.lessonId);
        this.prerequisites$ = this.lessonService.getLessonPrerequisites(this.lessonId);
        this.lessonInstructors$ = this.lessonService.getLessonInstructors(this.lessonId);
        this.lessonEnrolments$ = this.enrolmentService.getByLesson(this.lessonId);
      } else {
        this.router.navigateByUrl('/administration/lessons');
      }
    });
  }

  editLesson(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/update/' + lesson.id);
  }

  viewAllEnrolments() {
    this.router.navigateByUrl('/administration/enrolments');
  }

  backToList() {
    this.router.navigateByUrl('/administration/lessons');
  }

}
