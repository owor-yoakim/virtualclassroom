import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import swal from 'sweetalert2';
import { AuthService } from '@services/auth/auth.service';
import { SubjectService } from '@services/subject/subject.service';
import { LessonService } from '@services/lesson/lesson.service';
import { InstructorService } from '@services/instructor/instructor.service';
import { LessonModuleService } from '@services/lesson-module/lesson-module.service';
import { User } from '@models/user';
import { LessonModule } from '@models/lesson-module';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-lesson-module-create',
  templateUrl: './lesson-module-create.component.html',
  styleUrls: ['./lesson-module-create.component.css']
})
export class LessonModuleCreateComponent implements OnInit {
  title = 'New Module';
  user: User = null;
  lessonModule: LessonModule = null;
  lesson$: Observable<Lesson>;
  lessonSlug = null;
  lessonModuleForm: FormGroup;
  now = (new Date).toISOString();

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private lessonService: LessonService,
    private instructorService: InstructorService,
    private lessonModuleService: LessonModuleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.lessonModule = new LessonModule();
    this.lessonModule.lessonId = 0;
    this.createForm();
    this.route.paramMap.subscribe(async (params) => {
      this.lessonSlug = params.get('lessonSlug') || null;
        if (this.lessonSlug !== null) {
          this.lesson$ = this.lessonService.getLessonBySlug(this.lessonSlug);
        } else {
          await swal({ title: 'Error', text: 'Unknown Lesson', type: 'warning', timer: 3000 });
          this.router.navigateByUrl('/administration/lessons');
        }
    });
  }

  createForm() {
    this.lessonModuleForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      scheduleDate: new FormControl('', [Validators.required]),
      startTime: new FormControl('', [Validators.required]),
      duration: new FormControl('', [Validators.required]),
      lessonId: new FormControl('', [Validators.required])
    });
  }

  async createLessonModule() {
    this.lessonModule.userId = this.user.id;
    try {
      let response =
          await this.lessonModuleService.createLessonModule(this.lessonModule)
              .toPromise();
      await swal({
        title: 'Response Status',
        text: 'Lesson Module Created Successfully',
        type: 'success',
        timer: 3000
      });
      this.lessonModuleForm.reset();
    } catch (error) {
      console.error(error);
      await swal({
        title: 'Response Status',
        text: error.error,
        type: 'error',
        timer: 3000
      });
    }
  }

  backToList(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/show/' + lesson.id);
  }

}
