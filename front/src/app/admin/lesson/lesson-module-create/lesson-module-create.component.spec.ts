import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonModuleCreateComponent } from './lesson-module-create.component';

describe('LessonModuleCreateComponent', () => {
  let component: LessonModuleCreateComponent;
  let fixture: ComponentFixture<LessonModuleCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonModuleCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonModuleCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
