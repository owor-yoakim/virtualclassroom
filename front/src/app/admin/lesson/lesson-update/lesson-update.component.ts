import {AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Lesson} from '@models/lesson';
import {Subject} from '@models/subject';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonService} from '@services/lesson/lesson.service';
import {SubjectService} from '@services/subject/subject.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

// declare var tinymce: any;

@Component({
  selector: 'app-lesson-update',
  templateUrl: './lesson-update.component.html',
  styleUrls: ['./lesson-update.component.css']
})
export class LessonUpdateComponent implements OnInit {
  title = 'Update Lesson';
  user: User = null;
  lesson: Lesson = null;
  prerequisites$: Observable<Lesson[]>;
  lessonPrerequisites: string[] = [];
  lessonForm: FormGroup;
  lessonId = 0;

  constructor(
      public authService: AuthService, private subjectService: SubjectService,
      private lessonService: LessonService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async (params) => {
      // console.log(params);
      this.lessonId = Number(params.get('lessonId')) || 0;

      this.user = this.authService.getAdminUser();

      this.createForm();
      if (this.lessonId > 0) {
        try {
          this.lesson = await this.lessonService.getLessonById(this.lessonId).toPromise();
          this.getPrerequisites();
        } catch (error) {
          console.error(error);
          await swal({text: error.error, type: 'error'});
          this.router.navigateByUrl('/administration/lessons');
        }
      } else {
        await swal({title: 'Error', text: 'Unknown Lesson', type: 'error'});
        this.router.navigateByUrl('/administration/lessons');
      }
    });
  }

  createForm() {
    this.lessonForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      outline: new FormControl('', []),
      outcome: new FormControl('', []),
      prerequisite: new FormControl('', []),
      subjectId: new FormControl('', [Validators.required])
    });
  }

  async updateLesson() {
    try {
      this.lesson.userId = this.user.id;
      this.lesson.prerequisites = this.lessonPrerequisites;
      // console.log("Lesson: ", this.lesson);
      let response =
          await this.lessonService.updateLesson(this.lesson).toPromise();
      this.lesson = await this.lessonService.getLessonById(this.lessonId).toPromise();
      this.getPrerequisites();
      await swal({
        title: 'Response Status',
        text: 'Lesson Updated Successfully',
        type: 'success',
        timer: 3000
      });
    } catch (error) {
      console.error(error);
      await swal({title: 'Response Status', text: error.error, type: 'error'});
    }
  }

  async getPrerequisites() {
    let prerequisites =
        await this.lessonService.getLessonPrerequisites(this.lessonId)
            .toPromise();
    prerequisites.forEach((lesson) => {
      this.lessonPrerequisites.push(lesson.id.toString());
    });
    this.prerequisites$ =
        this.lessonService.getLessonsBySubject(this.lesson.subjectId)
            .map(
                lessons =>
                    lessons.filter(lesson => lesson.id !== this.lesson.id));
  }

  onTitleKeyup() {
    let str: string = this.lesson.title;
    this.lesson.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/lessons');
  }
}
