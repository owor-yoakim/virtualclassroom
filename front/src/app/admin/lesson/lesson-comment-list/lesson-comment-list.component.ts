import { Component, OnInit, Input } from '@angular/core';
import { Lesson } from '@models/lesson';
import { LessonComment } from '@models/lesson-comment';
import { LessonCommentService } from '@services/lesson-comment/lesson-comment.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-lesson-comment-list',
  templateUrl: './lesson-comment-list.component.html',
  styleUrls: ['./lesson-comment-list.component.css']
})
export class LessonCommentListComponent implements OnInit {
  @Input('lessonComments') lessonComments: LessonComment[] = [];
  
  constructor() { }

  ngOnInit() {
  }

}
