import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonCommentListComponent } from './lesson-comment-list.component';

describe('LessonCommentListComponent', () => {
  let component: LessonCommentListComponent;
  let fixture: ComponentFixture<LessonCommentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonCommentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonCommentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
