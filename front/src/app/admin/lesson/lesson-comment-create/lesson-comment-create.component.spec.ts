import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonCommentCreateComponent } from './lesson-comment-create.component';

describe('LessonCommentCreateComponent', () => {
  let component: LessonCommentCreateComponent;
  let fixture: ComponentFixture<LessonCommentCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonCommentCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonCommentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
