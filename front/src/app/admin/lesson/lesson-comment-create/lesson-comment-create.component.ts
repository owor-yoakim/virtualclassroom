import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Lesson } from '@models/lesson';
import { LessonComment } from '@models/lesson-comment';
import { LessonCommentService } from '@services/lesson-comment/lesson-comment.service';

import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { LessonModule } from '@models/lesson-module';
import { AuthService } from '@services/auth/auth.service';
import { User } from '@models/user';


@Component({
  selector: 'app-lesson-comment-create',
  templateUrl: './lesson-comment-create.component.html',
  styleUrls: ['./lesson-comment-create.component.css']
})
export class LessonCommentCreateComponent implements OnInit {
  @Input('lesson') lesson: Lesson = null;
  @Input('lessonModule') lessonModule: LessonModule = null;
  comments$: Observable<LessonComment[]>;
  lessonComment: LessonComment = null;
  lessonCommentForm: FormGroup;
  panelOpenState = false;
  user: User = null;

  constructor(public authService: AuthService, private lessonCommentService: LessonCommentService) { }

  ngOnInit() {
    this.getComments();

    if (this.authService.isAdminAuthenticated()) {
      this.user = this.authService.getAdminUser();
    } else {
      this.user = this.authService.getUser();
    }

    this.lessonComment = new LessonComment();
    this.lessonComment.lessonId = this.lesson.id;
    this.lessonComment.name = this.user.firstName + ' ' + this.user.lastName;
    this.lessonComment.email = this.user.emailAddress;

    this.lessonCommentForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      body: new FormControl('', [Validators.required, Validators.minLength(10)])
    });
  }

  getComments() {
    if (this.lessonModule !== null) {
      this.comments$ =
        this.lessonCommentService.getLessonModuleComments(this.lessonModule);
    } else {
      this.comments$ =
        this.lessonCommentService.getLessonComments(this.lesson.id);
    }
  }

  async onAddComment() {
    try {
      this.lessonComment.lessonId = this.lesson.id;
      this.lessonComment.name = this.user.firstName + ' ' + this.user.lastName;
      this.lessonComment.email = this.user.emailAddress;
      this.lessonComment.lessonModuleId = (this.lessonModule !== null) ? this.lessonModule.id : null;
      console.log(this.lessonComment);
      let response =
        await this.lessonCommentService.addComment(this.lessonComment).toPromise();
      this.lessonComment.body = '';
      this.getComments();
      await swal({ title: 'Response Status', text: 'Comment Submitted', type: 'success', timer: 3000 });
    } catch (error) {
      console.error(error);
    }
  }

}
