
import {AdminLessonContainerComponent} from '@admin/lesson/admin-lesson-container/admin-lesson-container.component';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { AdminLessonRoutingModule } from '@admin/lesson/admin-lesson-rounting.module';

import { LessonListComponent } from '@admin/lesson/lesson-list/lesson-list.component';
import { LessonCreateComponent } from '@admin/lesson/lesson-create/lesson-create.component';
import { LessonShowComponent } from '@admin/lesson/lesson-show/lesson-show.component';
import { LessonModuleListComponent } from '@admin/lesson/lesson-module-list/lesson-module-list.component';
import { LessonModuleShowComponent } from '@admin/lesson/lesson-module-show/lesson-module-show.component';
import { LessonCommentCreateComponent } from '@admin/lesson/lesson-comment-create/lesson-comment-create.component';
import { LessonCommentListComponent } from '@admin/lesson/lesson-comment-list/lesson-comment-list.component';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminLessonRoutingModule
  ],
  declarations: [
    AdminLessonContainerComponent,
    LessonListComponent,
    LessonCreateComponent,
    LessonShowComponent,
    LessonModuleListComponent,
    LessonModuleShowComponent
  ],
  providers: [],
  exports: []
})
export class AdminLessonModule { }
