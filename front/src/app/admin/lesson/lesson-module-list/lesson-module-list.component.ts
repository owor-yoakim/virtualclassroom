import {Component, Input, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import { LessonModuleService } from '@services/lesson-module/lesson-module.service';
import { LessonModule } from '@models/lesson-module';
import {Lesson} from '@models/lesson';

import {Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-lesson-module-list',
  templateUrl: './lesson-module-list.component.html',
  styleUrls: ['./lesson-module-list.component.css']
})
export class LessonModuleListComponent implements OnInit {
  @Input('lesson') lesson: Lesson = null;
  lessonModules$: Observable<LessonModule[]>;

  constructor(private lessonModuleService: LessonModuleService, private router: Router) { }

  ngOnInit() {
    this.lessonModules$ = this.lessonModuleService.getLessonModules(this.lesson.id);
  }

  createLessonModule() {
    this.router.navigateByUrl('/administration/lessons/' + this.lesson.slug + '/modules/create');
  }

  viewLessonModule(lessonModule: LessonModule) {
    this.router.navigateByUrl(
        '/administration/lessons/' + this.lesson.slug + '/modules/show/' + lessonModule.id);
  }

  editLessonModule(lessonModule: LessonModule) {
    this.router.navigateByUrl(
        '/administration/lessons/' + this.lesson.slug + '/modules/update/' + lessonModule.id);
  }

  deleteLessonModule(lessonModule: LessonModule) {
    
  }

}
