import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonModuleListComponent } from './lesson-module-list.component';

describe('LessonModuleListComponent', () => {
  let component: LessonModuleListComponent;
  let fixture: ComponentFixture<LessonModuleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonModuleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonModuleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
