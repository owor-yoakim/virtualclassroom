import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLessonContainerComponent } from './admin-lesson-container.component';

describe('AdminLessonContainerComponent', () => {
  let component: AdminLessonContainerComponent;
  let fixture: ComponentFixture<AdminLessonContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLessonContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLessonContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
