import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router/';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Lesson} from '@models/lesson';
import {Subject} from '@models/subject';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonService} from '@services/lesson/lesson.service';
import {SubjectService} from '@services/subject/subject.service';

import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lesson-list',
  templateUrl: './lesson-list.component.html',
  styleUrls: ['./lesson-list.component.css']
})
export class LessonListComponent implements OnInit {
  user: User = null;
  subjects$: Observable<Subject[]>;
  lessons$: Observable<Lesson[]>;
  filteredLessons$: Observable<Lesson[]>;
  subjectId = 0;
  subjectForm: FormGroup;

  constructor(
      public authService: AuthService, private subjectService: SubjectService,
      private lessonService: LessonService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getUser();
    this.lessons$ = this.lessonService.getAllLessons();
    this.subjects$ = this.subjectService.getAllSubjects();
  }

  addLesson(lesson: Lesson) {
      this.router.navigateByUrl('/administration/lessons/create');
  }

  editLesson(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/update/' + lesson.id);
  }


  viewLesson(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/show/' + lesson.id);
  }

  onFilterBySubjects() {
    this.subjectId = Number(this.subjectId);
    console.log(this.subjectId);
    if (this.subjectId > 0) {
      this.lessons$ = this.lessons$.map(lessons => lessons.filter(lesson => lesson.subjectId === this.subjectId));
    } else {
      this.lessons$ = this.lessonService.getAllLessons();
    }
  }

  async deleteLesson(lesson: Lesson) {
    try {
      let confirm = await swal({
        title: 'Are you sure?',
        text: 'The changes can\'t be undone.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        cancelButtonText: 'Cancel'
      });
      if (confirm.value) {
        let response = await this.lessonService.deleteLesson(lesson).toPromise();
        await swal({
          title: 'Response Status',
          text: 'Lesson deleted successfully.',
          type: 'success'
        });
        this.lessons$ = this.lessonService.getAllLessons();
        this.onFilterBySubjects();
      }
    } catch (error) {
      console.error(error);
      await swal({ title: 'Response Status', text: error.error, type: 'error' });
    }
  }
}
