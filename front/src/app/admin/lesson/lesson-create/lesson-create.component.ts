import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Lesson} from '@models/lesson';
import {Subject} from '@models/subject';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonService} from '@services/lesson/lesson.service';
import {SubjectService} from '@services/subject/subject.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lesson-create',
  templateUrl: './lesson-create.component.html',
  styleUrls: ['./lesson-create.component.css']
})
export class LessonCreateComponent implements OnInit {
  title = 'New Lesson';
  user: User = null;
  subjects$: Observable<Subject[]>;
  lessons$: Observable<Lesson[]>;
  prerequisites$: Observable<Lesson[]>;
  lesson: Lesson;
  lessonPrerequisites: string[] = [];
  lessonForm: FormGroup;

  constructor(
      public authService: AuthService, private subjectService: SubjectService,
      private lessonService: LessonService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.lesson = new Lesson();
    this.lesson.subjectId = 0;
    this.subjects$ = this.subjectService.getAllSubjects();
    this.lessons$ = this.lessonService.getAllLessons();
    this.createForm();
  }

  createForm() {
    this.lessonForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      slug: new FormControl('', [Validators.required]),
      description: new FormControl('', []),
      outline: new FormControl('', []),
      outcome: new FormControl('', []),
      prerequisite: new FormControl('', []),
      subjectId: new FormControl('', [Validators.required])
    });
  }

  async createLesson() {
    this.lesson.userId = this.user.id;
    this.lesson.prerequisites = this.lessonPrerequisites;
    try {
      let response =
          await this.lessonService.createLesson(this.lesson).toPromise();
      await swal({
        title: 'Response Status',
        text: 'Save Successful',
        type: 'success',
        timer: 3000
      });
      this.lesson = new Lesson();
      this.lessonForm.reset();
      this.router.navigateByUrl('/administration/lessons');
    } catch (error) {
      console.error(error);
      await swal({title: 'Response Status', text: error.error, type: 'error'});
    }
  }

  getPrerequisites() {
    let subjectId = Number(this.lesson.subjectId);
    if (subjectId > 0) {
      this.prerequisites$ = this.lessons$.map(
        lessons => lessons.filter(lesson => lesson.subjectId === subjectId));
    }else {
      this.prerequisites$ = this.lessons$;
    }
  }

  onTitleKeyup() {
    let str: string = this.lesson.title;
    this.lesson.slug = str.split(' ').join('-').toLocaleLowerCase();
  }

  backToList() {
    this.router.navigateByUrl('/administration/lessons');
  }
}
