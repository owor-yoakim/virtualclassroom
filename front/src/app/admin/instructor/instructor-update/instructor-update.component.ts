import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Address} from '@models/address';
import {Instructor} from '@models/instructor';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {InstructorService} from '@services/instructor/instructor.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-instructor-update',
  templateUrl: './instructor-update.component.html',
  styleUrls: ['./instructor-update.component.css']
})
export class InstructorUpdateComponent implements OnInit {
  user: User = null;
  instructor: Instructor = null;
  instructorId = 0;
  now = (new Date).toISOString();

  emailSent = false;
  emailExists = false;

  instructorForm: FormGroup;

  constructor(
    public authService: AuthService,
    private instructorService: InstructorService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.createForm();
    this.route.paramMap.subscribe(async (params) => {
      this.instructorId = Number(params.get('instructorId')) || 0;
      if (this.instructorId > 0) {
        try {
          this.instructor =
              await this.instructorService.getInstructorById(this.instructorId)
              .toPromise();
          console.log(this.instructor);
          this.instructor.password = null;
          if (this.instructor.address === null) { this.instructor.address = new Address(); }
        } catch (error) {
          console.error(error);
          swal({
            title: 'Error',
            text: error.error,
            type: 'error'
          });
          this.backToList();
        }
      } else {
        swal({
          title: 'Error',
          text: 'Unknown Instructor',
          type: 'error'
        });
        this.backToList();
      }
    });
  }

  createForm() {
    this.instructorForm = this.fb.group({
      title: ['', [Validators.required]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      emailAddress: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(6)]],
      joinDate: ['', [Validators.required]],
      instructorStatus: ['', []],
      qualification: ['', [Validators.required]],
      specialization: ['', [Validators.required]],
      profile: ['', []],
      institution: ['', []],
      phoneNumber: ['', [Validators.pattern('[0-9]+')]],
      instructorAddress: this.fb.group({
        addressType: ['', [Validators.required]],
        addressLine1: ['', [Validators.required, Validators.minLength(3)]],
        addressLine2: ['', [Validators.required, Validators.minLength(3)]],
        city: ['', [Validators.required, Validators.minLength(3)]],
        state: ['', [Validators.required, Validators.minLength(3)]],
        country: ['', [Validators.required]],
        zip: ['', [Validators.required, Validators.minLength(3)]]
      })
    });
  }

  async updateInstructor() {
    try {
      this.instructor.userId = this.user.id;
      let response =
          await this.instructorService.updateInstructor(this.instructor)
              .toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Instructor updated successfully',
        type: 'success',
        timer: 3000
      });
      this.instructor = <Instructor>response;
    } catch (error) {
      console.error(error);
      swal({
        title: 'Response Status',
        text: error.text(),
        type: 'error',
        timer: 3000
      });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/instructors');
  }

}
