import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { InstructorListComponent } from './instructor-list/instructor-list.component';
import { InstructorCreateComponent } from './instructor-create/instructor-create.component';
import { InstructorShowComponent } from './instructor-show/instructor-show.component';
import { AdminInstructorContainerComponent } from '@admin/instructor/admin-instructor-container/admin-instructor-container.component';
import { AdminInstructorRoutingModule } from '@admin/instructor/admin-instructor-rounting.module';
import { InstructorUpdateComponent } from '@admin/instructor/instructor-update/instructor-update.component';
import { InstructorAssignLessonComponent } from '@admin/instructor/instructor-assign-lesson/instructor-assign-lesson.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminInstructorRoutingModule
  ],
  declarations: [
    AdminInstructorContainerComponent,
    InstructorListComponent,
    InstructorCreateComponent,
    InstructorUpdateComponent,
    InstructorShowComponent,
    InstructorAssignLessonComponent
  ],
  exports: []
})
export class AdminInstructorModule { }
