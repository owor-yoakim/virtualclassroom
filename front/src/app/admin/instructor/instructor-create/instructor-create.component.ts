import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import swal from 'sweetalert2';

import { User } from '@models/user';
import { Instructor } from '@models/instructor';
import { Address } from '@models/address';
import { passwordsMatcher } from '@helpers/passwords-matcher';
import { AuthService } from '@services/auth/auth.service';
import { InstructorService } from '@services/instructor/instructor.service';


@Component({
  selector: 'app-instructor-create',
  templateUrl: './instructor-create.component.html',
  styleUrls: ['./instructor-create.component.css']
})
export class InstructorCreateComponent implements OnInit {
  user: User = null;
  instructor: Instructor = null;

  now = (new Date).toISOString();
  
  emailSent = false;
  emailExists = false;

  instructorForm: FormGroup;

  constructor(
    public authService: AuthService,
    private instructorService: InstructorService,
    private fb: FormBuilder,
    private router: Router
  ) { }


  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.instructor = new Instructor();
    this.instructor.address = new Address();
    this.instructor.userId = this.user.id;
    this.createForm();
    
  }

  createForm() {
    this.instructorForm =
        this.fb.group({
          title: ['', [Validators.required]],
          firstName: ['', [Validators.required, Validators.minLength(3)]],
          lastName: ['', [Validators.required, Validators.minLength(3)]],
          emailAddress: ['', [Validators.required, Validators.email]],
          username: ['', [Validators.required, Validators.minLength(6)]],
          joinDate: ['', [Validators.required]],
          instructorStatus: ['', []],
          qualification: ['', [Validators.required]],
          specialization: ['', [Validators.required]],
          profile: ['', []],
          institution: ['', []],
          phoneNumber: ['', [Validators.pattern('[0-9]+')]],
          instructorAddress: this.fb.group({
            addressType: ['', [Validators.required]],
            addressLine1: ['', [Validators.required, Validators.minLength(3)]],
            addressLine2: ['', [Validators.required, Validators.minLength(3)]],
            city: ['', [Validators.required, Validators.minLength(3)]],
            state: ['', [Validators.required, Validators.minLength(3)]],
            country: ['', [Validators.required]],
            zip: ['', [Validators.required, Validators.minLength(3)]]
          }),
          instructorPassword:
              this.fb.group(
                  {
                    password1:
                        ['', [Validators.required, Validators.minLength(6)]],
                    password2: ['', [Validators.required]]
                  },
                  {validator: passwordsMatcher})
        });
  }

  async createInstructor() {
    try {
      let response =
          await this.instructorService.createInstructor(this.instructor)
          .toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Instructor created successfully',
        type: 'success',
        timer: 3000
      });
      this.instructor = new Instructor();
      this.instructor.address = new Address();
      this.instructor.userId = this.user.id;
      this.instructorForm.reset();
    } catch (error) {
      console.error(error);
      await swal({
        title: 'Response Status',
        text: error.error,
        type: 'error'
      });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/instructors');
  }

}
