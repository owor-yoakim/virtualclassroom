import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';

import { User } from '@models/user';
import { Instructor } from '@models/instructor';
import { SubjectService } from '@services/subject/subject.service';
import { LessonService } from '@services/lesson/lesson.service';
import { InstructorService } from '@services/instructor/instructor.service';
import { AuthService } from '@services/auth/auth.service';
import { Observable } from 'rxjs/Observable';




@Component({
  selector: 'app-instructor-list',
  templateUrl: './instructor-list.component.html',
  styleUrls: ['./instructor-list.component.css']
})
export class InstructorListComponent implements OnInit {
  user: User = null;
  instructors$: Observable<Instructor[]>;
  title = 'Instructors List';

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private lessonService: LessonService,
    private instructorService: InstructorService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    
    this.instructors$ = this.instructorService.getAllInstructors();
  }

  viewInstructor(id: number) {
    this.router.navigateByUrl('/administration/instructors/show/' + id);
  }

  createInstructor() {
    this.router.navigateByUrl('/administration/instructors/create');
  }

  editInstructor(id: number) {
    this.router.navigateByUrl('/administration/instructors/update/' + id);
  }

  async onStatusChanged(instructorId: number) {
    try {
      let response = await this.instructorService.activateOrDeactivateInstructor(instructorId).toPromise();
      // console.log(response);
      await swal({ title: 'Response Status',text: 'Status Updated', type: 'success', timer: 3000 });
    } catch (error) {
      console.error(error);
    }
  }

}
