import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminInstructorContainerComponent } from './admin-instructor-container.component';

describe('AdminInstructorContainerComponent', () => {
  let component: AdminInstructorContainerComponent;
  let fixture: ComponentFixture<AdminInstructorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminInstructorContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminInstructorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
