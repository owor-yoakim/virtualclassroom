import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorAssignLessonComponent } from './instructor-assign-lesson.component';

describe('InstructorAssignLessonComponent', () => {
  let component: InstructorAssignLessonComponent;
  let fixture: ComponentFixture<InstructorAssignLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorAssignLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorAssignLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
