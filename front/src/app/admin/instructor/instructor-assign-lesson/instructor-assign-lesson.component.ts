import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Instructor } from '@models/instructor';
import { AuthService } from '@services/auth/auth.service';
import { InstructorService } from '@services/instructor/instructor.service';
import { LessonService } from '@services/lesson/lesson.service';
import { User } from '@models/user';
import { Lesson } from '@models/lesson';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-instructor-assign-lesson',
  templateUrl: './instructor-assign-lesson.component.html',
  styleUrls: ['./instructor-assign-lesson.component.css']
})
export class InstructorAssignLessonComponent implements OnInit {
  user: User = null;
  instructor: Instructor = null;
  lessons$: Observable<Lesson[]>;
  instructorLessons: Lesson[] = [];
  instructorLessonsIds: number[] = [];
  instructorId = 0;
  instructorName = '';

  constructor(
    public authService: AuthService,
    private instructorService: InstructorService,
    private lessonService: LessonService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.route.paramMap.subscribe(async (params) => {
      this.instructorId = Number(params.get('instructorId')) || 0;
      if (this.instructorId > 0) {
        try {
          this.instructor =
              await this.instructorService.getInstructorById(this.instructorId)
              .toPromise();
          
          this.instructorName = this.instructor.title + ' ' +
            this.instructor.firstName + ' ' + this.instructor.lastName;  
          this.initializeLessons();
        } catch (error) {
          console.error(error);
          await swal({
            title: 'Error',
            text: 'Unknown Instructor',
            type: 'error'
          });
          this.backToList();
        }
      } else {
        await swal({
          title: 'Error',
          text: 'Unknown Instructor',
          type: 'error'
        });
        this.backToList();
      }
    });
  }

  async initializeLessons() {
    this.instructorLessons = await this.instructorService.getInstructorLessons(this.instructorId).toPromise();
    this.instructorLessons.forEach(lesson => {
      this.instructorLessonsIds.push(lesson.id);
    });
    this.lessons$ = this.lessonService.getAllLessons();
  }

  backToList() {
    this.router.navigateByUrl('/administration/instructors');
  }

  editInstructor(instructor: Instructor) {
    this.router.navigateByUrl('/administration/instructors/update/' + instructor.id);
  }

  async assignLesson(lesson: Lesson) {
    console.log(lesson);
    try {
      let response = await this.instructorService.assignLesson(this.instructor, lesson).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Lesson Assigned Successfully',
        type: 'success',
        timer: 3000
      });
      this.instructorLessonsIds = [];
      this.initializeLessons();
    } catch (error) {
      console.error(error);
      await swal({title: 'Response Status', text: error.error, type: 'error'});
    }
  }

  async removeLesson(lesson: Lesson) {
    console.log(lesson);
    try {
      let response = await this.instructorService.removeLesson(this.instructor, lesson).toPromise();
      console.log(response);
      await swal({
        title: 'Response Status',
        text: 'Lesson Removed Successfully',
        type: 'success',
        timer: 3000
      });
      this.instructorLessonsIds = [];
      this.initializeLessons();
    } catch (error) {
      console.error(error);
      await swal({
        title: 'Response Status',
        text: error.error,
        type: 'error'
      });
    }
  }

  hasLesson(lesson: Lesson) {
    if (this.instructorLessonsIds.length === 0) {
      return false;
    }
    return this.instructorLessonsIds.includes(lesson.id);
  }

}
