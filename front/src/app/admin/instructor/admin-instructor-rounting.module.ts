import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminInstructorContainerComponent } from '@admin/instructor/admin-instructor-container/admin-instructor-container.component';
import { InstructorListComponent } from '@admin/instructor/instructor-list/instructor-list.component';
import { InstructorCreateComponent } from '@admin/instructor/instructor-create/instructor-create.component';
// import { InstructorUpdateComponent } from '@admin/instructor/instructor-update/instructor-update.component';
import { InstructorShowComponent } from '@admin/instructor/instructor-show/instructor-show.component';
import { AdminGuard } from '@services/admin-guard/admin.guard';
import { InstructorUpdateComponent } from '@admin/instructor/instructor-update/instructor-update.component';
import { InstructorAssignLessonComponent } from '@admin/instructor/instructor-assign-lesson/instructor-assign-lesson.component';


const adminInstructorRoutes: Routes = [{
  path: '',
  component: AdminInstructorContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: InstructorListComponent},
    {path: 'create', component: InstructorCreateComponent},
    {path: 'update/:instructorId', component: InstructorUpdateComponent},
    {path: 'show/:instructorId', component: InstructorShowComponent},
    {path: 'assign-lessons/:instructorId', component: InstructorAssignLessonComponent}
  ]
}

];

@NgModule({
  imports: [
    RouterModule.forChild(adminInstructorRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminInstructorRoutingModule { }
