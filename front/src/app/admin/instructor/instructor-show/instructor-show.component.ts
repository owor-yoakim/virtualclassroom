import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Instructor} from '@models/instructor';
import {Lesson} from '@models/lesson';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {FileUploadService} from '@services/file-upload/file-upload.service';
import {InstructorService} from '@services/instructor/instructor.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-instructor-show',
  templateUrl: './instructor-show.component.html',
  styleUrls: ['./instructor-show.component.css']
})
export class InstructorShowComponent implements OnInit {
  user: User = null;
  instructor$: Observable<Instructor>;
  instructorLessons$: Observable<Lesson[]>;

  instructorId = 0;

  constructor(
      private authService: AuthService,
      private instructorService: InstructorService,
      private fileUploadService: FileUploadService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.route.paramMap.subscribe((params) => {
      this.instructorId = Number(params.get('instructorId')) || 0;

      if (this.instructorId > 0) {
        this.instructor$ =
            this.instructorService.getInstructorById(this.instructorId);
        this.instructorLessons$ =
            this.instructorService.getInstructorLessons(this.instructorId);
      } else {
        swal({title: 'Error', text: 'Unknown Instructor', type: 'error'});
        this.backToList();
      }
    });
  }

  editInstructor(instructor: Instructor) {
    console.log(instructor);
    this.router.navigateByUrl(
        '/administration/instructors/update/' + instructor.id);
  }

  backToList() {
    this.router.navigateByUrl('/administration/instructors');
  }

  lessonAssignments() {
    this.router.navigateByUrl('/administration/instructors/assign-lessons/' + this.instructorId);
  }

  viewAllLessons() {
    this.router.navigateByUrl('/administration/lessons');
  }

  editLesson(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/update/' + lesson.id);
  }

  viewLesson(lesson: Lesson) {
    this.router.navigateByUrl('/administration/lessons/show/' + lesson.id);
  }


}
