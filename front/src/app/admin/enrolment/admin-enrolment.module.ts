import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminEnrolmentRoutingModule } from '@admin/enrolment/admin-enrolment-rounting.module';
import { SharedModule } from '@shared/shared.module';
import { AdminEnrolmentContainerComponent } from '@admin/enrolment/admin-enrolment-container/admin-enrolment-container.component';
import { EnrolmentListComponent } from '@admin/enrolment/enrolment-list/enrolment-list.component';
import { EnrolmentCreateComponent } from '@admin/enrolment/enrolment-create/enrolment-create.component';
import { EnrolmentUpdateComponent } from '@admin/enrolment/enrolment-update/enrolment-update.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminEnrolmentRoutingModule
  ],
  declarations: [
    AdminEnrolmentContainerComponent,
    EnrolmentListComponent,
    EnrolmentCreateComponent,
    EnrolmentUpdateComponent
  ]
})
export class AdminEnrolmentModule { }
