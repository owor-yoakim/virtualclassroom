import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Enrolment} from '@models/enrolment';
import {Lesson} from '@models/lesson';
import {Student} from '@models/student';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonService} from '@services/lesson/lesson.service';
import {StudentService} from '@services/student/student.service';
import { EnrolmentService } from '@services/enrolment/enrolment.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';


@Component({
  selector: 'app-enrolment-create',
  templateUrl: './enrolment-create.component.html',
  styleUrls: ['./enrolment-create.component.css']
})
export class EnrolmentCreateComponent implements OnInit {
  lessons$: Observable<Lesson[]>;
  students$: Observable<Student[]>;
  enrolment: Enrolment;
  user: User = null;
  enrolmentForm: FormGroup;
  now = (new Date).toISOString();

  constructor(
    public authService: AuthService,
    private lessonService: LessonService,
    private studentService: StudentService,
    private enrolmentService: EnrolmentService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = this.authService.getAdminUser();
    this.students$ = this.studentService.getAllStudents();
    this.lessons$ = this.lessonService.getAllLessons();
    this.enrolment = new Enrolment();
    this.enrolment.active = true;
    this.createForm();
  }

  createForm() {
    this.enrolmentForm = new FormGroup({
      lessonId: new FormControl('', [Validators.required]),
      studentId: new FormControl('', [Validators.required]),
      enrolmentDate: new FormControl('', [Validators.required]),
      enrolmentStatus: new FormControl('', [])
    });
  }

  async createEnrolment() {
    console.log(this.enrolment);
    try {
      let response = await this.enrolmentService.createEnrolment(this.enrolment).toPromise();
      await swal({
        title: 'Response Status',
        text: 'Enrolment created successfully',
        type: 'success',
        timer: 3000
      });
      this.enrolmentForm.reset();
      this.enrolment = new Enrolment();
      this.enrolment.active = true;
    } catch (error) {
      console.error(error);
      await swal({
        title: 'Response Status',
        text:  error.error,
        type: 'error'
      });
    }
  }

  backToList() {
    this.router.navigateByUrl('/administration/enrolments');
  }

}
