import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolmentCreateComponent } from './enrolment-create.component';

describe('EnrolmentCreateComponent', () => {
  let component: EnrolmentCreateComponent;
  let fixture: ComponentFixture<EnrolmentCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolmentCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolmentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
