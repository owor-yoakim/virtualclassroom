import {AdminEnrolmentContainerComponent} from '@admin/enrolment/admin-enrolment-container/admin-enrolment-container.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from '@services/admin-guard/admin.guard';
import { EnrolmentListComponent } from '@admin/enrolment/enrolment-list/enrolment-list.component';
import { EnrolmentCreateComponent } from '@admin/enrolment/enrolment-create/enrolment-create.component';
import { EnrolmentUpdateComponent } from '@admin/enrolment/enrolment-update/enrolment-update.component';


const adminEnrolmentRoutes: Routes = [{
  path: '',
  component: AdminEnrolmentContainerComponent,
  canActivateChild: [AdminGuard],
  children: [
    {path: '', component: EnrolmentListComponent},
    {path: 'create', component: EnrolmentCreateComponent},
    {path: 'edit/:enrolmentId', component: EnrolmentUpdateComponent}
  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(adminEnrolmentRoutes)],
  exports: [RouterModule]
})
export class AdminEnrolmentRoutingModule { }
