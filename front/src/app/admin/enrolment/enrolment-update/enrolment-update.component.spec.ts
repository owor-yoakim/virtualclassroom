import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrolmentUpdateComponent } from './enrolment-update.component';

describe('EnrolmentUpdateComponent', () => {
  let component: EnrolmentUpdateComponent;
  let fixture: ComponentFixture<EnrolmentUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrolmentUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrolmentUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
