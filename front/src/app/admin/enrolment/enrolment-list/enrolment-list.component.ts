import 'rxjs/operators/filter';

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Enrolment} from '@models/enrolment';
import {Lesson} from '@models/lesson';
import {Student} from '@models/student';
import {AuthService} from '@services/auth/auth.service';
import {EnrolmentService} from '@services/enrolment/enrolment.service';
import {LessonService} from '@services/lesson/lesson.service';
import {StudentService} from '@services/student/student.service';
import {Observable} from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-enrolment-list',
  templateUrl: './enrolment-list.component.html',
  styleUrls: ['./enrolment-list.component.css']
})
export class EnrolmentListComponent implements OnInit {
  enrolments$: Observable<Enrolment[]>;
  filteredEnrolments$: Observable<Enrolment[]>;
  lessons$: Observable<Lesson[]>;
  students$: Observable<Student[]>;
  selectedStudent = 0;
  selectedLesson = 0;
  status = -1;
  constructor(
      public authService: AuthService,
      public enrolmentService: EnrolmentService,
      public lessonService: LessonService,
      public studentService: StudentService, private router: Router,
      private route: ActivatedRoute) {}

  ngOnInit() {
    this.enrolments$ = this.enrolmentService.getAllEnrolments();
    this.lessons$ = this.lessonService.getAllLessons();
    this.students$ = this.studentService.getAllStudents();
    // this.filterEnrolments();
    this.filteredEnrolments$ = this.enrolments$;
  }

  filterEnrolments() {
    this.selectedLesson = Number(this.selectedLesson);
    this.selectedStudent = Number(this.selectedStudent);
    this.status = Number(this.status);
    console.log(this.selectedStudent, this.selectedLesson, this.status);
    this.filteredEnrolments$ = this.enrolments$.map((enrolments) => {
      if (this.selectedStudent > 0) {
        enrolments = enrolments.filter(
          enrolment => enrolment.studentId === this.selectedStudent);
      }

      if (this.selectedLesson > 0) {
        enrolments = enrolments.filter(
          enrolment => enrolment.lessonId === this.selectedLesson);
      }

      if (this.status === 0) {
        enrolments =
          enrolments.filter(enrolment => enrolment.active === false);
      }

      if (this.status === 1) {
        enrolments =
          enrolments.filter(enrolment => enrolment.active === true);
      }

      return enrolments;
    });
  }


  createEnrolment() {
    this.router.navigateByUrl('/administration/enrolments/create');
  }

  async deleteEnrolment(enrolment: Enrolment) {
    try {
      let confirm = await swal({
        title: 'Are you sure?',
        text: 'The changes cannot be undone.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        cancelButtonText: 'Cancel'
      });
      if (confirm.value) {
        let response =
            await this.enrolmentService.deleteEnrolment(enrolment).toPromise();
        await swal({
          title: 'Response Status',
          text: 'Enrolment Deleted Successfully.',
          type: 'success',
          timer: 3000
        });
        this.enrolments$ = this.enrolmentService.getAllEnrolments();
        this.filterEnrolments();
      }
    } catch (error) {
      console.error(error);
      await swal({title: 'Response Status', text: error.error, type: 'error'});
    }
  }
}
