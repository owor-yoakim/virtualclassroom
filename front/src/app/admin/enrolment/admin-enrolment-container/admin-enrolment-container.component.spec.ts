import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEnrolmentContainerComponent } from './admin-enrolment-container.component';

describe('AdminEnrolmentContainerComponent', () => {
  let component: AdminEnrolmentContainerComponent;
  let fixture: ComponentFixture<AdminEnrolmentContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEnrolmentContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEnrolmentContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
