import { Lesson } from './lesson';
import { Instructor } from './instructor';

export class Subject {
    public id: number;
    public title: string;
    public slug: string;
    public description: string;
    public userId: number;
    public lessons?: Lesson[];
    public instructors?: Instructor[];
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
