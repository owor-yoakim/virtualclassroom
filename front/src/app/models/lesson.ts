import { Subject } from './subject';
import { LessonModule } from '@models/lesson-module';

export class Lesson {
    public id: number;
    public title: string;
    public slug: string;
    public description: string;
    public outline: string;
    public outcome: string;
    public prerequisites: string[];
    public avatar: string;
    public subjectId: number;
    public userId: number;
    public subject?: Subject;
    public startsOn: string;
    public endsOn: string;
    public status: string;
    public lessonModules?: LessonModule[];
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
