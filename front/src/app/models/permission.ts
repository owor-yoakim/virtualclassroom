export class Permission {
    public id: number;
    public title: string;
    public slug: string;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
