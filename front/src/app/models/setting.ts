export class Setting {
    public id: number;
    public key: string;
    public value: string;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
