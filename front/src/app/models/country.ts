export class Country {
    public id: number;
    public code: string;
    public name: string;
    public dialingCode: number;
    public currencySymbol: string;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
