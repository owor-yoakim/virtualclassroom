export class Page {
    public id: number;
    public title: string;
    public slug: string;
    public body: string;
    public published: boolean;
    public userId: number;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
