export class LessonFile {
    public id: number;
    public name: string;
    public size: number;
    public type: string;
    public localPath: string;
    public path: string;
    public lessonId: number;
    public scheduleId: number;
    public instructorId: number;
    public studentId: number;
    public description: string;
}
