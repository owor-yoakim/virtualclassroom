import { Student } from './student';
import { Lesson } from './lesson';

export class Enrolment {
    public studentId: number;
    public lessonId: number;
    public enrolmentDate: string;
    public active: boolean;
    public student?: Student;
    public lesson?: Lesson;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
