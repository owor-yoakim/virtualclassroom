import { Country } from '@models/country';

export class Address {
    public id: number;
    public type: string;
    public line_1: string;
    public line_2: string;
    public city: string;
    public state: string;
    public countryId: number;
    public zip: string;
    public country?: Country;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
