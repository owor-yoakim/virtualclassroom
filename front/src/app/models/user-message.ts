export class UserMessage {
    public id: number;
    public senderId: number;
    public receiverId: number;
    public senderEmail: string;
    public receiverEmail: string;
    public subject: string;
    public message: string;
    public read: boolean;
    public userId: number;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}

