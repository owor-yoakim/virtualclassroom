import { User } from '@models/user';
import { Enrolment } from '@models/enrolment';

export class Student extends User {
    public userId: number;
    public enrolments?: Enrolment[];
}
