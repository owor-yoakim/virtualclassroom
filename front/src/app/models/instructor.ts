import { User } from '@models/user';

export class Instructor extends User {
    public title: string;
    public userId: number;
    public qualification: string;
    public specialization: string;
    public experience: number;
    public institution: string;
    public profile: string;
}

