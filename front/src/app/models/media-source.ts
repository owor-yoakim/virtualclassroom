export class MediaSource {
    public id: string;
    public name: string;
    public type: string;
}
