export class Stream {
    public id: string;
    public roomId: string;
    public initiatorId: number;
    public mediaStream: MediaStream;
}
