import { Permission } from './permission';

export class Role {
    public id: number;
    public title: string;
    public slug: string;
    public active: boolean;
    public permissions: Permission[] = [];
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
