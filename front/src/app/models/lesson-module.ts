import { Lesson } from './lesson';
import { Instructor } from './instructor';
import { LessonVideo } from '@models/lesson-video';
import { LessonFile } from '@models/lesson-file';

export class LessonModule {
    public id: number;
    public title: string;
    public description: string;
    public lessonId: number;
    public duration: number;
    public scheduleDate: string;
    public startTime: string;
    public status: string;
    public userId: number;
    public lesson?: Lesson;
    public lessonModuleFiles?: LessonFile[];
    public lessonModuleVideos?: LessonVideo[];
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
