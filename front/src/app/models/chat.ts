import { Student } from '@models/student';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';

export class Chat {
    public id: number;
    public studentId: number = null;
    public instructorId: number = null;
    public lessonId: number = null;
    public message = '';
    public fileUrl = '';
    public fileName = '';
    public lesson?: Lesson;
    public student?: Student;
    public instructor?: Instructor;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;
}
