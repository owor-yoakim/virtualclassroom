import { Role } from './role';
import { Chat } from './chat';
import { Address } from '@models/address';
import { UserMessage } from '@models/user-message';
import { LessonComment } from '@models/lesson-comment';

export class User {
    public id: number;
    public firstName: string;
    public lastName: string;
    public gender: string;
    public emailAddress: string;
    public phoneNumber: string;
    public addressId?: number;
    public address?: Address;
    public username: string;
    public password: string;
    public active: boolean;
    public avatar: string;
    public type: string;
    public joinDate: string;
    public roleId: number;
    public role: Role;
    public createdAt: string;
    public updatedAt: string;
    public deletedAt: string;

    public hasAccess(permissionSlug: string) {
        let permission = this.role.permissions.find(x => {
            return x.slug === permissionSlug;
        });

        return (permission !== undefined && permission !== null);
    }
}
