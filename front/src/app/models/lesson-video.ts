import {Lesson} from '@models/lesson';
import {LessonModule} from '@models/lesson-module';

export class LessonVideo {
  public id: number;
  public fileName: string;
  public title: string;
  public size: number;
  public extension: string;
  public duration: number;
  public localPath: string;
  public path: string;
  public lessonId: number;
  public moduleId: number;
  public description: string;
  public lesson?: Lesson;
  public lessonModule?: LessonModule;
  public createdAt: string;
  public updatedAt: string;
  public deletedAt: string;
}
