import { LessonModule } from '@models/lesson-module';

export class LessonComment {
    public id: number;
    public name: string;
    public email: string;
    public body: string;
    public lessonId: number;
    public lessonModuleId?: number;
    public lessonModule?: LessonModule;
    public createdAt: string;
    public updatedAt: string;
    public deletedA: string;
}
