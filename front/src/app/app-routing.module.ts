import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '@home/not-found/not-found.component';
import { TestComponent } from '@shared/test/test.component';
import { UdacityComponent } from '@shared/udacity/udacity.component';


const appRoutes: Routes = [
    {
        path: '',
        loadChildren: 'app/home/home.module#HomeModule'
    },
    {
        path: 'instructors',
        loadChildren: 'app/instructor/instructor.module#InstructorModule'
    },
    {
        path: 'lessons',
        loadChildren: 'app/lesson/lesson.module#LessonModule'
    },
    {
        path: 'account',
        loadChildren: 'app/account/account.module#AccountModule'
    },

    {
        path: 'administration',
        loadChildren: 'app/admin/admin.module#AdminModule'
    },

    {
        path: 'test',
        component: TestComponent
    },
    {
        path: 'whiteboard',
        component: UdacityComponent
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [
      RouterModule.forRoot(appRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class AppRoutingModule {}

