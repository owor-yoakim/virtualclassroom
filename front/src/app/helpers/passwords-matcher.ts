import { FormControl, AbstractControl } from '@angular/forms';

export const passwordsMatcher = (control: AbstractControl): { [key: string]: boolean } => {
    const fieldValue1 = control.get('password1');
    const fieldValue2 = control.get('password2');
    if (!fieldValue1 || !fieldValue2) { return null; }
    if (fieldValue1.value === fieldValue2.value) { return null; }
    return { nomatch: true };
};
