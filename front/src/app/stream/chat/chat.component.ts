import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgModel, NgForm, FormGroup, FormControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs/Subscription';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import swal from 'sweetalert2';

import { AppConstant } from '@config/app-constant';
import { DateTimeHelper } from '@helpers/date-time-helper';

import { Student } from '@models/student';
import { Lesson } from '@models/lesson';
import { Chat } from '@models/chat';
import { User } from '@models/user';

import { ChatService } from '@services/chat/chat.service';
import { AuthService } from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';
import { SocketService } from '@services/socket/socket.service';
import { FileUploadService } from '@services/file-upload/file-upload.service';
import { LessonFile } from '@models/lesson-file';
import { HttpEventType, HttpResponse } from '@angular/common/http';



@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  /*  the lesson to be streamed  */
  @Input('lesson') lesson: Lesson;
  @Input('connection') connection: any;

  chats: Chat[] = [];
  /* the chat message object   */
  messageData = {
    clientId: 0,
    lessonId: 0,
    messageBody: '',
    date: ''
  };
  /* the signaling server url   */
  serverUrl = AppConstant.serverUrl;
  /*  the logged in client  */
  client: User = null;
  /* the current chat  */
  chat: Chat = new Chat();
  user: User = null;
  chunkSize = 5;
  chunkedChats: Chat[] = [];

  connected = true;

  dtHelper = new DateTimeHelper();

  hasFile = false;
  selectedFiles: FileList;
  file: File;
  progressPercentage = 0;
  lessonFile: LessonFile = null;
  chatForm: FormGroup;
  openPopup: Function;
  inputClass = 'form-control';

  constructor(
    public authService: AuthService,
    private chatService: ChatService,
    private socketService: SocketService,
    private studentService: StudentService,
    private uploadService: FileUploadService,
    private router: Router) { }

  async ngOnInit() {
    // get the client
    this.user = this.authService.getUser();
    // initialize the chat
    if (this.user.type === 'learner') {
      this.chat.studentId = this.user.id;
    }
    if (this.user.type === 'instructor') {
      this.chat.instructorId = this.user.id;
    }

    this.chat.lessonId = this.lesson.id;
    

    this.chatForm = new FormGroup({
      message: new FormControl('', [Validators.required]),
      uploadFile: new FormControl('', [])
    });
    
    try {
       // get chats for this lesson
       this.chats =
           await this.chatService.getChatsByLesson(this.lesson).toPromise();
       this.chunkChats();
    } catch (err) {
      console.error(err);
    }

    /*
    this.connection.onmessage =  (event) => {
      let chat = <Chat>event.data;
      console.log('New Message: ', chat);
      // insert new message at the beginning
      this.chats.unshift(chat);
    };
*/
    // if a new message comes in
    this.socketService.on('message', (data) => {
      const chat = <Chat>JSON.parse(data);
      console.log('New Message: ', chat);
      // insert new message at the beginning
      this.chats.unshift(chat);
      this.chunkChats();
    });
  }

  setPopupAction(fn: any) {
    this.openPopup = fn;
  }

  onChange(value) {
    console.log(value);
    // this.chat.message = value;
  }

  chunkChats(isMerged = false) {
    if (isMerged) {
      this.chunkedChats = this.chats.slice(0, this.chunkedChats.length + 5);
    } else if (this.chunkedChats.length > 5) {
      this.chunkedChats = this.chats.slice(0, this.chunkedChats.length);
    } else {
      this.chunkedChats = this.chats.slice(0, 5);
    }
  }

  async sendMessage() {
    try {
      let chat = await this.chatService.createChat(this.chat).toPromise();

      this.socketService.emit('message', JSON.stringify(chat));
      // this.connection.send(this.chat);
      this.chats.unshift(chat);
      this.chunkChats();
      this.chat.message = '';

    } catch (err) {
      console.error(err);
    }
  }

  getChats() {

  }

  onFileSelected(event: any) {
    console.log(event.target.files);
    this.selectedFiles = event.target.files;
    console.log(this.selectedFiles.length);
    this.hasFile = this.selectedFiles.length > 0;
  }

  async uploadFile() {
    this.progressPercentage = 0;
    // console.log(this.selectedFiles.length);
    // return;
    if (this.hasFile) {
      // upload file first
      this.file = this.selectedFiles.item(0);
    this.lessonFile = new LessonFile();
      this.lessonFile.studentId = null;
    this.lessonFile.instructorId = null;

    if (this.authService.isStudentAuthenticated()) {
      this.lessonFile.studentId = this.user.id;
    } else if (this.authService.isInstructorAuthenticated()) {
      this.lessonFile.instructorId = this.user.id;
    }
    this.lessonFile.lessonId = this.lesson.id;
    this.lessonFile.name = this.file.name;
    this.lessonFile.type = this.file.type;
    this.lessonFile.size = this.file.size;
    this.lessonFile.description = this.chat.message;

    this.uploadService.uploadFile('/lesson/upload',
      this.lessonFile, this.file,
      this.lesson).subscribe(async (response) => {
          if (response.type === HttpEventType.UploadProgress) {
            this.progressPercentage =
                Math.round(100 * response.loaded / response.total);
          } else if (response instanceof HttpResponse) {
            // schend the message from here
            let lessonFile = <LessonFile>JSON.parse(<any>response.body);
            // console.log('File uploaded! ', lessonFile);
            this.chat.fileUrl = this.serverUrl + '/' + lessonFile.localPath;
            this.chat.fileName = lessonFile.name;
            try {
              let chat =
                  await this.chatService.createChat(this.chat).toPromise();

              this.socketService.emit('message', JSON.stringify(chat));
              // this.connection.send(this.chat);
              this.chats.unshift(chat);
              this.chunkChats();
              this.chat.message = '';

            } catch (err) {
              console.error(err);
            }
          }
      }, (err) => {
        console.error(err);
      });
    this.selectedFiles = null;
      this.hasFile = false;
    } else {
      // schend the message from here
            try {
              let chat =
                  await this.chatService.createChat(this.chat).toPromise();

              this.socketService.emit('message', JSON.stringify(chat));
              // this.connection.send(this.chat);
              this.chats.unshift(chat);
              this.chunkChats();
              this.chat.message = '';

            } catch (err) {
              console.error(err);
            }
    }
    
  }
}
