import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@translate/translate.module';

import { StreamComponent } from '@stream/stream/stream.component';
import { BroadcastComponent } from '@stream/broadcast/broadcast.component';
import { ChatComponent } from '@stream/chat/chat.component';
import { InstructorComponent } from '@stream/instructor/instructor.component';
import { LearnerComponent } from '@stream/learner/learner.component';
import { ClassroomComponent } from '@stream/classroom/classroom.component';
import { WhiteboardComponent } from '@stream/whiteboard/whiteboard.component';



@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [
    StreamComponent,
    BroadcastComponent,
    ChatComponent,
    ClassroomComponent,
    InstructorComponent,
    LearnerComponent,
    WhiteboardComponent
  ],
  exports: [
    StreamComponent,
    BroadcastComponent,
    ChatComponent,
    ClassroomComponent,
    InstructorComponent,
    LearnerComponent,
    WhiteboardComponent
  ]
})
export class StreamModule { }
