import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { CanvasWhiteboardComponent, CanvasWhiteboardService, CanvasWhiteboardUpdate} from 'ng2-canvas-whiteboard';
import { CanvasWhiteboardOptions } from 'ng2-canvas-whiteboard/dist/canvas-whiteboard.component';
import {User} from '@models/user';
import {LessonModule} from '@models/lesson-module';

@Component({
  selector: 'app-whiteboard',
  viewProviders: [CanvasWhiteboardComponent],
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.css']
})
export class WhiteboardComponent implements OnInit {
  @ViewChild('canvasWhiteboard') canvasWhiteboard: CanvasWhiteboardComponent;
  @Input('user') user: User = null;
  @Input('lessonModule') lessonModule: LessonModule = null;

  canvasOptions: CanvasWhiteboardOptions = {
    drawButtonEnabled: true,
    drawButtonClass: 'fa fa-pencil fa-2x',
    clearButtonEnabled: true,
    clearButtonClass: 'fa fa-eraser fa-2x',
    undoButtonClass: 'fa fa-undo fa-2x',
    undoButtonEnabled: true,
    redoButtonClass: 'fa fa-repeat fa-2x',
    redoButtonEnabled: true,
    colorPickerEnabled: true,
    saveDataButtonEnabled: true,
    saveDataButtonClass: 'fa fa-save fa-2x',
    lineWidth: 5,
    strokeColor: 'rgb(255,0,0)',
    shouldDownloadDrawing: true
  };
  constructor(private canvasWhiteboardService: CanvasWhiteboardService) { }

  ngOnInit() {
  }

  sendBatchUpdate(updates: CanvasWhiteboardUpdate[]) {
    console.log(updates);
  }

  onCanvasDraw(event: any) {
    console.log('Drawing on the canvas');
  }

  onCanvasClear() {
    console.log('The canvas was cleared');
  }
  onCanvasUndo(updateUUID: string) {
    console.log(`UNDO with uuid: ${updateUUID}`);
  }
  onCanvasRedo(updateUUID: string) {
    console.log(`REDO with uuid: ${updateUUID}`);
  }

  

}
