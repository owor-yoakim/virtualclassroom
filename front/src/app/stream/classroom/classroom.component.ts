import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';

import { AuthService } from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';
import { InstructorService } from '@services/instructor/instructor.service';
import { User } from '@models/user';
import { Student } from '@models/student';
import { Instructor } from '@models/instructor';
import { Lesson } from '@models/lesson';
import { Enrolment } from '@models/enrolment';
import { LessonService } from '@services/lesson/lesson.service';
import { CanComponentDeactivate } from '@services/can-deactivate/can-deactivate.guard';


@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css']
})
export class ClassroomComponent implements OnInit {
  loading = false;
  user: User = null;
  student: Student = null;
  instructor: Instructor = null;
  lesson: Lesson = null;
  lessons: Lesson[] = [];
  enrolments: Enrolment[] = [];
  lessonSlug: string = null;

  constructor(
      public authService: AuthService, private studentService: StudentService,
      private instructorService: InstructorService,
      private lessonService: LessonService, private router: Router,
      private route: ActivatedRoute) {}

  async ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();
    this.route.paramMap.subscribe((params) => {
      // console.log(params);
      this.lessonSlug = params.get('lessonSlug');
      console.log(this.lessonSlug);
    });

    if (this.lessonSlug === null) {
      await swal({ title: 'Unknown lesson', type: 'error', timer: 3000 });
      this.router.navigate(['/account/my-lessons']);
      return;
    }

    try {
      this.lesson = await this.lessonService.getLessonBySlug(this.lessonSlug).toPromise();
      console.log(this.lesson);
      
      if (this.user.type === 'learner') {
        let student = this.studentService.getStudentById(this.user.id).toPromise();
        let enrolments = this.studentService.getEnrolments(this.user.id).toPromise();
        this.student = await student;
          this.enrolments = await enrolments;

          this.enrolments.forEach((enrolment) => {
            this.lessons.push(enrolment.lesson);
          });

          this.loading = false;

        } else if (this.user.type === 'instructor') {
          let instructor = this.instructorService.getInstructorById(this.user.id).toPromise();
          let lessons =
              this.instructorService.getInstructorLessons(this.user.id)
                  .toPromise();

          this.instructor = await instructor;
          this.lessons = await lessons;

          this.loading = false;
        
        } else {
          swal({ title: 'Access Denied!', type: 'error', timer: 3000 });
          this.router.navigate(['/account/my-lessons']);
          return;
        }
      } catch (err) {
        await swal({ title: 'Internal Server Erroor', text: err, type: 'error', timer: 3000 });
        this.router.navigate(['/account/my-lessons']);
        return;
      }

  }



}
