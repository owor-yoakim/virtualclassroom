import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/pairwise';

import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {LessonModule} from '@models/lesson-module';
import {AuthService} from '@services/auth/auth.service';
import {SocketService} from '@services/socket/socket.service';
import {Observable} from 'rxjs/Observable';
import * as screenshot from 'screenshot-desktop';

interface Point {
  x: number;
  y: number;
}

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements AfterViewInit {
  // the canvas element reference
  @ViewChild('canvas') canvas: ElementRef;
  // setting a width and height for the canvas
  @Input() public width = 720;
  @Input() public height = 480;
  // the lesson module being taught
  @Input('lessonModule') lessonModule: LessonModule = null;

  lineWidth = 2;
  eraserLineWidth = 20;
  bgColor = '#FFFFFF';
  curColor = '#FF0000';
  remoteColor = '#0000FF';
  tool = 'pencil';
  isErasing = false;
  savedCanvasCount = 0;
  requestedInstructorWhiteboard = false;

  private context: CanvasRenderingContext2D;


  constructor(
    public authService: AuthService, private socketService: SocketService) { }

  ngAfterViewInit() {
    // get the context
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.context = canvasEl.getContext('2d');
    const canvasContainer = document.getElementById('canvasContainer');
    // set the width and height
    canvasEl.width = canvasContainer.scrollWidth;
    canvasEl.height = this.height;
    // canvasEl.width = this.width;
    // canvasEl.height = this.height;
    // set some default properties about the line
    this.context.lineWidth = this.lineWidth;
    this.context.lineCap = 'round';
    this.context.strokeStyle = this.curColor;
    this.context.canvas.style.backgroundColor = this.bgColor;
    // start capturing mouse events
    this.captureEvents(canvasEl);
    // this.handleMouseEvents(canvasEl);


    this.socketService.on(
      'draw', (data: { prevPos: Point, currentPos: Point }) => {
        // if user receives the drawing from instructor whiteboard
        this.drawOnCanvas(data.prevPos, data.currentPos, this.remoteColor);
      });

    this.socketService.on('cleardrawing', (data) => {
      // Clears the canvas
      this.isErasing = false;
      this.context.clearRect(
        0, 0, this.context.canvas.width, this.context.canvas.height);
    });

    this.socketService.on('changestrokecolor', (data) => {
      // erase the canvas
      if (data.color === this.bgColor.toLowerCase()) {
        this.isErasing = true;
      } else {
        this.curColor = data.color;
        this.isErasing = false;
      }
    });

    this.socketService.on('starterasing', (data) => {
      // erase the canvas
      this.isErasing = true;
    });

    this.socketService.on('stoperasing', (data) => {
      // stop erase the canvas
      this.isErasing = false;
    });

    this.socketService.on('changelinewidth', (data) => {
      // change line width
      this.lineWidth = data.lineWidth;
    });
  }


  captureEvents(canvasEl: HTMLCanvasElement) {
    Observable
        // this will capture all mousedown events from teh canvas element
        .fromEvent(canvasEl, 'mousedown')
        .switchMap((e) => {
          return Observable
              // after a mouse down, we'll record all mouse moves
              .fromEvent(canvasEl, 'mousemove')
              // we'll stop (and unsubscribe) once the user releases the mouse
              // this will trigger a 'mouseup' event
              .takeUntil(Observable.fromEvent(canvasEl, 'mouseup'))
              // we'll also stop (and unsubscribe) once the mouse leaves the
              // canvas (mouseleave event)
              // .takeUntil(Observable.fromEvent(canvasEl, 'mouseleave'))
              // pairwise lets us get the previous value to draw a line from
              // the previous point to the current point
              .pairwise();
        })
        .subscribe((res: [MouseEvent, MouseEvent]) => {
          const rect = canvasEl.getBoundingClientRect();

          // previous and current position with the offset
          const prevPos: Point = {
            x: res[0].clientX - rect.left,
            y: res[0].clientY - rect.top
          };

          const currentPos: Point = {
            x: res[1].clientX - rect.left,
            y: res[1].clientY - rect.top
          };

          // this method we'll implement soon to do the actual drawing
          this.drawOnCanvas(prevPos, currentPos);
          this.socketService.emit(
              'draw', {prevPos: prevPos, currentPos: currentPos});
        });
  }

  private drawOnCanvas(prevPos: Point, currentPos: Point, color?: string) {
    // incase the context is not set
    if (!this.context) {
      return;
    }

    switch (this.tool) {
      case 'pencil':
        this.drawShape(prevPos, currentPos, color);
        break;
    }
  }

  private drawShape(prevPos: Point, currentPos: Point, color?: string) {
    // we're drawing lines so we need a previous position
    if (prevPos) {
      if (prevPos.x === currentPos.x && prevPos.y === currentPos.y) {
        // draw a point
        this.drawPoint(currentPos, color);
      } else {
        // start our drawing path
        this.context.beginPath();
        // sets the start point
        this.context.moveTo(prevPos.x, prevPos.y);  // from
        // draws a line from the start pos until the current position
        this.context.lineTo(currentPos.x, currentPos.y);
        if (this.isErasing) {
          this.context.strokeStyle = this.bgColor;
          this.context.lineWidth = this.eraserLineWidth;
        } else {
          this.context.strokeStyle = color || this.curColor;
          this.context.lineWidth = this.lineWidth;
        }
        // strokes the current path with the styles we set earlier
        this.context.closePath();
        this.context.stroke();
      }
    }
  }

  drawPoint(point: Point, color?: string) {
    // draw a dot
    this.context.beginPath();
    this.context.arc(point.x, point.y, 3, 0, Math.PI * 2, true);
    this.context.fillStyle = color || this.curColor;
    this.context.closePath();
    this.context.fill();
  }

  private drawLine(start: Point, end: Point) {
    if (start) {
      // start our drawing path
      this.context.beginPath();
      this.context.moveTo(start.x, start.y);
      this.context.lineTo(end.x, end.y);
      this.context.strokeStyle = this.curColor;
      this.context.stroke();
    }
  }

  private drawRect(start: Point, end: Point) {
    if (start) {
      // start our drawing path
      this.context.beginPath();
      const width = Math.abs(end.x - start.x);
      const height = Math.abs(end.y - start.y);
      this.context.lineWidth = this.lineWidth;
      this.context.moveTo(start.x, start.y);
      this.context.strokeRect(start.x, start.y, width, height);
      this.context.strokeStyle = this.curColor;
    }
  }

  private fillRect(start: Point, width: number, height: number) {
    if (start) {
      // start our drawing path
      this.context.beginPath();
      this.context.moveTo(start.x, start.y);
      this.context.fillRect(start.x, start.y, width, height);
      this.context.fillStyle = this.curColor;
      this.context.fill();
    }
  }

  private drawCircle() {
    // start our drawing path
    this.context.beginPath();
  }

  private fillCircle() {
    // start our drawing path
    this.context.beginPath();
  }

  setColor(color: string) {
    if (color.toLowerCase() === this.remoteColor.toLowerCase()) {
      return;
    }
    if (color.toLowerCase() === this.bgColor.toLowerCase()) {
      this.isErasing = true;
    } else {
      this.isErasing = false;
      this.curColor = color;
    }
    // this.socketService.emit('changestrokecolor', {color: color});
    console.log(this.curColor);
  }

  eraseCanvas() {
    if (this.isErasing) {
      this.isErasing = false;
      this.socketService.emit('stoperasing', {});
    } else {
      this.isErasing = true;
      this.socketService.emit('starterasing', {});
    }
  }

  changeCanvasBackground(color: string) {
    this.bgColor = color;
    this.socketService.emit('changebackground', {color: this.bgColor});
  }

  onChangeWidth() {
    if (this.authService.isInstructorAuthenticated()) {
      this.socketService.emit('changelinewidth', { lineWidth: this.lineWidth });
    }
  }


  setTool(tool: string) {
    this.tool = tool;
    console.log(this.tool);
  }

  undoCanvas() {}

  redoCanvas() {}

  saveCanvas() {
    // this.context.getImageData(0, 0, this.context.canvas.width,
    // this.context.canvas.height);
    let dataUrl = this.context.canvas.toDataURL();
    let link = document.createElement('a');
    link.setAttribute('href', dataUrl);
    link.setAttribute('download', 'canvas.png');
    link.innerText = 'Download';
    document.getElementById('download').appendChild(link);
    link.click();
    link.remove();
  }

  clearCanvas() {
    // Clears the canvas
    this.isErasing = false;
    this.context.clearRect(
        0, 0, this.context.canvas.width, this.context.canvas.height);
    if (this.authService.isInstructorAuthenticated()) {
      this.socketService.emit('cleardrawing', {});
    }
  }




  handleMouseEvents(canvasEl: HTMLCanvasElement) {
    const rect = canvasEl.getBoundingClientRect();
    let prevPos: Point = null;
    let currentPos: Point = null;
    canvasEl.addEventListener('mousedown', (event: MouseEvent) => {
      prevPos.x = event.clientX - rect.left;
      prevPos.y = event.clientY - rect.top;
      this.draw(prevPos.x, prevPos.y, 'dragstart');
      console.log('Start: ', prevPos);
    });

    canvasEl.addEventListener('mousemove', (event: MouseEvent) => {
      currentPos.x = event.clientX - rect.left;
      currentPos.y = event.clientY - rect.top;
      this.draw(currentPos.x, currentPos.y, 'drag');
      this.socketService.emit('draw', { prevPos: prevPos, currentPos: currentPos });
    });

    canvasEl.addEventListener(
      'mouseup',
      (event: MouseEvent) => {
        currentPos.x = event.clientX - rect.left;
        currentPos.y = event.clientY - rect.top;
        this.draw(currentPos.x, currentPos.y, 'dragstop');
        this.socketService.emit('draw', { prevPos: prevPos, currentPos: currentPos });
      });
  }

  draw(x, y, type) {
    if (type === 'dragstart') {
      this.context.beginPath();
      this.context.moveTo(x, y);
    } else if (type === 'drag') {
      this.context.lineTo(x, y);
      if (this.isErasing) {
        this.context.strokeStyle = this.bgColor;
        this.context.lineWidth = this.eraserLineWidth;
      } else {
        this.context.strokeStyle = this.curColor;
        this.context.lineWidth = this.lineWidth;
      }
      this.context.stroke();
    } else {
      this.context.closePath();
    }
    return;
  }


}
