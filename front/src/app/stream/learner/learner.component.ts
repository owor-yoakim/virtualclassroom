import {Component, Input, OnInit} from '@angular/core';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Enrolment} from '@models/enrolment';
import {Lesson} from '@models/lesson';
import {Student} from '@models/student';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {BroadcastService} from '@services/broadcast/broadcast.service';
import {SocketService} from '@services/socket/socket.service';


@Component({
  selector: 'app-learner',
  templateUrl: './learner.component.html',
  styleUrls: ['./learner.component.css']
})
export class LearnerComponent implements OnInit {
  @Input('student') student: Student;
  @Input('lesson') lesson: Lesson;
  @Input('lessons') lessons: Lesson[];
  @Input('enrolments') enrolments: Enrolment[];

  user: User = null;
  hasStarted = false;
  isLive = false;
  hasJoined = false;
  socket = null;
  numberConnected = 0;
  isDataChannelOpen = false;
  stream: MediaStream = null;
  dtHelper = new DateTimeHelper();
  slug = '';
  interval = null;

  constructor(
      public broadcastService: BroadcastService,
      private socketService: SocketService, public authService: AuthService) {}

  ngOnInit() {
    this.isDataChannelOpen = false;
    this.user = this.authService.getUser();
    this.broadcastService.setUser(this.user);
    this.broadcastService.setLesson(this.lesson);
    this.socket = this.broadcastService.connection.connectSocket;
    console.log(this.broadcastService.connection, this.socket);
    this.joinClass();

    // listen for incoming streams
    this.broadcastService.connection.onstream = (event) => {
      // console.log('Got remote stream');
      if (event.isScreen) {
        // it is screen
        document.querySelector('span#remoteScreen')
            .appendChild(event.mediaElement);
        console.log('Got screen stream');
      } else {
        this.stream = event.stream;
        const vid = document.querySelector('video');
        vid.srcObject = event.stream;
      }
      this.isLive = true;
    };

    this.broadcastService.connection.onstreamended = (event) => {
      const vid = document.querySelector('video');
      vid.srcObject = null;
      this.isLive = false;
      // renegotiate while no media stream is added
      this.broadcastService.connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
      };
      // this.broadcastService.connection.renegotiate();
      // this.joinClass();
    };

    this.interval = setInterval(() => {
      this.numberConnected =
          this.broadcastService.connection.getAllParticipants().length;
      // this.broadcastService.connection.getAllParticipants().forEach(p => {
      //   console.log(this.broadcastService.connection.peers[p]);
      // });
    }, 5000);

    this.socketService.on('datachannelopened', (data) => {
      if (data.lesson.id === this.lesson.id) {
        this.isDataChannelOpen = true;
      }
    });
    this.socketService.on('datachannelclosed', (data) => {
      if (data.lesson.id === this.lesson.id) {
        this.isDataChannelOpen = false;
      }
    });
  }

  joinClass() {
    // this.broadcastService.setLesson(this.lesson);
    if (this.hasJoined) {
      this.broadcastService.connection.leave();
    }
    const vid = document.querySelector('video');
    vid.srcObject = null;
    this.broadcastService.joinClassroom();
    this.hasJoined = true;
    console.log(this.broadcastService.connection.userid);
  }

  exitClass() {
    // set flags
    this.isLive = false;
    this.hasJoined = false;
    // leave the classroom
    this.broadcastService.connection.leave();
    // close the socket
    this.broadcastService.connection.close();
  }
}
