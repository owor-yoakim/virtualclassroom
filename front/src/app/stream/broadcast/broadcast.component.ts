import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import swal from 'sweetalert2';

import { AppConstant } from '@config/app-constant';
import { Student } from '@models/student';
import { User } from '@models/user';

import { StreamService } from '@services/stream/stream.service';
import { StudentService } from '@services/student/student.service';
import { AuthService } from '@services/auth/auth.service';
import { SocketService } from '@services/socket/socket.service';





@Component({
  selector: 'app-broadcast',
  templateUrl: './broadcast.component.html',
  styleUrls: ['./broadcast.component.css']
})
export class BroadcastComponent implements OnInit {
loading = false;
  isLive = false;
  paused = false;
  hasJoined = false;
  serverUrl = AppConstant.serverUrl;
  student: User = null;
  stream: MediaStream = null;
  videoElement: HTMLVideoElement = null;
  // this peer's connection
  peerConnection: RTCPeerConnection = null;
  // used for any advertisings while the video is playing
  arrayOfWebPImages = [];
  // SimplePeer Configurations
  peerConfig: RTCConfiguration = {
    iceServers: AppConstant.iceServers
  };
  /*
    * the type of user accessing this page
    * ['guest','learner','instructor']
    */
  clientType = 'instructor';
  // number of online clients
  numberConnected = 0;
  // mediaDevices configuration options
  mediaOptions: MediaStreamConstraints = {
    video: {
      width: { min: 640, ideal: 1920, max: 1920 },
      height: { min: 480, ideal: 720, max: 1080 },
      aspectRatio: 1.77,
      frameRate: 64
    },
    audio: true
  };

  constructor(
    private authService: AuthService,
    private streamService: StreamService,
    private socketService: SocketService,
    private studentService: StudentService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

    this.student = this.authService.getUser();

    this.loading = false;

    this.socketService.on('connect', () => {
      this.hasJoined = true;
      this.socketService.emit('clientInfo', JSON.stringify(this.student));
    });

    // if a new client connectes
    this.socketService.on('newConnection', (data) => {
      this.numberConnected = Number(data);
    });

    // create the peerConnection
    // create the peer
    this.peerConnection = new RTCPeerConnection(this.peerConfig);
    // send ice candidate to signaling channel
    this.peerConnection.onicecandidate = (event) => {
      if (event.candidate) {
        this.socketService.emit('candidate', event.candidate);
      }
    };

    // create an offer
    // this.createOffer();

    console.log('Peer created', this.peerConnection);

    // start the signal handler
    this.socketService.on('offer', (offer) => {
      // create a new connection
      if (this.peerConnection === undefined) {
        // create the peer
        this.createPeerConnection();
      }
      // set remote sdp
      console.log('Peer', this.peerConnection);
      // this.setRemoteSDP(offer);
      const newSdp = new RTCSessionDescription(offer);
      this.peerConnection.setRemoteDescription(newSdp).then(() => {
        console.log('Remote SDP Set');
      }).catch((error) => {
        console.log('setRemoteSDP() Error: ' + error.message);
      });
      // create answer
      this.createAnswer();
    });


    this.socketService.on('answer', (answer) => {
      // this.setRemoteSDP(answer);
      const newSdp = new RTCSessionDescription(answer);
      this.peerConnection.setRemoteDescription(newSdp).then(() => {
        console.log('Remote SDP Set');
      }).catch((error) => {
        console.log('setRemoteSDP Error: ' + error.message);
      });
    });


    this.socketService.on('candidate', (candidate) => {
      const newCandidate = new RTCIceCandidate(candidate);
      this.peerConnection.addIceCandidate(newCandidate).then(() => {
        console.log('Remote Ice Candidate Added');
      }).catch((error) => {
        console.log('onCandidate() Error: ' + error.message);
      });
    });

    this.socketService.on('paused', () => {
      if (this.student.type === 'learner') {
        const vid = document.querySelector('video');
        vid.pause();
        vid.controls = false;
        console.log('Class Paused');
      }
    });

    this.socketService.on('resumed', () => {
      if (this.student.type === 'learner') {
        const vid = document.querySelector('video');
        vid.play();
        vid.controls = true;
        console.log('Class Resumed');
      }
    });

    this.socketService.on('classended', () => {
      if (this.student.type === 'learner') {
        this.removeRemoteStream();
        console.log('Class Ended');
        this.isLive = false;
      }
    });

    this.socketService.on('disconnected', (message) => {
      console.log('Client exit room: Remaining ', message);
      this.numberConnected = Number(message);
    });

    // listen to incoming streams
    this.peerConnection.onaddstream = (event) => {
      if (this.student.type === 'learner') {
        console.log('Received Remote Stream');
        this.stream = event.stream;
        this.isLive = true;
        this.renderRemoteStream(event.stream);
      }
    };

    // listen to stream removed event
    this.peerConnection.onremovestream = (event) => {
      if (this.student.type === 'learner') {
        this.removeRemoteStream();
        swal({ text: 'The Instructor Has Ended the class. Check again later.', type: 'info' });
      }
    };

    // console.log("Client", this.client);

    this.router.events.subscribe((event) => {
      // console.log(event);
      this.removeLocalStream();
      this.removeRemoteStream();
    });

  }

  @HostListener('window:beforeunload')
  onBeforeUnload() {
    this.removeLocalStream();
    this.removeRemoteStream();
    this.socketService.emit('exitclass', JSON.stringify(this.student));
    this.socketService.disconnect();
  }

  // creates a new peer connection
  createPeerConnection() {
    // create the peer
    this.peerConnection = new RTCPeerConnection(this.peerConfig);
    // send ice candidate to signaling channel
    this.peerConnection.onicecandidate = (event) => {
      if (event.candidate) {
        this.socketService.emit('candidate', event.candidate);
      }
    };

    console.log('Peer created', this.peerConnection);
  }

  // an offer received
  onOffer(offer) {
    // create a new connection
    if (this.peerConnection === null) {
      // create the peer
      this.createPeerConnection();
    }
    // set remote sdp
    console.log('Peer', this.peerConnection);
    // this.setRemoteSDP(offer);
    const newSdp = new RTCSessionDescription(offer);
    this.peerConnection.setRemoteDescription(newSdp).then(() => {
      console.log('Remote SDP Set');
    }).catch((error) => {
      console.log('setRemoteSDP() Error: ' + error.message);
    });
    // create answer
    this.createAnswer();
  }


  onIceCandidate(event) {
    if (event.candiadate) {
      this.socketService.emit('candidate', event.candidate);
    }
  }

  addIceCandidate(candidate) {
    const newCandidate = new RTCIceCandidate(candidate);
    this.peerConnection.addIceCandidate(newCandidate).then(() => {
      console.log('Remote Ice Candidate Added');
    }).catch((error) => {
      console.log('onCandidate() Error: ' + error.message);
    });
  }


  createAnswer() {
    this.peerConnection.createAnswer().then(sdp => {
      // send local sdp
      this.setLocalSDP(sdp);
      // send answer with local sdp
      this.socketService.emit('answer', this.peerConnection.localDescription);
      console.log('Answer Signal Sent with Local SDP');
    }).catch((error) => {
      console.log('createAnswer() Error: ' + error.message);
    });
  }

  // handle on answer event
  onAnswer(answer) {
    // this.setRemoteSDP(answer);
    const newSdp = new RTCSessionDescription(answer);
    this.peerConnection.setRemoteDescription(newSdp).then(() => {
      console.log('Remote SDP Set');
    }).catch((error) => {
      console.log('setRemoteSDP() Error: ' + error.message);
    });
  }

  // triggered if there is stream
  onAddStream(event: MediaStreamEvent) {
    if (this.student.type === 'learner') {
      console.log('Received Remote Stream');
      this.stream = event.stream;
      this.isLive = true;
      // this.renderRemoteStream(event.stream);
      const vid = document.getElementById('remoteVideo');
      vid.setAttribute('poster', 'assets/images/spinner.gif');
      vid.setAttribute('controls', 'true');
      vid.setAttribute('muted', 'false');
      vid.setAttribute('src', window.URL.createObjectURL(event.stream));
    }
  }

  addStream(stream: MediaStream) {
    if (this.stream === null && this.student.type === 'instructor') {
      this.stream = stream;
      // add the stream to this peer connection
      this.peerConnection.addStream(stream);

      console.log('Stream Added to peer Connection');
    }
    // this.peerConnection.addStream(stream);
  }

  createOffer() {
    // create offer
    this.peerConnection.createOffer().then((sdp) => {
      console.log('Offer Created');
      // set local sdp
      this.setLocalSDP(sdp);
      console.log('Offer Signal Sent');
    }).catch((error) => {
      console.log('createOffer() Error: ' + error.message);
    });
  }


  setLocalSDP(sdp) {
    this.peerConnection.setLocalDescription(sdp).then(() => {
      // send offer to signaling server
      this.socketService.emit('offer', sdp);
      console.log('Local SDP Set');
    }).catch((error) => {
      console.log('setLocalSDP() Error: ' + error.message);
    });
  }

  // set remote sdp
  setRemoteSDP(sdp) {
    const newSdp = new RTCSessionDescription(sdp);
    this.peerConnection.setRemoteDescription(newSdp).then(() => {
      console.log('Remote SDP Set');
    }).catch((error) => {
      console.log('setRemoteSDP() Error: ' + error.message);
    });
  }

  // if stream is removed
  onRemoveStream(event: MediaStreamEvent) {
    if (this.student.type === 'learner') {
      this.removeRemoteStream();
      swal({ text: 'The Instructor Has Ended the class. Check again later.', type: 'info' });
    }
  }

  removeLocalStream() {
    if (this.stream !== null) {
      const vid = document.getElementById('videoPreview');
      vid.setAttribute('poster', 'assets/images/spinner.gif');
      const stream = this.stream;
      stream.getTracks().forEach((track) => {
        track.stop();
      });
      this.stream = null;
      vid.setAttribute('controls', 'false');
      vid.setAttribute('muted', 'true');
      vid.setAttribute('src', '');
      vid.setAttribute('poster', '/assets/images/logo.png');
      this.socketService.emit('endclass', null);
    }
  }

  removeRemoteStream() {
    if (this.stream !== null) {
      const vid = document.getElementById('remoteVideo');
      vid.setAttribute('poster', 'assets/images/spinner.gif');
      this.stream = null;
      vid.setAttribute('src', '');
      vid.setAttribute('poster', '/assets/images/logo.png');
      // this.socket.emit('endclass');
    }
  }

  startBroadcast() {
    // navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia
    // || navigator.mozGetUserMedia || navigator.msGetUserMedia);
    // Get media stream from devices
    navigator.mediaDevices.getUserMedia(this.mediaOptions).then(stream => {
      // success handler
      this.isLive = true;
      this.stream = stream;
      // stream to peer connection
      this.peerConnection.addStream(stream);
      // console.log("Peer ", this.peerConnection);
      // create an offer
      this.createOffer();
      console.log('Stream Added to peer Connection');
      // render the stream
      this.renderLocalStream();
    }).catch((error) => {
      console.log('getUserMedia() Error: ', error);
    });
  }


  // render the video on the video element
  renderLocalStream() {
    const vid = document.getElementById('videoPreview');
    vid.setAttribute('poster', 'assets/images/spinner.gif');
    vid.setAttribute('controls', 'true');
    vid.setAttribute('muted', 'true');
    vid.setAttribute('src', window.URL.createObjectURL(this.stream));
    vid.setAttribute('poster', '/assets/images/logo.png');
  }

  // render the remote video on the remoteVideo element
  renderRemoteStream(stream: MediaStream) {
    const vid = document.getElementById('remoteVideo');
    vid.setAttribute('poster', 'assets/images/spinner.gif');
    vid.setAttribute('controls', 'true');
    vid.setAttribute('muted', 'false');
    vid.setAttribute('src', window.URL.createObjectURL(stream));
  }

  pauseBroadcast() {
    if (this.student.type === 'instructor' && this.stream !== null) {
      const vid = document.querySelector('video');
      vid.pause();
      this.paused = true;
      console.log('Paused Video');
      this.socketService.emit('paused', null);
    }
  }

  resumeBroadcast() {
    if (this.student.type === 'instructor') {
      const vid = document.querySelector('video');
      vid.play();
      this.paused = false;
      console.log('Resumed Video');
      this.socketService.emit('resumed', null);
    }
  }

  stopBroadcast() {
    console.log('Stream', this.stream);
    if (this.stream !== null) {
      this.peerConnection.removeStream(this.stream);
    }
    this.removeLocalStream();
    this.isLive = false;
    this.paused = false;
  }

  exitClass() {
    if (this.student.type === 'instructor' && this.stream !== null) {
      this.peerConnection.removeStream(this.stream);
      this.removeLocalStream();
      this.isLive = false;
      this.paused = false;
      this.hasJoined = false;
      this.socketService.emit('exitclass', JSON.stringify(this.student));
      this.socketService.disconnect();
    } else if (this.student.type === 'learner') {
      this.removeRemoteStream();
      this.socketService.disconnect();
      this.hasJoined = false;
    }

  }

  enableShareScreen() {

  }

}
