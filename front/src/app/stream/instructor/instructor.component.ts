import { Component, OnInit, Input, HostListener } from '@angular/core';

import swal from 'sweetalert2';
import * as DetectRTC from 'detectrtc';
import md5 from 'md5-hash';
import * as io from 'socket.io-client';

import { AuthService } from '@services/auth/auth.service';
import { BroadcastService } from '@services/broadcast/broadcast.service';
import { RecordingService } from '@services/recording/recording.service';

import { DateTimeHelper } from '@helpers/date-time-helper';

import { User } from '@models/user';
import { Lesson } from '@models/lesson';
import { Instructor } from '@models/instructor';
import { MediaSource } from '@models/media-source';
import { CanComponentDeactivate } from '@services/can-deactivate/can-deactivate.guard';
import { Observable } from 'rxjs/Observable';
import { SocketService } from '@services/socket/socket.service';

@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.css']
})
export class InstructorComponent implements OnInit, CanComponentDeactivate {
  @Input('instructor') instructor: Instructor;
  @Input('lessons') lessons: Lesson[];
  @Input('lesson') lesson: Lesson;

  user: User = null;
  hasStarted = false;
  isLive = false;
  isPaused = false;
  hasJoined = false;
  socket = null;
  numberConnected = 0;
  stream: MediaStream = null;
  dtHelper = new DateTimeHelper();
  slug = '';
  interval = null;


  sourceCameras: MediaSource[] = [];
  sourceMicrophones: MediaSource[] = [];
  sourceCamera: MediaSource = null;
  sourceMicrophone: MediaSource = null;

  isScreenShared = false;
  isDataChannelOpen = false;

  constructor(
    public broadcastService: BroadcastService,
    private recorder: RecordingService,
    private socketService: SocketService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.user = this.authService.getUser();

    this.socket = this.broadcastService.connection.connectSocket;

    this.broadcastService.setUser(this.user);

    this.broadcastService.setLesson(this.lesson);

    console.log(this.broadcastService.connection, this.socket, this.lesson);

    this.openClassroom();

     // listen for incoming streams
    this.broadcastService.connection.onstream = (event) => {
      this.stream = event.stream;
      const vid = document.querySelector('video');
      vid.srcObject = event.stream;
      vid.muted = true;
      this.isLive = true;
      // record both audio and video
      this.recorder.startRecording(event.stream);
    };

    this.broadcastService.connection.onstreamended = (event) => {
      const vid = document.querySelector('video');
      vid.srcObject = null;
    };

    this.getMediaDevices();

    this.interval = setInterval(() => {
      this.numberConnected = this.broadcastService.numberConnected();
      if (this.isDataChannelOpen) {
        this.socketService.emit('datachannelopened', { lesson: this.lesson });
      } else {
        this.socketService.emit('datachannelclosed', { lesson: this.lesson });
      }
    }, 5000);

  }

  canDeactivate() {
    if (this.isLive && this.instructor !== null) {
      // tslint:disable-next-line:max-line-length
      return window.confirm('Leaving this windows while broadcast is on will automatically stop all the media devices. All connected viewers will loose connection. Are you sure to continue?');
    }
    return true;
  }

  

  /*
    @HostListener('window:beforeunload')
    async onBeforeUnload() {
      let allow = false;
      if (this.isLive && this.instructor !== null) {
        // tslint:disable-next-line:prefer-const
        let confirm = await swal({
          title: 'Are you sure?',
          text:
            // tslint:disable-next-line:max-line-length
            'Leaving this windows while broadcast is on will automatically stop
    all the media devices. All connected viewers will loose connection. Are you
    sure to continue?', type: 'warning', showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Continue'
        });
          if (confirm.value) {
            this.exitClass();
            this.broadcastService.connection.close();
            allow = true;
          }
      }
      return allow;
    }

    */

  openClassroom() {
      // opens a classroom
      this.broadcastService.openClassroom();
      this.hasJoined = true;
      this.hasStarted = true;
  }

  /**
   * Add camera and microphone
   */
  startBroadcast() {
    // console.log(this.connection.userid);
    if (this.instructor !== null) {
      this.broadcastService.startBroadcast();
      // this.broadcastService.shareScreen();
      this.isLive = true;
      this.isPaused = false;
    }
  }


  async endBroadcast() {
    try {
      // tslint:disable-next-line:prefer-const
    let confirm = await swal({
      title: 'Are yo sure?',
      text:
        'Stopping the broadcast will disconnect all the viewers! Are you sure to continue?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    });
      if (confirm.value) {
        // stop both audio and video recording
        this.recorder.stopRecording(this.lesson.slug);
        // stop stream capture
        this.broadcastService.stopStream();

        this.isLive = false;
        this.isPaused = false;
        // this.hasStarted = false;
      }
    } catch (error) {

    }

  }

  async exitClass() {
    try {
      // tslint:disable-next-line:prefer-const
      let confirm = await swal({
        title: 'Are yo sure?',
        text:
          'Stopping the broadcast will disconnect all the viewers! Are you sure to continue?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      });
      if (confirm.value) {
          // stop both audio and video recording
          // record both audio and video
          this.recorder.stopRecording(this.lesson.slug);
          // stop stream capture
          this.broadcastService.stopStream();

          // disconnect from all connected participants
          this.broadcastService.disconnectWithAll();
          this.isLive = false;
          this.isPaused = false;
          this.hasStarted = false;
           // leave the classroom
          this.broadcastService.connection.leave();
          // close the socket
          this.broadcastService.connection.close();
        }
    } catch (error) {
      console.error(error);
    }

  }


  onSelectVideoDevice(deviceId: any) {
    console.log(deviceId);
    if (this.isLive) {
      this.broadcastService.connection.attachStreams.forEach((oldStream) => {
        oldStream.stop();
      });

      setTimeout(() => {
        this.broadcastService.connection.mediaConstraints.video.optional =
            [{deviceId: deviceId}];
        this.broadcastService.connection.addStream({ video: true, audio: true, oneway: true });
      }, 3000);  // wait for socket.io to remove old stream from all users
    } else {
      this.broadcastService.connection.mediaConstraints.video.optional =
          [{deviceId: deviceId}];
    }
  }

  onSelectAudioDevice(deviceId: string) {
    console.log(deviceId);
    if (this.isLive) {
      this.broadcastService.connection.attachStreams.forEach((oldStream) => {
        oldStream.stop();
      });

      setTimeout(() => {
        this.broadcastService.connection.mediaConstraints.audio.deviceId =
            deviceId;
        this.broadcastService.connection.addStream({ video: true, audio: true, oneway: true });
      }, 3000);  // wait for socket.io to remove old stream from all users
    } else {
      this.broadcastService.connection.mediaConstraints.audio.deviceId =
          deviceId;
    }
  }

  onToggleShareScreen(clientId?: number) {
    if (this.isScreenShared) {
      if (clientId) {
        this.broadcastService.removeScreen(clientId);
      } else {
        this.broadcastService.removeScreen();
      }
      this.isScreenShared = false;
    } else {
      if (clientId) {
        this.broadcastService.shareScreen(clientId);
      } else {
        this.broadcastService.shareScreen();
      }
      this.isScreenShared = true;
    }
  }

  onToggleDataChannel(clientId?: number) {
    if (this.isDataChannelOpen) {
      if (clientId) {
        this.broadcastService.connection.peers[clientId].addStream({
          data: false
        });
        this.socketService.emit('datachannelclosed', { lesson: this.lesson, clientId: clientId });
      } else {
        this.broadcastService.connection.addStream({
          data: false
        });
        this.socketService.emit('datachannelclosed', { lesson: this.lesson});
      }
      
      this.isDataChannelOpen = false;
    } else {
      if (clientId) {
        this.broadcastService.connection.peers[clientId].addStream({
          data: true
        });
        this.socketService.emit('datachannelopened', { lesson: this.lesson, clientId: clientId});
      } else {
        this.broadcastService.connection.addStream({
          data: true
        });
        this.socketService.emit('datachannelopened', {lesson: this.lesson});
      }
      
      this.isDataChannelOpen = true;
    }
    
  }

  removeFromClass(clientId: number) {
    if (!this.broadcastService.connection.isInitiator) {
      swal({ title: 'Access Denied', text: 'Only the instructor can remove participants!', type: 'error' });
      return;
    }
    if (!this.broadcastService.connection.peers[clientId]) {
      swal({ title: 'Access Denied', text: 'Trying to remove a non-existence user!', type: 'error' });
      return;
    }
    // eject the user
    this.broadcastService.removeParticipant(clientId);
  }

  getMediaDevices() {
    DetectRTC.load(() => {
      if (DetectRTC.isWebRTCSupported === false) {
          swal({title: 'Please use Chrome or Firefox.', type: 'error', timer: 3000});
          return;
      }

      if (DetectRTC.hasWebcam === false) {
        swal({title: 'No microphone in your system. Please install an external webcam device.', type: 'error', timer: 3000});
        return;
      }

      if (DetectRTC.hasMicrophone === false) {
        swal({title: 'No microphone in your system. Please install an external microphone device..', type: 'error', timer: 3000});
        return;
      }
      // get the cameras connected
      DetectRTC.videoInputDevices.forEach((device) => {
          if (this.sourceCameras.find((x) => {
            return x.id === device.id;
          }) === undefined) {
            const camera = new MediaSource();
            camera.id = device.id;
            camera.name = device.label;
            camera.type = device.kind;
            this.sourceCameras.push(camera);
          }
      });

       // get the microphones connected
       DetectRTC.audioInputDevices.forEach((device) => {
        if (this.sourceMicrophones.find((x) => {
          return x.id === device.id;
        }) === undefined) {
          const mic = new MediaSource();
          mic.id = device.id;
          mic.name = device.label;
          mic.type = device.kind;
          this.sourceMicrophones.push(mic);
        }
      });
    });

    // console.log(this.sourceCameras, this.sourceMicrophones);
  }

}
