import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {NgForm, NgModel} from '@angular/forms';
import {Router} from '@angular/router';

import * as DetectRTC from 'detectrtc';
import md5 from 'md5-hash';
import * as io from 'socket.io-client';
import swal from 'sweetalert2';

import {AppConstant} from '@config/app-constant';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Student} from '@models/student';
import {Lesson} from '@models/lesson';
import {MediaSource} from '@models/media-source';
import { User } from '@models/user';
import { Instructor } from '@models/instructor';
import { Enrolment } from '@models/enrolment';

import {AuthService} from '@services/auth/auth.service';
import {StudentService} from '@services/student/student.service';
import {LessonService} from '@services/lesson/lesson.service';
import {RecordingService} from '@services/recording/recording.service';
import {StreamService} from '@services/stream/stream.service';
import { BroadcastService } from '@services/broadcast/broadcast.service';
import { InstructorService } from '@services/instructor/instructor.service';

declare const RTCMultiConnection;
// declare const RecordRTC;
// declare const DetectRTC;

@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.css']
})
export class StreamComponent implements OnInit {
  loading = true;
  title = 'Classroom';

  lesson: Lesson = null;
  student: Student = null;
  instructor: Instructor = null;
  user: User = null;
  lessons: Lesson[] = [];
  enrolments: Enrolment[] = [];

  hasStarted = false;
  isLive = false;
  hasJoined = false;
  isPaused = false;

  connection = null;
  socket = null;
  // private recorder = null;
  interval = null;
  numberConnected = 0;

  sourceCameras: MediaSource[] = [];
  sourceMicrophones: MediaSource[] = [];
  sourceCamera: MediaSource = null;
  sourceMicrophone: MediaSource = null;

  allowScreenSharing = false;
  allowDataStream = true;
  allowSessionRecording = true;

  serverUrl = AppConstant.serverUrl;
  socketUrl = AppConstant.socketURL;

  videoElement: HTMLVideoElement = null;
  stream: MediaStream = null;

  // @ViewChild('localVideo') videoElement: any;
  dtHelper = new DateTimeHelper();
  slug = '';

  constructor(
    // private streamService: StreamService,
    private studentService: StudentService,
    private authService: AuthService,
    private lessonService: LessonService,
    private instructorService: InstructorService,
    private broadcastService: BroadcastService,
    private recorder: RecordingService,
    private router: Router
  ) { }

  async ngOnInit() {
    this.loading = true;

    this.user = this.authService.getUser();

   
    try {
      if (this.user.type === 'learner') {
        let student = this.studentService.getStudentById(this.user.id).toPromise();
        let enrolments = this.studentService.getEnrolments(this.user.id).toPromise();

        this.student = await student;
        this.enrolments = await enrolments;

        this.enrolments.forEach((enrolment) => {
          this.lessons.push(enrolment.lesson);
        });

        this.loading = false;

      } else if (this.user.type === 'instructor') {
        let instructor =
            this.instructorService.getInstructorById(this.user.id).toPromise();
        let lessons = this.instructorService.getInstructorLessons(this.user.id).toPromise();
        
        this.instructor = await instructor;
        this.lessons = await lessons;

        this.loading = false;
        
      } else {
        await swal({ title: 'Access Denied!', type: 'error', timer: 3000 });
        this.router.navigate(['/']);
        this.loading = false;
      }
    } catch (err) {
      await swal({ title: 'Internal Server Erroor', text: err, type: 'error', timer: 3000 });
      this.router.navigate(['/']);
      this.loading = false;
    }
    

    this.connection = this.broadcastService.connection;

    this.loading = false;
    // listen for incoming streams
    this.connection.onstream = (event) => {
      this.stream = event.stream;

      if (this.student !== null) {
        this.videoElement = <HTMLVideoElement>document.getElementById('remoteVideo');
        this.videoElement.srcObject = event.stream;
        this.videoElement.muted = false;
        this.videoElement.play();
        this.isLive = true;
      } else {
        this.videoElement = <HTMLVideoElement>document.getElementById('localVideo');
        this.videoElement.srcObject = event.stream;
        this.videoElement.muted = true;
        this.videoElement.play();
        this.isLive = true;
        if (this.allowSessionRecording) {
          // record both audio and video
          // this.recorder.startRecording(event.stream);
        }
      }
    };

    this.connection.onstreamended = (event) => {
      if (this.instructor !== null) {
       const vid = <HTMLVideoElement>document.getElementById('localVideo');
       vid.srcObject  = null;
       vid.src = '';
      } else {
        const vid = <HTMLVideoElement>document.getElementById('remoteVideo');
        vid.srcObject  = null;
        vid.src = '';
        this.isLive = false;
        // renegotiate while no media stream is added
        // this.connection.peers[this.lesson.slug].renegotiate();
        // this.joinClass();
      }

    };

    this.getMediaDevices();

    this.interval = setInterval(() => {
      this.numberConnected = this.connection.getAllParticipants().length;
      // this.connection.getAllParticipants().forEach(p => {
      //   console.log(this.connection.peers[p]);
      // });
    }, 5000);

    console.log(this.connection);
    console.log(this.recorder);
  }

  @HostListener('window:beforeunload')
  onBeforeUnload() {
    let allow = false;
    if (this.isLive && this.instructor !== null) {
      swal({
        title: 'Are you sure?',
        text:
          'Leaving this windows while broadcast is on will automatically ' +
          'stop all the media devices.All connected viewers will loose connection.Are you sure to continue?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continue'
      }).then(confirm => {
        if (confirm.value) {
          this.exitClass();
          this.connection.close();
          allow = true;
        }
      });
    }
    return allow;
  }


  getAllLessons() {
    this.lessonService.getAllLessons()
        .toPromise()
        .then((lessons) => {
          this.lessons = lessons;
        })
        .catch((err) => {
          console.error(err);
        });
  }

  onLessonChanged(event: any) {
    this.slug = event.target.value;
    console.log(this.slug);
    this.lesson = this.lessons.find((x) => {
      return x.slug === this.slug;
    });
    // console.log(this.lesson);
    this.broadcastService.setLesson(this.lesson);
  }

  /**
    * learner or viewer methods
  */

  joinClass() {
    if (this.student !== null) {
      this.broadcastService.setLesson(this.lesson);
      this.broadcastService.joinClassroom();
      this.hasJoined = true;
      console.log(this.connection.userid);
    }
  }

  exitClass() {
    if (this.instructor !== null) {
      // stop both audio and video recording
      // this.recorder.stopRecording(this.lesson.slug);
      // stop stream capture
      // this.connection.streams.stop();
      // disconnect from all connected participants
      this.connection.getAllParticipants().forEach(participant => {
        this.connection.disconnectWith(participant);
      });
      this.hasStarted = false;
      this.isPaused = false;
    }
    // set flags
    this.isLive = false;
    this.hasJoined = false;
    // leave the classroom
    this.connection.leave();
    // close the socket
    this.connection.close();
  }

  /**
    * instructor methods
  */
  openClassroom() {
    if (this.instructor !== null) {
      this.broadcastService.setLesson(this.lesson);
      // opens a classroom
      this.broadcastService.openClassroom();
      this.hasJoined = true;
      this.hasStarted = true;
      console.log(this.connection.userid);
    }
  }




  /**
   * Add camera and microphone
   */
  startBroadcast() {
    // console.log(this.connection.userid);
    if (this.instructor !== null) {
      this.broadcastService.startBroadcast();
      this.isLive = true;
      this.isPaused = false;
    }
  }

  endBroadcast() {
    swal({
      title: 'Are yo sure?',
      text:
          'Stopping the broadcast will disconnect all the viewers! Are you sure to continue?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continue'
    }).then((confirm) => {
      if (confirm.value) {
        // stop both audio and video recording
        // this.recorder.stopRecording(this.lesson.slug);
        // stop stream capture
        // this.connection.streams.stop();
        this.connection.attachStreams.forEach(function(localStream) {
          localStream.stop();
        });
        this.isLive = false;
        this.isPaused = false;
        // this.hasStarted = false;
      }
    });
  }

  onCameraChange(event: any) {

  }

  onMicrophoneChange(event: any) {
      console.log(event.target.value);
  }

  onAllowScreenSharingChange(studentId?: number) {
    if (studentId) {
      this.connection.peers[studentId].addStream({screen: true, oneway: true});
    } else {
      this.connection.addStream({
        screen: true,
        oneway: true
      });
      this.allowScreenSharing = !this.allowScreenSharing;
    }
  }

  onAllowDataStreamChange(studentId?: number) {
    if (studentId) {
      this.connection.peers[studentId].addStream({
        data: true
      });
    } else {
      this.connection.addStream({
        data: true
      });
      this.allowDataStream = !this.allowDataStream;
    }
  }

  onAllowSessionRecordingChange() {
    this.allowSessionRecording = !this.allowSessionRecording;
  }

  removeFromClass(studentId: number) {
    if (!this.connection.isInitiator) {
      swal({ title: 'Access Denied', text: 'Only the instructor can remove participants!', type: 'error' });
      return;
    }
    if (!this.connection.peers[studentId]) {
      swal({ title: 'Access Denied', text: 'Trying to remove a non-existence user!', type: 'error' });
      return;
    }
    // eject the user
    this.connection.peers[studentId].sendCustomMessage({ejected: true});
  }

  getMediaDevices() {
    DetectRTC.load(() => {
      if (DetectRTC.isWebRTCSupported === false) {
          swal({title: 'Please use Chrome or Firefox.', type: 'error', timer: 3000});
          return;
      }

      if (DetectRTC.hasWebcam === false) {
        swal({title: 'No microphone in your system. Please install an external webcam device.', type: 'error', timer: 3000});
        return;
      }

      if (DetectRTC.hasMicrophone === false) {
        swal({title: 'No microphone in your system. Please install an external microphone device..', type: 'error', timer: 3000});
        return;
      }
      // get the cameras connected
      DetectRTC.videoInputDevices.forEach((device) => {
          if (this.sourceCameras.find((x) => {
            return x.id === device.id;
          }) === undefined) {
            const camera = new MediaSource();
            camera.id = device.id;
            camera.name = device.label;
            camera.type = device.kind;
            this.sourceCameras.push(camera);
          }
      });

       // get the microphones connected
       DetectRTC.audioInputDevices.forEach((device) => {
        if (this.sourceMicrophones.find((x) => {
          return x.id === device.id;
        }) === undefined) {
          const mic = new MediaSource();
          mic.id = device.id;
          mic.name = device.label;
          mic.type = device.kind;
          this.sourceMicrophones.push(mic);
        }
      });
    });

    // console.log(this.sourceCameras, this.sourceMicrophones);
  }

}
