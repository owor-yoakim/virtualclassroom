import {CommonModule} from '@angular/common';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {AddressService} from '@services/address/address.service';
import {AdminGuard} from '@services/admin-guard/admin.guard';
import {AuthGuardService} from '@services/auth-guard/auth-guard.service';
import {AuthService} from '@services/auth/auth.service';
import {BroadcastService} from '@services/broadcast/broadcast.service';
import {CanDeactivateGuard} from '@services/can-deactivate/can-deactivate.guard';
import {ChatService} from '@services/chat/chat.service';
import {EnrolmentService} from '@services/enrolment/enrolment.service';
import {FileUploadService} from '@services/file-upload/file-upload.service';
import {InstructorService} from '@services/instructor/instructor.service';
import {LessonCommentService} from '@services/lesson-comment/lesson-comment.service';
import {LessonModuleService} from '@services/lesson-module/lesson-module.service';
import {LessonService} from '@services/lesson/lesson.service';
import {RecordingService} from '@services/recording/recording.service';
import {SettingService} from '@services/setting/setting.service';
import {SocketService} from '@services/socket/socket.service';
import {StreamService} from '@services/stream/stream.service';
import {StudentService} from '@services/student/student.service';
import {SubjectService} from '@services/subject/subject.service';
import {UserMessageService} from '@services/user-message/user-message.service';
import {UserService} from '@services/user/user.service';
import { PageService } from '@services/page/page.service';

@NgModule({
  providers: [

  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthService,
        AuthGuardService,
        AdminGuard,
        SettingService,
        LessonService,
        EnrolmentService,
        LessonCommentService,
        StudentService,
        InstructorService,
        BroadcastService,
        StreamService,
        ChatService,
        UserMessageService,
        SubjectService,
        LessonModuleService,
        AddressService,
        RecordingService,
        SocketService,
        FileUploadService,
        PageService,
        UserService,
        CanDeactivateGuard
      ]
    };
  }
}
