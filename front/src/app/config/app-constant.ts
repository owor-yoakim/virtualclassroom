export class AppConstant {
    public static serverUrl = 'http://localhost:3300';
    public static adminBaseUrl = 'http://localhost:3300/admin/api';
    public static pageSizeOptions = [5, 10, 15, 20, 25, 50, 100];
    public static pageSize = 5;
    // public static socketURL = 'http://localhost:9001/';
    public static socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
    public static iceServers = [
        { urls: ['stun:stun.l.google.com:19302', 'stun:stun1.l.google.com:19302'] },
        {
            urls: 'turn:numb.viagenie.ca',
            credential: 'muazkh',
            username: 'webrtc@live.com'
        }
    ];

    public static session = {
        audio: true, // by default, it is true
        video: true, // by default, it is true
        screen: false,
        data: true, // if you want text chat
        oneway: true,
        broadcast: true
    };

    public static bandwidth = {
        audio: 128,
        video: 1024
    };

    public static videoConstraints = {
        mandatory: {
            maxWidth: screen.width > 1920 ? screen.width : 1920,
            maxHeight: screen.height > 1080 ? screen.height : 1080,
            minWidth: 1280,
            minHeight: 720,
            // minHeight: 480,
            minAspectRatio: 1.77
            // minFrameRate: 3,
           // maxFrameRate: 64
        },
        optional: []
    };

    public static recorderOptions = {
        video: {
            width: 1280,
            height: 720
        },
      // mimeType: 'video/webm',
      // mimeType: 'video/mp4',
      // mimeType: 'video/webm\;codecs=vp9',
       mimeType: 'video/webm\;codecs=h264',
      // mimeType: 'video/mp4\;codecs=h264'
      // mimeType: 'video/mp4; codecs="mpeg4, aac"'
    };

    public static maxParticipantsAllowed = 1000;

}
