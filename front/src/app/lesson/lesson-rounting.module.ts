import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LessonModuleViewComponent} from '@lesson/lesson-module-view/lesson-module-view.component';
import {LessonViewComponent} from '@lesson/lesson-view/lesson-view.component';
import {LessonsContainerComponent} from '@lesson/lessons-container/lessons-container.component';
import {LessonsComponent} from '@lesson/lessons/lessons.component';
import {StreamComponent} from '@stream/stream/stream.component';

const lessonRoutes: Routes = [{
  path: 'lessons',
  children: [{
    path: '',
    component: LessonsContainerComponent,
    children: [
        { path: '', component: LessonsComponent },
        {
        path: ':lessonSlug/module/:moduleId',
        component: LessonModuleViewComponent
      },
        {
            path: 'show/:lessonSlug',
            component: LessonViewComponent
        },

        {
            path: ':subjectSlug/all',
            component: LessonsComponent
        }
    ]
  }]
}];

@NgModule(
    {imports: [RouterModule.forChild(lessonRoutes)], exports: [RouterModule]})
export class LessonRoutingModule {}
