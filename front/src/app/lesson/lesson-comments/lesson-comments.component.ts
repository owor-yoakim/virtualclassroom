import {Component, Input, OnInit} from '@angular/core';
import {Lesson} from '@models/lesson';
import {LessonComment} from '@models/lesson-comment';
import {LessonCommentService} from '@services/lesson-comment/lesson-comment.service';

@Component({
  selector: 'app-lesson-comments',
  templateUrl: './lesson-comments.component.html',
  styleUrls: ['./lesson-comments.component.css']
})
export class LessonCommentsComponent implements OnInit {
  @Input('lesson') lesson: Lesson;
  comments: LessonComment[] = [];

  constructor(private commentsService: LessonCommentService) {}

  ngOnInit() {}
}
