import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Lesson} from '@models/lesson';
import {Subject} from '@models/subject';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonService} from '@services/lesson/lesson.service';
import {SubjectService} from '@services/subject/subject.service';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.css']
})
export class LessonsComponent implements OnInit {
  title = '"lang_18"';
  user: User = null;
  subject: Subject = null;
  lessons: Lesson[] = [];
  subjectLessons: any[] = [];
  subjects: Subject[] = [];
  loading = false;
  slug: string = null;

  constructor(
    public authService: AuthService,
    private subjectService: SubjectService,
    private lessonService: LessonService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.loading = true;
    this.user = this.authService.getUser();

    this.route.paramMap.subscribe(async (params) => {
      this.slug = params.get('subjectSlug') || null;

      try {
        this.subjects = await this.subjectService.getAllSubjects().toPromise();

        if (this.slug !== null) {
          this.subject = this.subjects.find((subject) => {
            return subject.slug === this.slug;
          });

          this.lessons =
              await this.lessonService.getLessonsBySubject(this.subject.id)
                  .toPromise();

        } else {
          this.lessons = await this.lessonService.getAllLessons().toPromise();
        }

        this.subjectLessons = this.lessonService.arrangeLessons(this.lessons);
        this.loading = false;

      } catch (err) {
        console.error(err);
        this.loading = false;
      }
    });

  }
}
