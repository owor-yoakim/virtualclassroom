import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LessonsComponent } from '@lesson/lessons/lessons.component';
import { LessonViewComponent } from '@lesson/lesson-view/lesson-view.component';
import { LessonLevelsComponent } from '@lesson/lesson-levels/lesson-levels.component';
import { LessonCommentsComponent } from '@lesson/lesson-comments/lesson-comments.component';
import { SharedModule } from '@shared/shared.module';
import { LessonRoutingModule } from '@lesson/lesson-rounting.module';
import { TranslateModule } from '@translate/translate.module';
import { LessonModuleViewComponent } from '@lesson/lesson-module-view/lesson-module-view.component';
import { LessonsContainerComponent } from '@lesson/lessons-container/lessons-container.component';


@NgModule({
  imports: [
    SharedModule,
    LessonRoutingModule
  ],
  declarations: [
    LessonsContainerComponent,
    LessonsComponent,
    LessonViewComponent,
    LessonLevelsComponent,
    LessonCommentsComponent,
    LessonModuleViewComponent
  ],
  exports: [ ]
})
export class LessonModule { }
