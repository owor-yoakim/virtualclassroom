import { Component, Input } from '@angular/core';
import { Lesson } from '@models/lesson';

@Component({
  selector: 'app-lesson-item',
  templateUrl: './lesson-item.component.html',
  styleUrls: ['./lesson-item.component.css']
})
export class LessonItemComponent {
  @Input('lesson') lesson: Lesson;

}
