import { Component, Input} from '@angular/core';
import { Lesson } from '@models/lesson';

@Component({
  selector: 'app-lessons-grid',
  templateUrl: './lessons-grid.component.html',
  styleUrls: ['./lessons-grid.component.css']
})
export class LessonsGridComponent {
  @Input('lessons') lessons: Lesson[] = [];

}
