import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonLevelsComponent } from './lesson-levels.component';

describe('LessonLevelsComponent', () => {
  let component: LessonLevelsComponent;
  let fixture: ComponentFixture<LessonLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonLevelsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
