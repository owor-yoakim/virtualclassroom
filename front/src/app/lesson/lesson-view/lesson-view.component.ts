import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DateTimeHelper} from '@helpers/date-time-helper';
import {Enrolment} from '@models/enrolment';
import {Instructor} from '@models/instructor';
import {Lesson} from '@models/lesson';
import {LessonModule} from '@models/lesson-module';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import { StudentService } from '@services/student/student.service';
import {LessonService} from '@services/lesson/lesson.service';
import {FlashMessagesService} from 'ngx-flash-messages';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-lesson-view',
  templateUrl: './lesson-view.component.html',
  styleUrls: ['./lesson-view.component.css']
})
export class LessonViewComponent implements OnInit {
  lesson: Lesson = null;
  user: User = null;
  lessonInstructors: Instructor[] = [];
  lessonSchedules: LessonModule[] = [];
  lessonPrerequisites: Lesson[] = [];
  enrolments: Enrolment[] = [];
  lessonModules$: Observable<LessonModule[]>;
  loading = false;
  slug: string = null;
  dt = new DateTimeHelper();
  isEditOutcome = false;
  isEditOutline = false;
  isEditDescription = false;




  constructor(
    public authService: AuthService,
    public studentService: StudentService,
    private lessonService: LessonService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.loading = true;

    this.user = this.authService.getUser();
    
 
    this.route.paramMap.subscribe(async (params) => {
      this.slug = params.get('lessonSlug') || null;

      if (this.slug !== null) {
        try {
          this.lesson =
            await this.lessonService.getLessonBySlug(this.slug).toPromise();
          // console.log(this.lesson);
          if (this.lesson === undefined || this.lesson === null) {
            console.error('Unknown Lesson', this.slug);
            await swal({
              title: 'Error',
              text: 'Unknown Lesson: ' + this.slug,
              type: 'error',
              timer: 3000
            });
            this.router.navigateByUrl('/lessons');
            return;
          }

          this.lessonModules$ = this.lessonService.getLessonModules(this.lesson.id);

          if (this.authService.isStudentAuthenticated()) {
            this.enrolments =
              await this.studentService.getEnrolments(this.user.id)
                    .toPromise();
          }
       
          let prerequisites =
              this.lessonService.getLessonPrerequisites(this.lesson.id)
                  .toPromise();
          let schedules =
              this.lessonService.getLessonModules(this.lesson.id).toPromise();
          let instructors =
              this.lessonService.getLessonInstructors(this.lesson.id)
                  .toPromise();

          this.lessonPrerequisites = await prerequisites;
          this.lessonSchedules = await schedules;
          this.lessonInstructors = await instructors;

          this.loading = false;

        } catch (err) {
          console.error(err);
          await swal({
            title: 'Error',
            text: 'Unknown Lesson: ' + this.slug,
            type: 'error',
            timer: 3000
          });
          this.router.navigateByUrl('/lessons');
          return;
        }

      } else {
        await swal({title: 'Bad Request ', type: 'error', timer: 3000});
        this.router.navigateByUrl('/lessons');
        return;
      }
    });

    
    
  }

  viewLessonModule(modeule: LessonModule) {
    this.router.navigateByUrl(
        '/lessons/' + this.lesson.slug + '/module/' + modeule.id);
   }

  async onEnrol() {
    console.log(this.lesson);
    if (this.user === null) {
      // this.router.navigate['/login'];
      this.flashMessage.show(
        'Error! You must be logged in to complete this request', {
          classes: ['alert', 'alert-danger', 'alert-dismissible'],
          timeout: 5000
        });
      return;
    }

    const enrolment = new Enrolment();
    enrolment.studentId = this.user.id;
    enrolment.lessonId = this.lesson.id;
    enrolment.enrolmentDate = (new Date()).toISOString();
    enrolment.active = true;
    enrolment.createdAt = (new Date()).toISOString();
    enrolment.updatedAt = (new Date()).toISOString();
    try {
      let response = await this.studentService.enrollForLesson(enrolment).toPromise();
      console.log(response);
      await this.studentService.refreshEnrolments();
      this.flashMessage.show(
        'Successfully Enrolled for the lesson ' + this.lesson.title, {
          classes: ['alert', 'alert-success', 'alert-dismissible'],
          timeout: 5000
        });
    } catch (err) {
      console.error(err);
      this.flashMessage.show(
        err.error, { classes: ['alert', 'alert-danger', 'alert-dismissible'] });
    }
  }

  onEditOutcome() {
    this.isEditOutcome = true;
  }
  onEditOutline() {
    this.isEditOutline = true;
  }
  onEditDescription() {
    this.isEditDescription = true;
  }

  async updateLesson() {
    try {
      let response = await this.lessonService.updateInstructorLesson(this.lesson).toPromise();
      console.log(response);
      this.lesson =
          await this.lessonService.getLessonBySlug(this.slug).toPromise();
      await swal({
        title: 'Respose Status',
        text: 'Lesson Updated Sucessfully',
        type: 'success',
        timer: 3000
      });
      this.isEditOutcome = false;
      this.isEditOutline = false;
      this.isEditDescription = false;
    } catch (error) {
      console.error(error);
    }
  }

}


