import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LessonFile} from '@models/lesson-file';
import {LessonModule} from '@models/lesson-module';
import {LessonVideo} from '@models/lesson-video';
import {User} from '@models/user';
import {AuthService} from '@services/auth/auth.service';
import {LessonModuleService} from '@services/lesson-module/lesson-module.service';
import swal from 'sweetalert2';
import { Lesson } from '@models/lesson';
import { LessonService } from '@services/lesson/lesson.service';

@Component({
  selector: 'app-lesson-module-view',
  templateUrl: './lesson-module-view.component.html',
  styleUrls: ['./lesson-module-view.component.css']
})
export class LessonModuleViewComponent implements OnInit {
  lesson: Lesson = null;
  lessonModule: LessonModule = null;
  lessonModuleVideos: LessonVideo[] = [];
  lessonModuleFiles: LessonFile[] = [];
  user: User = null;
  lessonSlug: string = null;
  lessonModuleId = 0;

  constructor(
    public authService: AuthService,
    private lessonService: LessonService,
    private lessonModuleService: LessonModuleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.authService.isAuthenticated()) {
      this.user = this.authService.getUser();
      this.route.paramMap.subscribe(async (params) => {
        this.lessonSlug = params.get('lessonSlug') || null;
        this.lessonModuleId = Number(params.get('moduleId')) || 0;
        if (this.lessonSlug !== null && this.lessonModuleId > 0) {
          try {
            this.lesson = await this.lessonService.getLessonBySlug(this.lessonSlug).toPromise();
            this.lessonModule = await this.lessonModuleService
              .getLessonModuleById(this.lesson.id, this.lessonModuleId)
              .toPromise();
            this.lessonModuleVideos = this.lessonModule.lessonModuleVideos || [];
            this.lessonModuleFiles = this.lessonModule.lessonModuleFiles || [];
          
          } catch (error) {
            console.error(error);
            await swal({ title: 'Error', text: error.error, type: 'error' });
            this.router.navigateByUrl('/lessons/show/' + this.lessonSlug);
          }
        } else {
          await swal({ title: 'Error', text: 'Unknown Lesson', type: 'error' });
          this.router.navigateByUrl('/lessons');
        }
      });
    } else {
      this.router.navigateByUrl('/login');
    }  
  }
}
