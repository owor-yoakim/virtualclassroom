import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonModuleViewComponent } from './lesson-module-view.component';

describe('LessonModuleViewComponent', () => {
  let component: LessonModuleViewComponent;
  let fixture: ComponentFixture<LessonModuleViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonModuleViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonModuleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
