const express = require('express');
const http = require('http');
const path = require('path');
//const favicon = require('serve-favicon');
const logger = require('morgan');
const bodyParser = require('body-parser');
const expressJwt = require('express-jwt');
const cors = require('cors');

//environment variables file
const env = require('node-env-file');


env('./.env', {
  verbose: false,
  overwrite: true,
  raise: false,
  logger: console
});

//configurations
const config = require('./config/config');
const db = require('./config/database').conn;

// create the express app
const app = new express();

//create the server
const server = http.createServer(app);

// singaling services
require('./config/streamer')(server);

//api routes
const siteRoutes = require('./routes/site');
const adminRoutes = require('./routes/admin');
// const index = require('./routes/index');



app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));


//api routes
app.use('/site/api', siteRoutes);
app.use('/admin/api', adminRoutes);
app.use(express.static(path.join(__dirname, 'public')));
app.use('*', function (req, res, next) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

// error handler
app.use(function (err, req, res, next) {
  console.error(err);
});

const port = normalizePort(config.port);
app.set('port', port);

/**
 * Synchronize the database then start the app
 * Listen on provided port, on all network interfaces.
 */
db.sync().then(() => {
  server.listen(port);
  server.on('listening', onListening);
}).catch((err) => {
  console.error(err);
});


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }
  if (port > 0) {
    // port number
    return port;
  }
  return false;
}


function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ?
    'pipe ' + addr :
    'port ' + addr.port;
  console.log('Listening on ' + bind);
}