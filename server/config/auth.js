module.exports = function () {
    var passport = require('passport');
    var LocalStrategy = require('passport-local').Strategy;
    //var User = require('../models/User');

    var settings = {
        usernameField: 'emailAdress',
        passwordField: 'passWord',
        passReqToCallback: true
    }

    passport.use('local', new LocalStrategy(settings,
        function (req, email, password, next) {
            //console.log("Email: " + email);
            //Lookup user with this email
            User.findOne({ where: { emailAddress: email } }, function (err, user) {
                //Some database error occurred
                if (err) {
                    req.session.error = err.message;
                    return next(err);
                }

                //Check for correct email and password
                if (!user || !user.validatePassword(password)) {
                    //Invalid email or password  
                    req.session.error = 'Invalid email or password!';
                    return next(null, null);
                }
                //All is well
                return next(null, user);
            });
        }
    ));


    passport.serializeUser(function (user, next) {
        next(null, user);
    });

    passport.deserializeUser(function (email, next) {
        User.find({ 'emailAddress': email }, function (err, user) {
            next(err, user);
        })
    })
};

