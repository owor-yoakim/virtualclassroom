const Sequelize = require('sequelize');
const conn = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  dialect: process.env.DB_DIALECT,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  pool: {
    max: process.env.DB_MAX_POOL,
    min: process.env.DB_MIN_POOL,
    idle: process.env.DB_IDLE_POOL
  }
});

exports.conn = conn;
exports.Sequelize = Sequelize;