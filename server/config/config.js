module.exports = {
  baseUrl: process.env.BASE_URL,
  port: process.env.PORT || 3333,
  mongoDbConnectionString: 'mongodb://owor:mlab3271985@ds121686.mlab.com:21686/virttutor\'',
  apiUrl: process.env.BASE_URL + ':' + process.env.PORT,
  studentSecret: 'student@virttutor.com',
  instructorSecret: 'instructor@virttutor.com',
  adminSecret: 'admin@virttutor.com',
  sessionDurationInSconds: 86400,
  maximumFileSize: 2097152, // 2mbs max
  allowedImageFormats: ['jpg', 'jpeg', 'png', 'gif'],
  allowedFileFormats: ['jpg', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx']

};