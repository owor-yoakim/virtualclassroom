module.exports = function (server) {
  var socket = require("socket.io");
  var io = socket.listen(server);
  //hashmap to store clients
  var socketClients = {};
  var numberClients = 0;
  

  io.sockets.on('connection', (socket) => {

    socket.on('clientInfo', (data) => {
      let client = JSON.parse(data);
      console.log('New connection', client);
      let keys = Object.keys(socketClients);
      if (client && !keys.includes(client.id.toString())) {
        var newClient = {
          client: client,
          socket: socket
        };
        socketClients[client.id.toString()] = newClient;
        numberClients = numberClients + 1;
        socket.emit('newConnection', Object.keys(socketClients).length);
        console.log('New connection', client.id);
      }
    });

    socket.on('offer', (message) => {
      //console.log('offer', message);
      socket.broadcast.emit('offer', message);
    });

    socket.on('answer', (message) => {
      //console.log('answer', message);
      socket.broadcast.emit('answer', message);
    });

    
    socket.on('candidate', (candidate) => {
      //console.log('candidate', candidate);
      socket.broadcast.emit('candidate', candidate);
    });

    socket.on('paused', () => {
      socket.broadcast.emit('paused');
      console.log("Paused Video");
    });

    socket.on('resumed', () => {
      socket.broadcast.emit('resumed');
      console.log("Resumed Video");
    });

    socket.on('endclass', () => {
      socket.broadcast.emit('classended');
      console.log("Broadcast ended");
    });

    socket.on('message', (message) => {
      console.log("New message", message);
      socket.broadcast.emit('message', message);
    });

    socket.on('datachannelopened', (data) => {
      console.log("Data channel opened", data);
      socket.broadcast.emit('datachannelopened', data);
    });

    socket.on('datachannelclosed', (data) => {
      console.log("Data channel closed", data);
      socket.broadcast.emit('datachannelclosed', data);
    });

    socket.on('exitclass', (data) => {
      let client = JSON.parse(data);
      let keys = Object.keys(socketClients);
      if (client && keys.length > 0 && keys.includes(client.id.toString())) {
        delete socketClients[client.id.toString()];
        numberClients = numberClients - 1;
        socket.broadcast.emit('disconnected', Object.keys(socketClients).length);
        socket.disconnect(true);
        console.log("Client Disconnected ", client.id);
      }
    });

    socket.on('draw', (data) => {
      socket.broadcast.emit('draw', data);
      console.log('Drawing: ', data);
    });

    socket.on('changestrokecolor', (data) => {
      socket.broadcast.emit('changestrokecolor', data);
      console.log('Stroke Color: ', data);
    });

    socket.on('cleardrawing', (data) => {
      socket.broadcast.emit('cleardrawing', data);
      console.log('Clear Drawing: ', data);
    });

    socket.on('starterasing', (data) => {
      socket.broadcast.emit('starterasing', data);
      console.log('Start Erasing: ', data);
    });

    socket.on('stoperasing', (data) => {
      socket.broadcast.emit('stoperasing', data);
      console.log('Stop Erasing: ', data);
    });

    socket.on('changelinewidth', (data) => {
      socket.broadcast.emit('changelinewidth', data);
      console.log('Line Width: ', data);
    });

  
    socket.on('screenshot', (data) => {
      socket.broadcast.emit('screenshot', data);
    });
    console.log(socketClients);
  });
};