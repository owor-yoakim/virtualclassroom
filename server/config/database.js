// Database Connection Object
const conn = require('./db').conn;
const Sequelize = require('./db').Sequelize;
//Models
const User = require('../models/User');
const UserMessage = require('../models/UserMessage');
const UserLogin = require('../models/UserLogin');
const Role = require('../models/Role');
const Permission = require('../models/Permission');
const RolePermission = require('../models/RolePermission');
const Instructor = require('../models/Instructor');
const Subject = require('../models/Subject');
const Lesson = require('../models/Lesson');
const LessonPrerequisite = require('../models/LessonPrerequisite');
const LessonModule = require('../models/LessonModule');
const LessonFile = require('../models/LessonFile');
const LessonComment = require('../models/LessonComment');
const LessonVideo = require('../models/LessonVideo');
const InstructorLesson = require('../models/InstructorLesson');
const Page = require('../models/Page');
const Student = require('../models/Student');
const Chat = require('../models/Chat');
const Setting = require('../models/Setting');
const Address = require('../models/Address');
const Country = require('../models/Country');
const LoginAttempt = require('../models/LoginAttempt');
const Enrolment = require('../models/Enrolment');

/**
 *
 * Model Relationships
 *
 */


/* Role-to-User => 1:N */
User.belongsTo(Role, {
  foreignKey: 'roleId'
});
Role.hasMany(User, {
  foreignKey: 'roleId'
});

/* Role-to-Permission => N:M */
Permission.belongsToMany(Role, {
  through: {
    model: RolePermission,
    unique: true
  }
}, {
  foreignKey: 'permissionId'
});
Role.belongsToMany(Permission, {
  through: {
    model: RolePermission,
    unique: true
  }
}, {
  foreignKey: 'roleId'
});

/*   User-to-UserLogin => 1:1       */
User.hasOne(UserLogin, {
  foreignKey: 'userId'
});
UserLogin.belongsTo(User, {
  foreignKey: 'userId'
});

/* User-to-LoginAttempt => 1:N */
LoginAttempt.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(LoginAttempt, {
  foreignKey: 'userId'
});


/* User-to-Subject => 1:N */
Subject.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(Subject, {
  foreignKey: 'userId'
});

/* User-to-Lesson => 1:N */
Lesson.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(Lesson, {
  foreignKey: 'userId'
});

/* User-to-Student => 1:N */
Student.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(Student, {
  foreignKey: 'userId'
});

/* User-to-Instructor => 1:N */
Instructor.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(Instructor, {
  foreignKey: 'userId'
});

/* User-to-LessonModule => 1:N */
LessonModule.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(LessonModule, {
  foreignKey: 'userId'
});

/* User-to-Page => 1:N */
Page.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(Page, {
  foreignKey: 'userId'
});

/* Subject-to-Lesson => 1:N */
Lesson.belongsTo(Subject, {
  foreignKey: 'subjectId'
});
Subject.hasMany(Lesson, {
  foreignKey: 'subjectId'
});
/* Lesson-to-Chat => 1:N */
Chat.belongsTo(Lesson, {
  foreignKey: 'lessonId'
});
Lesson.hasMany(Chat, {
  foreignKey: 'lessonId'
});

/* Lesson-to-LessonPrerequisite => 1:N */
Lesson.belongsToMany(Lesson, {
  as: "prerequisites",
  through: LessonPrerequisite,
  foreignKey: "lessonId",
  otherKey: "prerequisiteId"
});
/* Lesson-to-LessonModule => 1:N */
LessonModule.belongsTo(Lesson, {
  foreignKey: 'lessonId'
});
Lesson.hasMany(LessonModule, {
  foreignKey: 'lessonId',
  as: 'lessonModules'
});
/* Lesson-to-LessonComment => 1:N */
LessonComment.belongsTo(Lesson, {
  foreignKey: 'lessonId'
});
Lesson.hasMany(LessonComment, {
  foreignKey: 'lessonId',
  as: 'lessonComments'
});
/* Lesson-to-LessonFiles => 1:N */
LessonFile.belongsTo(Lesson, {
  foreignKey: 'lessonId'
});
Lesson.hasMany(LessonFile, {
  foreignKey: 'lessonId',
  as: 'lessonFiles'
});

/* Instructor-to-LessonFile => 1:N */
LessonFile.belongsTo(Instructor, {
  foreignKey: 'instructorId'
});
Instructor.hasMany(LessonFile, {
  foreignKey: 'instructorId',
  as: 'instructorFiles'
});

/* LessonModule-to-LessonFile => 1:N */
LessonFile.belongsTo(LessonModule, {
  foreignKey: 'lessonModuleId'
});
LessonModule.hasMany(LessonFile, {
  foreignKey: 'lessonModuleId',
  as: 'lessonModuleFiles'
});

/* LessonModule-to-LessonVideo => 1:N */
LessonVideo.belongsTo(LessonModule, {
  foreignKey: 'lessonModuleId'
});
LessonModule.hasMany(LessonVideo, {
  foreignKey: 'lessonModuleId',
  as: 'lessonModuleVideos'
});

/* LessonModule-to-LessonComment => 1:N */
LessonComment.belongsTo(LessonModule, {
  foreignKey: 'lessonModuleId'
});
LessonModule.hasMany(LessonComment, {
  foreignKey: 'lessonModuleId',
  as: 'lessonModuleComments'
});

/* Student-to-LessonFile => 1:N */
LessonFile.belongsTo(Student, {
  foreignKey: 'studentId'
});
Student.hasMany(LessonFile, {
  foreignKey: 'studentId',
  as: 'studentFiles'
});
/* Instructor-to-Chat => 1:N */
Chat.belongsTo(Instructor, {
  foreignKey: 'instructorId'
});
Instructor.hasMany(Chat, {
  foreignKey: 'instructorId'
});

/* Lesson-to-Enrolment => 1:N */
Enrolment.belongsTo(Lesson, {
  foreignKey: 'lessonId'
});
Lesson.hasMany(Enrolment, {
  foreignKey: 'lessonId'
});

/* Student-to-Enrolment => 1:N */
Enrolment.belongsTo(Student, {
  foreignKey: 'studentId'
});
Student.hasMany(Enrolment, {
  foreignKey: 'studentId'
});

/* Instructor-to-Lesson => N:M */
Lesson.belongsToMany(Instructor, {
  through: {
    model: InstructorLesson,
    unique: true
  }
}, {
  foreignKey: 'lessonId'
});
Instructor.belongsToMany(Lesson, {
  through: {
    model: InstructorLesson,
    unique: true
  }
}, {
  foreignKey: 'instructorId'
});

/*  Student-to-Chat => 1:N  */
Chat.belongsTo(Student, {
  foreignKey: 'studentId'
});
Student.hasMany(Chat, {
  foreignKey: 'studentId'
});

/* Address-to-User => 1:N */
User.belongsTo(Address, {
  foreignKey: 'addressId'
});
Address.hasMany(User, {
  foreignKey: 'addressId'
});

/* Address-to-Student => 1:N */
Student.belongsTo(Address, {
  foreignKey: 'addressId'
});
Address.hasMany(Student, {
  foreignKey: 'addressId'
});

/* Country-to-Address => 1:N */
Address.belongsTo(Country, {
  foreignKey: 'countryId'
});
Country.hasMany(Address, {
  foreignKey: 'countryId'
});

/* Address-to-Instructor => 1:N */
Instructor.belongsTo(Address, {
  foreignKey: 'addressId'
});
Address.hasMany(Instructor, {
  foreignKey: 'addressId'
});

/* User-to-UserMessage => 1:N */
UserMessage.belongsTo(User, {
  foreignKey: 'userId'
});
User.hasMany(UserMessage, {
  foreignKey: 'userId'
});


module.exports = {
  conn: conn,
  Sequelize: Sequelize,
  User: User,
  UserLogin: UserLogin,
  UserMessage: UserMessage,
  Role: Role,
  Permission: Permission,
  RolePermission: RolePermission,
  Instructor: Instructor,
  Subject: Subject,
  Lesson: Lesson,
  LessonModule: LessonModule,
  LessonPrerequisite: LessonPrerequisite,
  LessonComment: LessonComment,
  LessonFile: LessonFile,
  LessonVideo: LessonVideo,
  InstructorLesson: InstructorLesson,
  Student: Student,
  Address: Address,
  Enrolment: Enrolment,
  Chat: Chat,
  LoginAttempt: LoginAttempt,
  Setting: Setting,
  Country: Country
};