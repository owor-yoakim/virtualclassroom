const _ = require('lodash');
const jwt = require('jsonwebtoken');
const util = require('util');
const config = require('./config');
const signJwt = util.promisify(jwt.sign);
const verifyJwt = util.promisify(jwt.verify);

function createSessionToken(payload, secret, expiresIn) {
  return signJwt({
    payload
  }, secret, {
    algorithm: 'HS256',
    expiresIn: expiresIn
  });
}

function verifySessionToken(token, secret) {
  return verifyJwt(token, secret);
}

function removeFields(data, fields) {
  return (_.omit(data, fields));
}

function removeFieldsFromDataItems(dataItems, fields) {
  return _.map(dataItems, (element, index, list) => {
    return removeFields(element.toJSON(), fields);
  });
}

function restrictStudent(req, res, next) {
  //console.log("Headers: ", req.headers);
  let token = req.headers.authorization;
  let msg = "Unauthorized Access! Please login to continue";
  if (typeof token !== 'undefined') {
    //console.log("Request Token: ", token);
    verifySessionToken(token, config.studentSecret).then((data) => {
      return next();
    }).catch((err) => {
      console.error("Bad Request: ", err);
      res.status(401).send(msg);
    });
  } else {
    console.error("Bad Request: ", msg);
    res.status(401).send(msg);
  }
}

function restrictInstructor(req, res, next) {
  //console.log("Headers: ", req.headers);
  let token = req.headers.authorization;
  let msg = "Unauthorized Access! Please login to continue";
  if (typeof token !== 'undefined') {
    //console.log("Request Token: ", token);
    verifySessionToken(token, config.instructorSecret).then((data) => {
      return next();
    }).catch((err) => {
      console.error("Bad Request: ", err);
      res.status(401).send(msg);
    });
  } else {
    console.error("Bad Request: ", msg);
    res.status(401).send(msg);
  }
}

function restrictAdmin(req, res, next) {
  let token = req.headers.authorization;
  let msg = "Unauthorized Access Denied!";
  if (typeof token !== 'undefined') {
    //console.log("Request Token: ", token);
    verifySessionToken(token, config.adminSecret).then((data) => {
      return next();
    }).catch((err) => {
      console.error("Bad Request: ", err);
      res.status(401).send(msg);
    });
  } else {
    console.error("Bad Request: ", msg);
    res.status(401).send(msg);
  }
}


module.exports.createSessionToken = createSessionToken;
module.exports.verifySessionToken = verifySessionToken;
module.exports.restrictAdmin = restrictAdmin;
module.exports.restrictInstructor = restrictInstructor;
module.exports.restrictStudent = restrictStudent;
module.exports.removeFields = removeFields;
module.exports.removeFieldsFromDataItems = removeFieldsFromDataItems;