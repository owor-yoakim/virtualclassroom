'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const LessonPrerequisite = conn.define(
    'lesson_prerequisite', {
      lessonId: {type: Sequelize.INTEGER, allowNull: false, primaryKey: true},
      prerequisiteId: {type: Sequelize.INTEGER, allowNull: false, primaryKey: true}
    },
    {underscored: false, timestamps: false, paranoid: false});

module.exports = LessonPrerequisite;