'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const UserMessage = conn.define(
    'user_message', {
      id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
      senderId: {type: Sequelize.INTEGER, allowNull: false},
      receiverId: {type: Sequelize.INTEGER, allowNull: false},
      senderEmail: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {isEmail: {msg: 'Invalid Sender Email Address'}}
      },

      receiverEmail: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {isEmail: {msg: 'Invalid Receiver Email Address'}}
      },
      subject: {type: Sequelize.STRING, allowNull: false},
      message: {type: Sequelize.TEXT, allowNull: false},
      read: {type: Sequelize.BOOLEAN, defaultValue: false},
      userId: {type: Sequelize.INTEGER, allowNull: true}
    },
    {
      underscored: false,
      timestamps: true,
      paranoid: true
    });

module.exports = UserMessage;
