'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Subject = conn.define(
  'subject', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    slug: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = Subject;