'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Country = conn.define(
  'country', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: Sequelize.STRING(6),
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    dialingCode: {
      type: Sequelize.INTEGER(6),
      allowNull: true
    },
    currencySymbol: {
      type: Sequelize.STRING(4),
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = Country;