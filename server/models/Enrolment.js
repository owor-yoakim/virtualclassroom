'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Enrolment = conn.define('enrolment', {
  studentId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false
  },
  lessonId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false
  },
  active: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
  enrolmentDate: {
    type: Sequelize.DATE,
    defaultValue: Sequelize.NOW
  }
}, {
  underscored: false,
  timestamps: true,
  paranoid: true
});

module.exports = Enrolment;