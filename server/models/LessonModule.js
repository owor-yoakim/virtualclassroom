'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const LessonModule = conn.define(
  'lesson_module', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    // the date of the lesson
    scheduleDate: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    // the time of the lesson
    startTime: {
      type: Sequelize.TIME,
      allowNull: false
    },
    // duration of the lesson in minutes
    duration: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    lessonId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    status: {
      type: Sequelize.ENUM('upcoming', 'ongoing', 'completed', 'cancelled'),
      defaultValue: 'upcoming'
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = LessonModule;