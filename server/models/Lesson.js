'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Lesson = conn.define(
  'lesson', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    slug: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    outline: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    outcome: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    avatar: {
      type: Sequelize.STRING,
      allowNull: true
    },
    subjectId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    startsOn: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    endsOn: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    status: {
      type: Sequelize.ENUM('upcoming', 'ongoing', 'completed'),
      defaultValue: 'upcoming'
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = Lesson;