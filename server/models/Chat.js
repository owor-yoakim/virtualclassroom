'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Chat = conn.define(
  'chat', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lessonId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    studentId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    instructorId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    message: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    fileName: {
      type: Sequelize.STRING,
      allowNull: true
    },
    fileUrl: {
      type: Sequelize.STRING,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = Chat;