'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Instructor = conn.define(
  'instructor', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: Sequelize.STRING,
      allowNull: true
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'First name must be atleast 3 characters in length'
        }
      }
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'Last name must be atleast 3 characters in length'
        }
      }
    },
    emailAddress: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'Invalid Email Address'
        }
      }
    },
    phoneNumber: {
      type: Sequelize.STRING,
      allowNull: true
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 6,
          msg: 'Password length should be atleast 6 characters long.'
        }
      }
    },
    qualification: {
      type: Sequelize.STRING,
      allowNull: true
    },
    specialization: {
      type: Sequelize.STRING,
      allowNull: true
    },
    experience: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    avatar: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: 'avatar.png'
    },
    joinDate: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    institution: {
      type: Sequelize.STRING,
      allowNull: true
    },
    profile: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    addressId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = Instructor;