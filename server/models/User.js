'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const User = conn.define(
  'user', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    roleId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'First name must be atleast 3 characters in length'
        }
      }
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'Last name must be atleast 3 characters in length'
        }
      }
    },
    emailAddress: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'Invalid Email Address'
        }
      }
    },
    username: {
      type: Sequelize.STRING,
      unique: true,
      validate: {
        len: {
          args: 5,
          msg: 'Username must be atleast 5 characters in length'
        },
        isAlpha: {
          msg: 'Username can contain only letters A-Z,a-z'
        }
      }
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    token: {
      type: Sequelize.STRING,
      allowNull: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    avatar: {
      type: Sequelize.STRING,
      defaultValue: 'avatar.png'
    },
    joinDate: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    addressId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = User;