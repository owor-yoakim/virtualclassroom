'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const RolePermission = conn.define('role_permission', {
  roleId: {type: Sequelize.INTEGER, allowNull: false},
  permissionId: { type: Sequelize.INTEGER, allowNull: false },
  active: {type: Sequelize.BOOLEAN, defaultValue: true}
},{timestamps: false, paranoid: false, underscored: false});

module.exports = RolePermission;
