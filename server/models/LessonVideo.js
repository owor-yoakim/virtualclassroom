'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const LessonVideo = conn.define(
  'lesson_video', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    fileName: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    size: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    extension: {
      type: Sequelize.STRING,
      allowNull: false
    },
    duration: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    path: {
      type: Sequelize.STRING,
      allowNull: false
    },
    localPath: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lessonId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    lessonModuleId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = LessonVideo;