'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Role = conn.define(
    'role', {
      id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
      title: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          len: {args: 3, msg: 'Title must be atleast 3 characters in length'}
        }
      },
      slug: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
          len: {args: 3, msg: 'Slug must be atleast 3 characters in length'}
        }
      },
      active: {type: Sequelize.BOOLEAN, defaultValue: true},
      description: {type: Sequelize.STRING, allowNull: true}
    },
    {
      underscored: false,
      timestamps: true,
      paranoid: true
    });

module.exports = Role;
