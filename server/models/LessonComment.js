'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;
/**
 * @description Creates the Sequelize DB Model definition and the table
 *"lesson_comments" in the DB
 * @author  Owor Yoakim
 */

const LessonComment = conn.define(
  'lesson_comment', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    body: {
      type: Sequelize.STRING,
      allowNull: false
    },
    lessonId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = LessonComment;