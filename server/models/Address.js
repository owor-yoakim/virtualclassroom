'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Address = conn.define(
    'address', {
        type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        line_1: {
            type: Sequelize.STRING,
            allowNull: false
        },
        line_2: {
            type: Sequelize.STRING,
            allowNull: true
        },
        city: {
            type: Sequelize.STRING,
            allowNull: false
        },
        state: {
            type: Sequelize.STRING,
            allowNull: true
        },
        zip: {
            type: Sequelize.STRING,
            allowNull: true
        },
        countryId: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, {
        underscored: false,
        timestamps: true,
        paranoid: true
    });

module.exports = Address;