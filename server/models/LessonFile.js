'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;
/**
 * @description Creates the Sequelize DB Model definition and the table
 * "lesson_files" in the DB
 * @author  Owor Yoakim
 */

const LessonFile = conn.define(
  'lesson_file', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    size: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false
    },
    path: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    localPath: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    lessonId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    instructorId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    studentId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    lessonModuleId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

module.exports = LessonFile;