"use strict";

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;
	
const UserLogin = conn.define('user_login', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		email: {
			type: Sequelize.STRING,
			unique: true,
			allowNull: false
		},
		timestamp: {
			type: Sequelize.INTEGER,
			allowNull: false
		},
		ipAddress: {
			type: Sequelize.STRING,
			allowNull: false
		},
		token: {
			type: Sequelize.TEXT,
			allowNull: false
		},
		expiryTime: {
			type: Sequelize.INTEGER,
			allowNull: false
		}
	},{
		underscored: false,
		timestamps: true,
		paranoid: true
	  });

module.exports = UserLogin;
