"use strict";

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const InstructorLesson = conn.define('instructor_lesson', {
    instructorId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false
    },
    lessonId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false
    },
    active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
}, {
    timestamps: true,
    paranoid: true
});

module.exports = InstructorLesson;