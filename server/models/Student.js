'use strict';
const bcrypt = require('bcrypt');
const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Student = conn.define(
  'student', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'First name must be atleast 3 characters in length'
        }
      }
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 3,
          msg: 'Last name must be atleast 3 characters in length'
        }
      }
    },
    gender: {
      type: Sequelize.ENUM(['male', 'female']),
      defaultValue: 'male'
    },
    emailAddress: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: {
          msg: 'Invalid Email Address'
        }
      }
    },
    phoneNumber: {
      type: Sequelize.STRING,
      allowNull: true
    },
    username: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    passWord: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        len: {
          args: 6,
          msg: 'Password length should be atleast 6 characters long.'
        }
      }
    },
    avatar: {
      type: Sequelize.STRING,
      defaultValue: 'avatar.png'
    },
    joinDate: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    addressId: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  }, {
    underscored: false,
    timestamps: true,
    paranoid: true
  });

// Student.hook('afterValidate', (student, options) => {
//   student.passWord = bcrypt.hashSync(student.passWord, 10);
// });

module.exports = Student;