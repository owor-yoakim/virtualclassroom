'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Page = conn.define(
    'page', {
      id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
      title: {type: Sequelize.STRING, allowNull: false},
      slug: {type: Sequelize.STRING, unique: true, allowNull: false},
      body: {type: Sequelize.TEXT, allowNull: true},
      published: {type: Sequelize.BOOLEAN, defaultValue: true},
      userId: {type: Sequelize.INTEGER, allowNull: true}
    },
    {
      underscored: false,
      timestamps: true,
      paranoid: true
    });

module.exports = Page;
