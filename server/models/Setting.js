'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const Setting = conn.define(
    'setting', {
      id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
      key: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {len: {args: 3, msg: 'Minimum length is 4 characters'}}
      },
      value: {type: Sequelize.STRING, allowNull: true}
    },
    {
      underscored: false,
      timestamps: true,
      paranoid: true
    });

module.exports = Setting;
