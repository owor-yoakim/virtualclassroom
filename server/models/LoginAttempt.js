'use strict';

const Sequelize = require('../config/db').Sequelize;
const conn = require('../config/db').conn;

const LoginAttempt = conn.define(
    'login_attempt', {
      id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
      userId: {type: Sequelize.INTEGER, allowNull: false},
      timestamp: {type: Sequelize.INTEGER, allowNull: false, defaultValue: Sequelize.NOW},
      ipAddress: {type: Sequelize.STRING, allowNull: false},
      username: {type: Sequelize.STRING, allowNull: false},
      count: {type: Sequelize.SMALLINT, defaultValue: 0}
    },
    {underscored: false, timestamps: true,paranoid: true});

module.exports = LoginAttempt;
