const chatService = require('../services/chat-service');

module.exports.createChat = createChat;
module.exports.getAllChats = getAllChats;
module.exports.getChatById = getChatById;
module.exports.getLessonChats = getLessonChats;
module.exports.uploadFile = uploadFile;


function getAllChats(req, res) {
  chatService.getAllChats().then((chats) => {
    res.status(200).send(chats);
  }).catch((err) => {
    res.status(401).send(err);
  });
}

function createChat(req, res) {
  let chatData = req.body;
  //  console.log("Data: " + chatData);
  chatService.createChat(chatData).then((chat) => {
    res.status(200).send(chat);
  }).catch((err) => {
    res.status(500).send(err);
  });
}


function getChatById(req, res) {
  let chatId = req.params.chatId;
  chatService.getChatById(chatId).then((chat) => {
    res.status(200).send(chat);
  }).catch((err) => {
    res.status(401).send(err);
  });
};

function getLessonChats(req, res) {
  let lessonId = req.params.lessonId;
  chatService.getLessonChats(lessonId).then((chats) => {
    res.status(200).send(chats);
  }).catch((err) => {
    res.status(401).send(err);
  });
};

function uploadFile(req, res) {
  res.status(200).send('Ok');
}
