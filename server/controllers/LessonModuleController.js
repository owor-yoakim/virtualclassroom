const config = require('../config/config');
const jwt = require('jsonwebtoken');

const userService = require('../services/user-service');
const subjectService = require('../services/subject-service');
const lessonService = require('../services/lesson-service');
const lessonModuleService = require('../services/lesson-module-service');

module.exports.getAll = function (req, res) {
    lessonModuleService.findAll(id).then((lessonModules) => {
        res.status(200).send(lessonModules);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getById = function (req, res) {
    let id = req.params.lessonModuleId;
    lessonModuleService.findById(id).then((lessonModule) => {
        if (lessonModule === null) {
            res.status(404).send("Not Found");
        } else {
            res.status(200).send(lessonModule);
        }
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getByLesson = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonModuleService.findByLesson(lessonId).then((lessonModules) => {
        res.status(200).send(lessonModules);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getByInstructor = function (req, res) {
    let instructorId = req.params.instructorId;
    lessonModuleService.findByInstructor(instructorId).then((lessonModules) => {
        res.status(200).send(lessonModules);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getBySubject = function (req, res) {
    let subjectId = req.params.subjectId;
    lessonModuleService.findBySubject(subjectId).then((lessonModules) => {
        res.status(200).send(lessonModules);
    }).catch((err) => {
        console.error(err);
        res.status(500).send("Internal Server Error " + err);
    });
};

module.exports.getAll = function (req, res) {
    lessonModuleService.findAll().then((lessonModules) => {
        res.status(200).send(lessonModules);
    }).catch((err) => {
        console.error(err);
        res.status(500).send("Internal Server Error " + err);
    });
};

module.exports.create = function (req, res) {
    let data = req.body;
    //console.log("Module: ", data);
    lessonModuleService.create(data).then((lessonModule) => {
        res.status(200).send(lessonModule);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.update = function (req, res) {
    let id = req.params.lessonModuleId;
    let data = req.body;
    console.log("Module data: ", data);
    lessonModuleService.update(id, data).then((lessonModule) => {
        res.status(200).send(lessonModule);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.delete = function (req, res) {
    let id = req.params.lessonModuleId;
    lessonModuleService.delete(id).then((result) => {
        res.status(200).send(result);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.activateOrDeactivate = function (req, res) {

};