const settingService = require('../services/setting-service');

module.exports.getAll = function (req, res) {
  settingService.findAll().then(settings => {
    res.status(200).send(settings);
  }).catch(err => {
    res.status(401).send(err);
  });
};

module.exports.create = function (req, res) {
  let data = req.body;
  //  console.log("Data: " + data);
  settingService.create(data)
  .then((setting) => {res.status(200).send(setting);})
  .catch((err) => {res.status(500).send(err);});
};

module.exports.getById = function (req, res) {
  let id = req.params.id;
  settingService.findById(id)
  .then((setting) => {res.status(200).send(setting);})
  .catch((err) => {res.status(401).send(err);});
};

module.exports.getByKey = function (req, res) {
  let key = req.params.key;
  settingService.findByKey(key)
  .then((setting) => {res.status(200).send(setting);})
  .catch((err) => {res.status(401).send(err);});
};

module.exports.update = function (req, res) {
  let id = req.params.id;
  let data = req.body;
  settingService.update(id, data)
  .then((setting) => {res.status(200).send(setting);})
  .catch((err) => {res.status(500).send(err);});
};
