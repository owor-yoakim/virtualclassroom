const jwt = require('jsonwebtoken');
const Base64 = require('js-base64').Base64;
const config = require('../config/config');
const restrict = require('../config/restrict');
const userService = require('../services/user-service');

module.exports.login = function (req, res) {
    let data = req.body;
    //console.log("Login Data: ", data);
    let loggedInUser = null;
    userService.authenticate(data.email, data.password).then((user) => {
        if (user) {
            //authentication successful
            loggedInUser = user;
            SESSION = Base64.encode(JSON.stringify(user));
            //generate authentication token
            return restrict.createSessionToken(user, config.adminSecret, config.sessionDurationInSconds);
        } else {
            // Invalid credentials
            return (null)
        }

    }).then((token) => {
        // If we have a token
        if (token !== null) {
            let sessionData = {
                token: token,
                SESSION: SESSION
            };
            console.log(sessionData);
            res.status(200).send(sessionData);
        } else {
            // return success with no user
            res.status(200).send(null);
        }
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.logout = function (req, res) {
    let token = req.headers.authorization;
    if (typeof token !== 'undefined') {
        //console.log("Request Token: ", token);
        jwt.verify(token, config.adminSecret, (err, data) => {
            if (err) {
                console.error("Request Forbidden: ", err);
                res.status(401).send('Request Forbidden');
            } else {
                //console.log("Logout Request: ", data);
                res.status(200).send(data);
            }
        });
    } else {
        console.log("Error: ", "Request Forbidden");
        res.status(401).send('Request Forbidden');
    }
};

module.exports.checkSession = function (req, res) {
    let token = req.headers.authorization;
    let msg = "Unauthorized Access! Session expired. Please login to continue";
    if (typeof token !== 'undefined') {
        //console.log("Request Token: ", token);
        jwt.verify(token, config.adminSecret, (err, data) => {
            if (err) {
                console.error("Bad Request: ", err);
                res.status(401).send(msg);
            } else {
                data.token = token;
                //console.log("Request Ok ", data);
                res.status(200).send(data);
            }
        });
    } else {
        console.error("Bad Request: ", msg);
        res.status(401).send(msg);
    }
};

module.exports.changePassword = function (req, res) {
    let data = req.body;
    userService.changePassword(data)
        .then((result) => {
            res.status(204).send(result);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
};

module.exports.resetPassword = function (req, res) {
    res.status(200).send("OK");
};