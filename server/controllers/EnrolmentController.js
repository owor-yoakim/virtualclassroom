const enrolmentService = require('../services/enrolment-service');
const Enrolment = require('../classes/Enrolment');

function getEnrolment(req, res) {
    let studentId = Number(req.params.studentId);
    let lessonId = Number(req.params.lessonId);
    enrolmentService.getEnrolment(studentId, lessonId)
        .then((enrolment) => {
            res.status(200).send(enrolment);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

function getActiveEnrolments(req, res) {
    enrolmentService.getActiveEnrolments()
        .then((enrolments) => {
            res.status(200).json(enrolments);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

function getInactiveEnrolments(req, res) {
    enrolmentService.getInactiveEnrolments()
        .then((enrolments) => {
            res.status(200).json(enrolments);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}


function getAllEnrolments(req, res) {
    enrolmentService.getAllEnrolments()
        .then((enrolments) => {
            res.status(200).send(enrolments);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}


function getEnrolmentsByStudent(req, res) {
    let studentId = Number(req.params.studentId);
    enrolmentService.getEnrolmentsByStudent(studentId)
        .then((enrolments) => {
            res.status(200).send(enrolments);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

function getEnrolmentsByLesson(req, res) {
    let lessonId = Number(req.params.lessonId);
    enrolmentService.getEnrolmentsByLesson(lessonId)
        .then((enrolments) => {
            res.status(200).send(enrolments);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}


function createEnrolment(req, res) {
    let enrolment = req.body;
    console.log(enrolment);
    enrolmentService.createEnrolment(enrolment)
        .then((enrolment) => {
            res.status(201).send(enrolment);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).send(err);
        });
}

function updateEnrolment(req, res) {
    let studentId = Number(req.params.studentId);
    let lessonId = Number(req.params.lessonId);
    let data = req.body;
    delete data.studentId;
    delete data.lessonId;
    delete data.createdAt;
    delete data.deletedAt;
    enrolmentService.updateEnrolment(studentId, lessonId, data)
        .then((data) => {
            res.status(201).send(data);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
function deleteEnrolment(req, res) {
    let studentId = Number(req.params.studentId);
    let lessonId = Number(req.params.lessonId);
    enrolmentService.deleteEnrolment(studentId, lessonId)
        .then((data) => {
            res.status(204).send('Enrolment Deleted Successfully. ' + data + 'Rows Affected.');
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

function activateOrDeactivate(req, res) {
    let studentId = Number(req.params.studentId);
    let lessonId = Number(req.params.lessonId);
    enrolmentService.activateOrDeactivate()
        .then((data) => {
            res.status(201).send(data);
        })
        .catch((err) => {
            res.status(501).send(err);
        });
}

module.exports.getEnrolment = getEnrolment;
module.exports.getActiveEnrolments = getActiveEnrolments;
module.exports.getInactiveEnrolments = getInactiveEnrolments;
module.exports.getAllEnrolments = getAllEnrolments;
module.exports.getEnrolmentsByStudent = getEnrolmentsByStudent;
module.exports.getEnrolmentsByLesson = getEnrolmentsByLesson;
module.exports.createEnrolment = createEnrolment;
module.exports.updateEnrolment = updateEnrolment;
module.exports.activateOrDeactivate = activateOrDeactivate;
module.exports.deleteEnrolment = deleteEnrolment;