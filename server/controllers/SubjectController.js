var config = require('../config/config');
var jwt = require('jsonwebtoken');

var userService = require('../services/user-service');
var subjectService = require('../services/subject-service');
var lessonService = require('../services/lesson-service');


module.exports.getAll = function (req, res) {
    subjectService.findAll().then(subjects => {
        res.status(200).send(subjects);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.create = function (req, res) {
    let data = req.body;
    //console.log("Body: ", data);
    subjectService.create(data).then(subject => {
        res.status(200).send(subject);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.delete = function (req, res) {
    let id = req.params.id;
    //console.log("Param: ", id);
    subjectService.delete(id).then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.update = function (req, res) {
    let id = req.params.id;
    let data = req.body;
    console.log("Request body:", data);
    subjectService.update(id, data).then(subject => {
        res.status(200).send(subject);
    }).catch(err => {
        res.status(500).send(err);
    });
};

module.exports.activate = function (req, res) {
    res.status(200).send("OK");
};

module.exports.deactivate = function (req, res) {
    res.status(200).send("OK");
};

module.exports.getById = function (req, res) {
    let id = req.params.id;
    subjectService.findById(id).then(subject => {
        res.status(200).send(subject);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getBySlug = function (req, res) {
    let slug = req.params.slug;
    subjectService.findBySlug(slug).then(subject => {
        if (subject === null) {
            res.status(403).send("Request Forbidden");
        } else {
            res.status(200).send(subject);
        }
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.numberOfSubjects = function (req, res) {
    subjectService.numberOfSubjects().then(num => {
        //console.log("Subject count: ", num);
        res.status(200).send({
            count: num
        });
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getInstructorsBySubjectSlug = function (req, res) {
    let slug = req.params.subjectSlug;
    subjectService.getInstructorsBySubjectSlug(slug).then((instructors) => {
        res.status(200).json(instructors);
    }).catch((err) => {
        res.status(500).send(err);
    });
}