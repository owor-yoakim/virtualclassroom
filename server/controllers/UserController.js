const userService = require('../services/user-service');

/* UserController Actions */
module.exports.getAll = function (req, res) {
    userService.getAllUsers().then(users => {
        res.status(200).json(users);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.getByRole = function (req, res) {
    let roleId = req.params.roleId;
    userService.getUsersByRole(roleId).then(users => {
        res.status(200).json(users);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.getById = function (req, res) {
    let id = req.params.userId;
    userService.getUserById(id).then(user => {
        res.status(200).json(user);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.getByEmail = function (req, res) {
    let id = req.params.email;
    userService.getUserByEmail(email).then(user => {
        res.status(200).json(user);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}


module.exports.createUser = function (req, res) {
    let userData = req.body;
    console.log('Body', userData);
    userService.getUserByEmailOrUsername(userData.emailAddress, userData.username).then((user) => {
        if (user) {
            //email already taken
            console.error("Error: ", "Email Address or Username Already Taken");
            res.status(403).send('Email Address or Username Already Taken');
            return;
        }
        //email not in use
        return userService.createUser(userData);
    }).then((user) => {
        //successfully registered
        console.log('New user', user);
        res.status(200).send(user);
    }).catch((err) => {
        //sever error
        console.log("Internal Server Error: ", err);
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.updateUser = function (req, res) {
    let data = req.body;
    let id = req.params.userId;
    userService.updateUser(id, data).then((user) => {
        res.status(200).send(user);
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    })
}

module.exports.deleteUser = function (req, res) {
    let id = req.params.userId;
    userService.deleteUser(id).then(user => {
        res.status(204).send('Delete Successful');
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.activateOrDeactivateUser = function (req, res) {
    let id = req.params.userId;
    userService.activateOrDeactivateUser(id).then((user) => {
        console.log(user.active);
        res.status(204).send('Status Updated Successful');
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    });
}