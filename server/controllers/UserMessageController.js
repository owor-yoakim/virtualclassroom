const userMessageService = require('../services/user-message-service');

module.exports.getAllMessages = function (req, res) {
    userMessageService.getAllMessages().then((messages) => {
        res.status(200).send(messages);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.getById = function (req, res) {
    let id = req.params.id;
    userMessageService.getById(id).then((message) => {
        res.status(200).send(message);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.getBySenderId = function (req, res) {
    let senderId = req.params.senderId;
    userMessageService.getBySenderId(senderId).then((messages) => {
        res.status(200).send(messages);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.getBySenderEmail = function (req, res) {
    let senderEmail = req.params.senderEmail;
    userMessageService.getBySenderEmail(senderEmail).then((messages) => {
        res.status(200).send(messages);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.getByReceiverId = function (req, res) {
    let receiverId = req.params.receiverId;
    userMessageService.getByReceiverId(receiverId).then((messages) => {
        res.status(200).send(messages);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.getByReceiverEmail = function (req, res) {
    let receiverEmail = req.params.receiverEmail;
    userMessageService.getByReceiverEmail(receiverEmail).then((messages) => {
        res.status(200).send(messages);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}

module.exports.createUserMessage = function (req, res) {
    let data = req.body;
    userMessageService.createUserMessage(data).then((message) => {
        res.status(200).send(message);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}


module.exports.updateUserMessage = function (req, res) {
    let data = req.body;
    let id = req.params.id;
    userMessageService.updateUserMessage(id, data).then((message) => {
        res.status(200).send(message);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}


module.exports.deleteUserMessage = function (req, res) {
    let id = req.params.id;
    userMessageService.deleteUserMessage(id).then((data) => {
        res.status(200).send(data);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
}