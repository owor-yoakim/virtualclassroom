const Base64 = require('js-base64').Base64;
const config = require('../config/config');
const restrict = require('../config/restrict');
const instructorService = require('../services/instructor-service');

module.exports.login = function (req, res) {
    let data = req.body;
    console.log("Encoded Data: ", data);
    let credentials = JSON.parse(Base64.decode(data.data));
    console.log("Credentials: ", credentials);
    let SESSIONID = null;
    //attempt instructor login
    instructorService.tryAuthenticate(credentials.loginName, credentials.loginPassword).then((instructor) => {
        if (instructor) {
            // authentication successful
            instructor.type = 'instructor';
            SESSIONID = Base64.encode(JSON.stringify(instructor));
            //generate authentication token
            return restrict.createSessionToken(instructor, config.instructorSecret, config.sessionDurationInSconds);
            //return authenticated instructor
            //res.status(200).send(loggedInInstructor);

            // }).catch((err) => {
            //     console.error(err);
            //     res.status(500).send("Internal Server Error: " + err);
            // });
        } else {
            // authentication failed
            //console.log("Request Forbidden: ", "Invalid Credentials");
            //res.status(400).send('Invalid Credentials');
            return (null);
        }
    }).then((token) => {
        if (token !== null) {
            let sessionData = {
                token: token,
                SESSIONID: SESSIONID
            };
            console.log(sessionData);
            res.status(200).send(sessionData);
        } else {
            res.status(200).send(null);
        }
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.logout = function (req, res) {
    let token = req.headers.authorization;
    //let token = req.body.token;
    if (typeof token !== 'undefined') {
        //console.log("Request Token: ", token);
        restrict.verifySessionToken(token, config.instructorSecret).then((data) => {
            //console.log("Logout Request: ", data);
            res.status(200).send(data);
        }).catch((err) => {
            console.error("Bad Request. Already logged out: ", err);
            res.status(400).send('Already logged out');
        });
    } else {
        console.log("Error: ", "Request Forbidden");
        res.status(401).send('Request Forbidden');
    }
};

module.exports.checkSession = async function (req, res) {
    let token = req.headers.authorization;
    let msg = "Unauthorized Access! Session expired. Please login to continue";
    if (typeof token !== 'undefined' || token !== null) {
        //console.log("Request Token: ", token);
        restrict.verifySessionToken(token, config.instructorSecret).then((data) => {
            data.token = token;
            //console.log("Request Ok ", data);
            res.status(200).send(data);
        }).catch((err) => {
            console.error("Bad Request: ", err);
            res.status(401).send(msg);
        });
    } else {
        console.error("Bad Request: ", msg);
        res.status(401).send(msg);
    }
};

module.exports.getAll = function (req, res) {
    instructorService.getAll().then((instructors) => {
        res.status(200).json(instructors);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};


module.exports.getById = function (req, res) {
    let id = req.params.instructorId;
    instructorService.getById(id).then((instructor) => {
        res.status(200).json(instructor);
    }).catch(err => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.getLessons = function (req, res) {
    let id = req.params.instructorId;
    instructorService.getInstructorLessons(id).then((lessons) => {
        res.status(200).json(lessons);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};
module.exports.getSchedules = function (req, res) {
    let id = req.params.instructorId;
    instructorService.getInstructorSchedules(id).then((schedules) => {
        res.status(200).json(schedules);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.getSubjects = function (req, res) {
    let id = req.params.instructorId;
    instructorService.getInstructorSubjects(id).then((subjects) => {
        res.status(200).json(subjects);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};



module.exports.createInstructor = function (req, res) {
    let instructor = req.body;
    instructorService.createInstructor(instructor).then((instructor) => {
        res.status(200).json(instructor);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.updateInstructor = function (req, res) {
    let id = req.params.instructorId;
    let data = req.body;
    instructorService.updateInstructor(id, data).then((instructor) => {
        res.status(200).json(instructor);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
}

module.exports.assignLesson = function (req, res) {
    let instructorId = req.params.instructorId;
    let lessonId = req.params.lessonId;
    instructorService.assignLesson(instructorId, lessonId).then((result) => {
        res.status(200).json(result);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.removeLesson = function (req, res) {
    let instructorId = req.params.instructorId;
    let lessonId = req.params.lessonId;
    instructorService.removeLesson(instructorId, lessonId).then((result) => {
        res.status(200).json(result);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.activateOrDeactivateInstructor = function (req, res) {
    let id = req.params.instructorId;
    instructorService.activateOrDeactivateInstructor(id).then((instructor) => {
        res.status(200).json(instructor);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.deleteInstructor = function (req, res) {
    let instructorId = req.params.instructorId;
    instructorService.deleteInstructor(instructorId).then((result) => {
        res.status(200).json(result);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};

module.exports.getInstructorsBySubjectSlug = function (req, res) {
    let slug = req.params.subjectSlug;
    instructorService.getInstructorsBySubjectSlug(slug).then((instructors) => {
        console.log(instructors);
        res.status(200).json(instructors);
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};