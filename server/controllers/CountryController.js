var countryService = require('../services/country-service');

module.exports.getAll = function (req, res) {
    countryService.getAll().then(countries => {
        res.status(200).send(countries);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
        console.error(err);
    });
};

module.exports.getById = function (req, res) {
    let id = req.params.countryId;
    countryService.getById(id).then(country => {
        res.status(200).send(country);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
        console.error(err);
    });
};

module.exports.getByCode = function (req, res) {
    let code = req.params.code;
    countryService.getByCode(code).then(country => {
        res.status(200).send(country);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
        console.error(err);
    });
};