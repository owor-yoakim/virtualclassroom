var config = require('../config/config');
var jwt = require('jsonwebtoken');
var util = require('util');
var studentService = require('../services/student-service');
var instructorService = require('../services/instructor-service');

module.exports.apiTest = function (req, res) {
    instructorService.getAll().then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(401).send(err);
    });
};