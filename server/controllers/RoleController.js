const userService = require('../services/user-service');

/* RoleController Actions */
module.exports.getAll = function (req, res) {
    userService.getAllRoles().then(roles => {
        res.status(200).json(roles);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}


module.exports.getById = function (req, res) {
    let id = req.params.roleId;
    userService.getRoleById(id).then(role => {
        res.status(200).json(role);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.getBySlug = function (req, res) {
    let id = req.params.slug;
    userService.getRoleBySlug(slug).then(role => {
        res.status(200).json(role);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.createRole = function (req, res) {
    let data = req.body;

    userService.createRole(data).then(role => {
        res.status(200).json(role);
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    })
}

module.exports.updateRole = function (req, res) {
    let newRole = req.body;
    let id = req.params.roleId;
    userService.updateRole(id, newRole).then(role => {
        res.status(200).json(role);
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    })
}

module.exports.deleteRole = function (req, res) {
    let id = req.params.roleId;
    userService.deleteRole(id).then(role => {
        res.status(200).send('Delete Successful');
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    });
}