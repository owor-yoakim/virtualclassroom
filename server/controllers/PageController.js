var pageService = require('../services/page-service');

module.exports.getPageContent = function (req, res) {
    let slug = req.params.pageSlug;
    pageService.getBySlug(slug).then(page => {
        res.status(200).send(page);
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
        console.error(err);
    });
};

module.exports.create = function (req, res) {
    let data = req.body;
    res.status(200).send('Ok');
}

module.exports.update = function (req, res) {
    let id = req.params.id;
    res.status(200).send('Ok: ' + id);
}

module.exports.publish = function (req, res) {
    let id = req.params.id;
    res.status(200).send('Ok: ' + id);
}

module.exports.unpublish = function (req, res) {
    let id = req.params.id;
    res.status(200).send('Ok: ' + id);
}

module.exports.deletePage = function (req, res) {
    let id = req.params.id;
    res.status(200).send('Ok: ' + id);
}