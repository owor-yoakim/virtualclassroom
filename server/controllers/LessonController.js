var multer = require('multer');
var path = require('path');
const Base64 = require('js-base64').Base64;
const config = require('../config/config');

const userService = require('../services/user-service');
const subjectService = require('../services/subject-service');
const lessonService = require('../services/lesson-service');

const filePath = 'public/assets/uploads/lessons/';

var datetimestamp = Date.now();

const imageFilter = function (req, file, cb) {
    // accept image only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

const fileFilter = function (req, file, cb) {
    // accept images, pdf, word, powerpoint, excel, text
    if (!file.originalname.match(
            /\.(jpg|jpeg|png|gif|rtf|pdf|doc|docx|ppt|pptx|xls|xlsx|txt)$/)) {
        return cb(new Error('Invalid file format!'), false);
    }
    cb(null, true);
};

var storage = multer.diskStorage({
    // destination
    destination: (req, file, cb) => {
        cb(null, filePath);
    },
    filename: (req, file, cb) => {
        // var ext = file.originalname.split('.').pop();
        datetimestamp = Date.now();
        cb(null, '' + datetimestamp + '-' + file.originalname);
    }
});
// define the type of upload multer would be doing and pass in its destination,
// in our case, its a single file with the name fileInput
var upload = multer({
    storage: storage,
    limits: {
        fileSize: 2 * 1024 * 1024 // 2mb, in bytes
    },
    fileFilter: fileFilter
}).single('fileInput');

module.exports.getById = function (req, res) {
    let id = req.params.id;
    lessonService.findById(id).then((lesson) => {
        if (lesson === null) {
            res.status(404).send("Not Found");
        } else {
            res.status(200).send(lesson);
        }
    }).catch((err) => {
        res.status(501).send("Internal Server Error: " + err);
    });
};

module.exports.getBySlug = function (req, res) {
    let slug = req.params.slug;
    lessonService.findBySlug(slug).then((lesson) => {
        res.status(200).send(lesson);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getBySubject = function (req, res) {
    let subjectId = req.params.subjectId;
    lessonService.findBySubject(subjectId).then((lessons) => {
        res.status(200).send(lessons);
    }).catch((err) => {
        console.error(err);
        res.status(500).send("Internal Server Error " + err);
    });
};

module.exports.getPrerequisites = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonService.getPrerequisites(lessonId).then((lessons) => {
        res.status(200).send(lessons);
    }).catch((err) => {
        console.error(err);
        res.status(500).send("Internal Server Error " + err);
    });
};

module.exports.getAll = function (req, res) {
    lessonService.findAll().then((lessons) => {
        //let lessonsDataEncoded = Base64.encode(JSON.stringify(lessons));
        //console.log(lessonsDataEncoded);
        res.status(200).send(lessons);
        //res.status(200).send({data: lessonsDataEncoded});
    }).catch((err) => {
        console.error(err);
        res.status(500).send("Internal Server Error " + err);
    });
};

module.exports.create = function (req, res) {
    let data = req.body;
    //console.log("Lesson: ", data);
    lessonService.create(data).then((lesson) => {
        res.status(200).send(lesson);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.update = function (req, res) {
    let id = req.params.id;
    let data = req.body;
    console.log("Lesson: ", data);
    lessonService.update(id, data).then((lesson) => {
        res.status(200).send(lesson);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};
module.exports.updateInstructorLesson = function (req, res) {
    let id = req.params.lessonId;
    let data = req.body;
    console.log("Lesson: ", data);
    lessonService.updateInstructorLesson(id, data).then((lesson) => {
        res.status(200).send(lesson);
    }).catch((err) => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.numberOfLessons = function (req, res) {
    lessonService.numberOfLessons().then(num => {
        //console.log("Lesson count: ", num);
        res.status(200).send({
            count: num
        });
    }).catch(err => {
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.countBySubject = function (req, res) {
    let subjectId = req.params.subjectId;
    lessonService.countBySubject(subjectId).then(num => {
        //console.log("Lesson count: ", num);
        res.status(200).send({
            count: num
        });
    }).catch(err => {
        console.error(err);
        res.status(500).send("Internal Server Error: " + err);
    });
};

module.exports.getLessonEnrolments = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonService.getLessonEnrolments(lessonId).then((enrolments) => {
        res.status(200).json(enrolments);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};
module.exports.getLessonModules = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonService.getLessonModules(lessonId).then((lessonModules) => {
        res.status(200).json(lessonModules);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

module.exports.getLessonInstructors = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonService.getLessonInstructors(lessonId).then((instructors) => {
        res.status(200).json(instructors);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

module.exports.addEnrolment = function (req, res) {
    let lessonId = req.params.lessonId;
    let data = req.body;
    lessonService.addEnrolment(lessonId, data).then((data) => {
        res.status(200).json(data);
    }).catch((err) => {
        res.status(501).send(err);
    });
};

module.exports.uploadFile = function (req, res) {

    //console.log(req.body);
    //const originalname = String(req.body.name);
    //const size = Number(req.body.size) || 0;

    //if (size > config.maximumFileSize) {
    //    return res.status(400).send('File too large, maximum size allowed is 2MB');
    //}

    //const result = originalname.split('.');
    //console.log('Result',result);
    //const extension = result.pop();
    //console.log('Extension', extension);
    //console.log('Test', config.allowedFileFormats.includes(extension));

    //if (!config.allowedFileFormats.includes(extension)) {
    //  return res.status(400).send('Invalid file formats');
    //}

    upload(req, res, (err) => {
        if (err) {
            // An error occurred while uploading
            console.log(err);
            return res.status(422).send('An Error occured while uploading file' + err)
        }

        // console.log(req.body);

        const originalname = String(req.body.name);
        const size = Number(req.body.size) || 0;
        const ext = originalname.split('.').pop();
        console.log('Extension', ext);
        console.log('Test', config.allowedFileFormats.includes(ext));
        const filename = '' + datetimestamp + '-' + originalname;
        const originalfile = {
            name: filename,
            description: req.body.description,
            type: req.body.type,
            size: size,
            localPath: 'assets/uploads/lessons/' + filename,
            path: req.body.path || null,
            instructorId: Number(req.body.instructorId) || null,
            lessonId: Number(req.body.lessonId) || null,
            clientId: Number(req.body.clientId) || null
        };
        console.log('LessonFile', originalfile);

        lessonService.uploadFile(originalfile).then((file) => {
            res.status(200).send(file);
        }).catch((err) => {
            console.error(err);
            res.status(500).send('Internal Server Error' + err);
        });
    });

};

module.exports.delete = function (req, res) {

};

module.exports.activate = function (req, res) {

};

module.exports.deactivate = function (req, res) {

};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getLessonComments = function (req, res) {
    let lessonId = req.params.lessonId;
    lessonService.getLessonComments(lessonId).then((comments) => {
        res.status(200).send(comments);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getLessonModuleComments = function (req, res) {
    let lessonId = req.params.lessonId;
    let lessonModuleId = req.params.lessonModuleId;
    lessonService.getLessonModuleComments(lessonModuleId).then((comments) => {
        res.status(200).send(comments);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getLessonComment = function (req, res) {
    let lessonId = req.params.lessonId;
    let commentId = req.params.commentId;
    lessonService.getLessonComment(lessonId, commentId).then((comment) => {
        res.status(200).send(comment);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.createLessonComment = function (req, res) {
    let lessonId = req.params.lessonId;
    let comment = req.body;
    lessonService.createLessonComment(lessonId, comment).then((comment) => {
        res.status(200).send(comment);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.updateLessonComment = function (req, res) {
    let lessonId = req.params.lessonId;
    let commentId = req.params.commentId;
    let data = req.body;
    lessonService.updateLessonComment(lessonId, commentId, data).then((comment) => {
        res.status(200).send(comment);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.deleteLessonComment = function (req, res) {
    let lessonId = req.params.lessonId;
    let commentId = req.params.commentId;
    let data = req.body;
    lessonService.deleteLessonComment(lessonId, commentId).then((comment) => {
        res.status(200).send(comment);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    })
};