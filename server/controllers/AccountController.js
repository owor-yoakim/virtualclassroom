const Base64 = require('js-base64').Base64;
const bcrypt = require('bcrypt');
const config = require('../config/config');
const restrict = require('../config/restrict');
const studentService = require('../services/student-service');
const instructorService = require('../services/instructor-service');

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.login = function (req, res) {
    let data = req.body;
    console.log("Encoded Data: ", data);
    let credentials = JSON.parse(Base64.decode(data.data));
    console.log("Credentials: ", credentials);
    let SESSIONID = null;
    //attempt learner login
    studentService.tryAuthenticate(credentials.loginName, credentials.loginPassword).then((student) => {
        if (student) {
            // authentication successful
            console.log("Authenticated Student: ", student);
            student.type = 'learner';
            SESSIONID = Base64.encode(JSON.stringify(student));
            //generate authentication token
            return restrict.createSessionToken(student, config.studentSecret, config.sessionDurationInSconds);
        } else {
            // Invalid Credentials
            return (null);
        }
    }).then((token) => {
        // If we have a token
        if (token !== null) {
            let sessionData = {
                token: token,
                SESSIONID: SESSIONID
            };
            console.log(sessionData);
            res.status(200).send(sessionData);
        } else {
            // return success with no user
            res.status(200).send(null);
        }
    }).catch((err) => {
        console.error(err);
        res.status(400).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.logout = function (req, res) {
    let token = req.headers.authorization;
    //let token = req.body.token;
    if (typeof token !== 'undefined') {
        //console.log("Request Token: ", token);
        restrict.verifySessionToken(token, config.studentSecret).then((studentData) => {
            //console.log("Logout Request: ", studentData);
            res.status(200).send(studentData);
        }).catch((err) => {
            console.error("Bad Request. Already logged out: ", err);
            res.status(400).send('Already logged out');
        });
    } else {
        console.log("Error: ", "Request Forbidden");
        res.status(401).send('Request Forbidden');
    }
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.checkSession = function (req, res) {
    let token = req.headers.authorization;
    let responseMessage = "Unauthorized Access! Please login to continue";
    if (typeof token !== 'undefined' || token !== null) {
        //console.log("Request Token: ", token);
        restrict.verifySessionToken(token, config.studentSecret).then((studentData) => {
            studentData.token = token;
            //console.log("Request Ok ", studentData);
            res.status(200).send(studentData);
        }).catch((err) => {
            console.error("Bad Request: ", err);
            res.status(401).send(responseMessage);
        });
    } else {
        console.error("Bad Request: ", responseMessage);
        res.status(401).send(responseMessage);
    }
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.register = function (req, res) {
    let studentData = req.body;
    console.log('Body', studentData);
    studentService.getByEmail(studentData.emailAddress).then((student) => {
        if (student) {
            //email already taken
            console.error("Error: ", "Email Address Already Taken");
            res.status(403).send('Email Address Already Taken');
            return null;
        }
        //email not in use
        return studentService.createStudent(studentData);
    }).then((student) => {
        //successfully registered
        console.log('New student', student);
        res.status(200).send(student);
    }).catch((err) => {
        //sever error
        console.log(err);
        res.status(500).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.resetPassword = function (req, res) {
    res.send('Reset Password API');
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.updateProfile = function (req, res) {
    res.send('Update Profile API');
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.changeStudentPassword = function (req, res) {
    let data = req.body;
    studentService.changePassword(data).then((result) => {
        res.status(200).send("Password Updated Successfully");
    }).catch((err) => {
        console.error(err);
        res.status(500).send(err);
    });
};


/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.changeInstructorPassword = function (req, res) {
    let instructorId = req.body.instructorId;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let currentPassword = bcrypt.hashSync(oldPassword, 10);
    instructorService.getById(instructorId).then((instructor) => {
        if (!instructor) {
            res.status(404).send("Not Found");
        } else if (!bcrypt.compareSync(currentPassword, instructor.passWord)) {
            res.status(401).send("Current Password does not match with the Old password");
        } else {
            newPassword = bcrypt.hashSync(newPassword, 10);
            instructorService.changePassword(instructorId, newPassword).then((result) => {
                res.status(200).send("Password Updated Successfully");
            }).catch((err) => {
                res.status(501).send("Internal Server Error");
            });
        }
    });
};