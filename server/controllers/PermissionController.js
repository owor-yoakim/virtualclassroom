const userService = require('../services/user-service');

/* PermissionController Actions */
module.exports.getAll = function (req, res) {
    userService.getAllPermissions().then(permissions => {
        res.status(200).json(permissions);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}


module.exports.getById = function (req, res) {
    let id = req.params.permissionId;
    userService.getPermissionById(id).then(permission => {
        res.status(200).json(permission);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.getBySlug = function (req, res) {
    let id = req.params.slug;
    userService.getPermissionBySlug(slug).then(permission => {
        res.status(200).json(permission);
    }).catch(err => {
        res.status(500).send('Internal Server Error: ' + err);
    });
}

module.exports.createPermission = function (req, res) {
    let newPermission = req.body;
    userService.createPermission(newPermission).then(permission => {
        res.status(200).json(permission);
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    })
}

module.exports.updatePermission = function (req, res) {
    let newPermission = req.body;
    let id = req.params.permissionId;
    userService.updatePermission(id, newPermission).then(permission => {
        res.status(200).json(permission);
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    })
}

module.exports.deletePermission = function (req, res) {
    let id = req.params.permissionId;
    userService.deletePermission(id).then(permission => {
        res.status(200).send('Delete Successful');
    }).catch(err => {
        console.error(err);
        res.status(500).send('Internal Server Error: ' + err);
    });
}