var studentService = require('../services/student-service');

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getAllStudents = function (req, res) {
    studentService.getAll().then((students) => {
        res.status(200).send(students);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getById = function (req, res) {
    let id = req.params.studentId;
    studentService.getById(id).then((student) => {
        res.status(200).json(student);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getByEmail = function (req, res) {
    let email = req.params.email;
    studentService.getByEmail(email).then((student) => {
        res.status(200).json(student);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.getByEmailOrUsername = function (req, res) {
    let email = req.params.email;
    //let username = req.params.username;
    studentService.getByEmailOrUsername(email).then((student) => {
        res.status(200).json(student);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.activateOrDeactivateStudent = function (req, res) {
    let id = req.params.studentId;
    studentService.activateOrDeactivateStudent(studentId).then((student) => {
        res.status(200).json(student);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
};

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.createStudent = function (req, res) {
    let data = req.body;
    studentService.createStudent(data).then((result) => {
        res.status(200).send(result);
    }).catch((err) => {
        res.status(500).send(err);
    });
}

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.updateStudent = function (req, res) {
    let studentId = req.params.studentId;
    let data = req.body;
    studentService.updateStudent(studentId, data).then((result) => {
        res.status(200).send(result);
    }).catch((err) => {
        res.status(500).send(err);
    });
}

/**
 * 
 * @param {Request} req 
 * @param {Response} res 
 */
module.exports.deleteStudent = function (req, res) {
    let studentId = req.params.studentId;
    studentService.delete(studentId).then((result) => {
        res.status(200).send(result);
    }).catch((err) => {
        console.error(err);
        res.status(501).send(err);
    });
}