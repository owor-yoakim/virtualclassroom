var config = require('../config/config');
const Chat = require('../config/database').Chat;
const Client = require('../config/database').Client;
const Page = require('../config/database').Page;

function create(data) {

}

function update(id, data) {

}

function getBySlug(slug) {
  return new Promise((resolve, reject) => {
    Page.findOne({
      where: {
        slug: slug
      }
    }).then((page) => {
      resolve(page);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

function publish(pageId) {

}

function unpublish(pageId) {

}

function deletePage(pageId) {

}

module.exports.create = create;
module.exports.update = update;
module.exports.getBySlug = getBySlug;
module.exports.publish = publish;
module.exports.unpublish = unpublish;
module.exports.deletePage = deletePage;