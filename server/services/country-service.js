var config = require('../config/config');
const Country = require('../config/database').Country;

function getById(id) {
  return new Promise((resolve, reject) => {
    Country.findById(id).then((country) => {
      resolve(country);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function getByCode(code) {
  return new Promise((resolve, reject) => {
    Country.findOne({
      where: {
        code: code
      }
    }).then((country) => {
      resolve(country);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

function getAll() {
  return new Promise((resolve, reject) => {
    Country.findAll().then((countries) => {
      resolve(countries);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

module.exports.getById = getById;
module.exports.getByCode = getByCode;
module.exports.getAll = getAll;