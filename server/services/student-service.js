const Student = require('../config/database').Student;
const Enrolment = require('../config/database').Enrolment;
const Address = require('../config/database').Address;
const Country = require('../config/database').Country;

const bcrypt = require('bcrypt');
const config = require('../config/config');
const restrict = require('../config/restrict');

function getById(id) {
  return new Promise((resolve, reject) => {
    Student.findById(id, {
      include: [{
        model: Address,
        include: [{
          model: Country
        }]
      }]
    }).then((student) => {
      // delete password
      //console.log(student);
      student = restrict.removeFields(student.dataValues, ['passWord']);
      //console.log(student);
      resolve(student);
    }).catch((err) => {
      console.log(err);
      reject(err);
    });
  });
}

function getByEmail(email) {
  return new Promise((resolve, reject) => {
    Student.findOne({
      where: {
        emailAddress: email
      },
      include: [{
        model: Address,
        include: [{
          model: Country
        }]
      }]
    }).then((student) => {
      //console.log(student);
      // delete password
      if (student) {
        student = restrict.removeFields(student.dataValues, ['passWord']);
      }
      resolve(student);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}


function getAll() {
  return new Promise((resolve, reject) => {
    Student.findAll({
      include: [{
        model: Address,
        include: [{
          model: Country
        }]
      }]
    }).then((students) => {
      // console.log(students);
      // remove students  passwords
      students = restrict.removeFieldsFromDataItems(students, ['passWord']);
      resolve(students);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function createStudent(data) {
  return new Promise((resolve, reject) => {
    console.log('New Data', data);

    let address = null;
    //create address
    Address.create(data.address)
      .then((addr) => {
        address = addr.dataValues;
        address.country = addr.getCountry();
        console.log('New Address', address);
        delete data.address;
        data.addressId = address.id;
        data.passWord = bcrypt.hashSync(data.password, 10);
        delete data.password;
        data.joinDate = data.joinDate || (new Date).toISOString();
        // create new student object
        return Student.create(data);
      })
      .then((student) => {
        student = restrict.removeFields(student.dataValues, ['passWord']);
        student.address = address;
        student.addressId = address.id;
        console.log('New Student', student);
        resolve(student);
      })
      .catch((err) => {
        console.error(err);
        reject(err.name + ': ' + err.message);
      });
  });
}


function updateStudent(id, data) {
  return new Promise((resolve, reject) => {
    console.log('New Data', data);
    let address = null;

    if (data.password !== null) {
      data.passWord = bcrypt.hashSync(data.password, 10);
      delete data.password;
    } else {
      delete data.password;
    }

    if (data.addressId === null) {
      Address.create(data.address)
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          console.log('New Address', address);
          data.addressId = address.id;
          return Student.findById(id);
        })
        .then((student) => {
          return student.setAddress(address.id);
        })
        .then((student) => {
          delete data.addressId;
          delete data.address;
          return student.update(data);
        })
        .then((student) => {
          student =
            restrict.removeFields(student.dataValues, ['passWord']);
          student.address = address;
          student.addressId = address.id;
          console.log('Updated Student', student);
          resolve(student);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    } else {
      Address.findById(data.addressId, {
          include: [{
            model: Country
          }]
        })
        .then((addr) => {
          return addr.update(data.address);
        })
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          console.log('Updated Address', address);
          return Student.findById(id);
        })
        .then((student) => {
          delete data.addressId;
          delete data.address;
          return student.update(data);
        })
        .then((student) => {
          student =
            restrict.removeFields(student.dataValues, ['passWord']);
          student.address = address;
          student.addressId = address.id;
          console.log('Updated Student', student);
          resolve(student);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    }
  });
}

function tryAuthenticate(loginName, password) {
  return new Promise((resolve, reject) => {
    getByEmailOrUsername(loginName).then((student) => {
      if (student !== null && bcrypt.compareSync(password, student.passWord)) {
        // valid student
        // delete password
        student = restrict.removeFields(student.dataValues, ['passWord']);
        resolve(student);
      } else {
        // authentication failed
        resolve(null);
      }
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}


function getByEmailOrUsername(value) {
  return new Promise((resolve, reject) => {
    Student.findOne({
      where: {
        $or: [{
            emailAddress: value
          },
          {
            username: value
          }
        ]
      },
      include: [{
        model: Address,
        include: [{
          model: Country
        }]
      }]
    }).then((student) => {
      resolve(student);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}


function activateOrDeactivateStudent(studentId) {
  return new Promise((resolve, reject) => {
    Student.findById(studentId).then((student) => {
      return student.update({
        active: !student.active
      });
    }).then((student) => {
      resolve(student);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}

function changePassword(data) {
  return new Promise((resolve, reject) => {
    let studentId = data.studentId;
    let oldPassword = data.oldPassword;
    let newPassword = data.newPassword;
    Student.findById(studentId).then((student) => {
      if (!student) {
        reject("Student Not Found");
      } else if (!bcrypt.compareSync(oldPassword, student.passWord)) {
        reject("Wrong Old password given");
      } else {
        return student.update({
          passWord: bcrypt.hashSync(newPassword, 10)
        });
      }
    }).then((student) => {
      resolve(student);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}

function deleteStudent(id) {
  return new Promise((resolve, reject) => {
    Student.findById(id)
      .then((student) => {
        return student.update({
          active: false
        });
      })
      .then((student) => {
        return student.destroy();
      })
      .then((data) => {
        resolve(data);
      }).catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}


module.exports.getById = getById;
module.exports.getByEmail = getByEmail;
module.exports.getAll = getAll;
module.exports.createStudent = createStudent;
module.exports.updateStudent = updateStudent;
module.exports.tryAuthenticate = tryAuthenticate;
module.exports.getByEmailOrUsername = getByEmailOrUsername;
module.exports.activateOrDeactivateStudent = activateOrDeactivateStudent;
module.exports.deleteStudent = deleteStudent;
module.exports.changePassword = changePassword;