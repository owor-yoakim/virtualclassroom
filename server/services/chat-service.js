const config = require('../config/config');
const restrict = require('../config/restrict');
const Chat = require('../config/database').Chat;
const Student = require('../config/database').Student;
const Instructor = require('../config/database').Instructor;
const Lesson = require('../config/database').Lesson;

function getChatById(chatId) {
  return new Promise((resolve, reject) => {
    Chat.findById(chatId).then((chat) => {
      resolve(chat);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

function getLessonChats(lessonId) {
  return new Promise((resolve, reject) => {
    Chat.findAll({
      where: {
        lessonId: lessonId
      },
      include: [
        { model: Student },
        { model: Instructor }
      ],
      limit: 20,
      order: [
        ['createdAt', 'DESC']
      ]
    }).then((chats) => {
      resolve(chats);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

function getAllChats() {
  return new Promise((resolve, reject) => {
    Chat.findAll({
      order: [
        ['createdAt', 'DESC']
      ]
    }).then((chats) => {
      //console.log(chats);
      resolve(chats);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

function createChat(chat) {
  return new Promise((resolve, reject) => {
    //console.log(chat);
    //create new chat 
    Chat.create(chat).then((newChat) => {
      //console.log(newChat);
      return Chat.findById(newChat.id,{
        include: [
          { model: Student },
          { model: Instructor }
        ],
        limit: 20,
        order: [
          ['createdAt', 'DESC']
        ]
      });
    }).then((newChat) => {
        if (newChat.instructor) {
          newChat = restrict.removeFields(newChat.dataValues, ['instructor.password']);
        }else if (newChat.student) {
          newChat = restrict.removeFields(newChat.dataValues, ['student.passWord']);
        }
        resolve(newChat);
    }).catch((err) => {
      console.error(err);
      reject(err.name + ': ' + err.message);
    });
  });
}

module.exports.getChatById = getChatById;
module.exports.getLessonChats = getLessonChats;
module.exports.getAllChats = getAllChats;
module.exports.createChat = createChat;
