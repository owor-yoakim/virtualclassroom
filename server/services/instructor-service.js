const bcrypt = require('bcrypt');
const config = require('../config/config');
const restrict = require('../config/restrict');
const Instructor = require('../config/database').Instructor;
const InstructorLesson = require('../config/database').InstructorLesson;
const Lesson = require('../config/database').Lesson;
const Subject = require('../config/database').Subject;
const Address = require('../config/database').Address;
const Country = require('../config/database').Country;

module.exports.getInstructorsBySubjectSlug = function (slug) {
  return new Promise((resolve, reject) => {
    Instructor
      .findAll({
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }, {
          model: Lesson,
          through: {
            attributes: []
          },
          include: [{
            model: Subject
            // where: {slug: slug},
            // required: true
          }]
        }]
      })
      .then((instructors) => {
        instructors =
          restrict.removeFieldsFromDataItems(instructors, ['password']);
        // console.log(instructors);
        let requiredInstructors = [];
        for (var i = 0, len1 = instructors.length; i < len1; i++) {
          // let lesson = lessons[i];
          let lessons = instructors[i].lessons;
          if (lessons
            .filter((x) => {
              return x.subject.slug === slug;
            })
            .length > 0) {
            requiredInstructors.push(instructors[i]);
          }
        }
        // return instructors;
        console.log('requiredInstructors', requiredInstructors);
        resolve(requiredInstructors);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

module.exports.getByEmail = function getByEmail(email) {
  return new Promise((resolve, reject) => {
    Instructor
      .findOne({
        where: {
          emailAddress: email
        },
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((instructor) => {
        // console.log(instructor);
        // remove instructor password
        instructor =
          restrict.removeFields(instructor.dataValues, ['password']);
        resolve(instructor);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};


module.exports.getAll = function () {
  return new Promise((resolve, reject) => {
    Instructor
      .findAll({
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }, {
          model: Lesson,
          through: {
            attributes: []
          },
          include: [{
            model: Subject
          }]
        }]
      })
      .then((instructors) => {
        instructors =
          restrict.removeFieldsFromDataItems(instructors, ['password']);
        resolve(instructors);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};


module.exports.getById = function (id) {
  return new Promise((resolve, reject) => {
    Instructor.findById(id, {
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((instructor) => {
        instructor =
          restrict.removeFields(instructor.dataValues, ['password']);
        resolve(instructor);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

module.exports.createInstructor = function (data) {
  return new Promise((resolve, reject) => {
    let hash = bcrypt.hashSync(data.password, 10);
    data.password = hash;
    let address = null;
    Address.create(data.address)
      .then((addr) => {
        address = addr.dataValues;
        address.country = addr.getCountry();
        // console.log('Address', address);
        data.addressId = address.id;
        delete data.address;
        return Instructor.create(data);
      })
      .then((instructor) => {
        instructor =
          restrict.removeFields(instructor.dataValues, ['password']);
        instructor.address = address;
        instructor.addressId = address.id;
        // console.log('New Instructor', instructor);
        resolve(instructor);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};


module.exports.updateInstructor = function (id, data) {
  return new Promise((resolve, reject) => {
    // console.log('New Data', data);
    let address = null;
    if (data.password !== null) {
      data.password = bcrypt.hashSync(data.password, 10);
    } else {
      delete data.password;
    }
    if (data.addressId === null) {
      Address.create(data.address)
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          // console.log('Address', address);
          data.addressId = address.id;
          return Instructor.findById(id);
        })
        .then((instructor) => {
          return instructor.setAddress(address.id);
        })
        .then((instructor) => {
          delete data.addressId;
          delete data.address;
          return instructor.update(data);
        })
        .then((instructor) => {
          instructor =
            restrict.removeFields(instructor.dataValues, ['password']);
          instructor.address = address;
          // console.log(instructor);
          resolve(instructor);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    } else {
      Address.findById(data.addressId, {
          include: [{
            model: Country
          }]
        })
        .then((addr) => {
          return addr.update(data.address);
        })
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          // console.log('Updated Address', address);
          return Instructor.findById(id);
        })
        .then((instructor) => {
          delete data.addressId;
          delete data.address;
          return instructor.update(data);
        })
        .then((instructor) => {
          instructor =
            restrict.removeFields(instructor.dataValues, ['password']);
          instructor.address = address;
          // console.log('Updated Instructor', instructor);
          resolve(instructor);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    }
  });
};

module.exports.deleteInstructor = function (id) {
  return new Promise((resolve, reject) => {
    Instructor.findById(id)
      .then((instructor) => {
        return instructor.update({
          active: false
        });
      })
      .then((instructor) => {
        return instructor.destroy();
      })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

module.exports.assignLesson = function (instructorId, lessonId) {
  return new Promise((resolve, reject) => {
    InstructorLesson.findOne({
      where: {
        instructorId: instructorId,
        lessonId: lessonId
      },
      paranoid: false
    }).then((instructorLesson) => {
      if (instructorLesson) {
        instructorLesson.restore()
          .then((instructorLesson) => {
            return instructorLesson.update({
              active: true
            });
          })
          .then((instructorLesson) => {
            resolve(instructorLesson);
          }).catch((err) => {
            console.error(err);
            reject(err);
          });
      } else {
        Instructor.findById(instructorId)
          .then((instructor) => {
            return instructor.addLesson(lessonId);
          })
          .then((result) => {
            resolve(result)
          })
          .catch(err => {
            console.error(err);
            reject(err);
          });
      }
    })

  });
};

module.exports.removeLesson = function (instructorId, lessonId) {
  return new Promise((resolve, reject) => {
    InstructorLesson.findOne({
        where: {
          instructorId: instructorId,
          lessonId: lessonId
        }
      })
      .then((instructorLesson) => {
        return instructorLesson.update({
          active: false
        })
      })
      .then((instructorLesson) => {
        return Instructor.findById(instructorId);
      })
      .then((instructor) => {
        return instructor.removeLesson(lessonId);
      })
      .then((result) => {
        resolve(result);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

module.exports.activateOrDeactivateInstructor = function (id) {
  return new Promise((resolve, reject) => {
    Instructor.findById(id)
      .then((instructor) => {
        return instructor.update({
          active: !instructor.active
        });
      })
      .then((instructor) => {
        instructor =
          restrict.removeFields(instructor.dataValues, ['password']);
        resolve(instructor)
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

module.exports.getInstructorLessons = function (id) {
  return new Promise((resolve, reject) => {
    Instructor
      .findById(id, {
        include: [{
          model: Lesson,
          through: {
            attributes: []
          },
          include: [{
            model: Subject
          }]
        }]
      })
      .then((instructor) => {
        return instructor.lessons;
      })
      .then((instructorLessons) => {
        resolve(instructorLessons);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

module.exports.getInstructorSchedules = function (instructorId) {
  return new Promise((resolve, reject) => {
    Instructor.findById(instructorId)
      .then((instructor) => {
        return instructor.getInstructorSchedules({
          include: [{
            model: Lesson
          }]
        });
      })
      .then((instructorSchedules) => {
        resolve(instructorSchedules);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
};

module.exports.tryAuthenticate = function (loginName, password) {
  return new Promise((resolve, reject) => {
    getByEmailOrUsername(loginName)
      .then((instructor) => {
        if (instructor !== null &&
          bcrypt.compareSync(password, instructor.password)) {
          // valid instructor
          // delete password
          instructor =
            restrict.removeFields(instructor.dataValues, ['password']);
          resolve(instructor);
        } else {
          // authentication failed
          resolve(null);
        }
      })
      .catch((err) => {
        console.error(err);
        reject(err.name + ': ' + err.message);
      });
  });
}

module.exports.changePassword = function (instructorId, password) {
  return new Promise((resolve, reject) => {
    Instructor.findById(instructorId)
      .then((instructor) => {
        return instructor.update({
          passWord: password
        });
      })
      .then((instructor) => {
        resolve(instructor)
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}


function getByEmailOrUsername(value) {
  return new Promise((resolve, reject) => {
    Instructor.findOne({
        where: {
          $or: [{
            emailAddress: value
          }, {
            username: value
          }]
        },
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((instructor) => {
        //console.log(instructor);
        resolve(instructor);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}