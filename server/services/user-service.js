const bcrypt = require('bcrypt');
const config = require('../config/config');
const restrict = require('../config/restrict');
const User = require('../config/database').User;
const Role = require('../config/database').Role;
const Permission = require('../config/database').Permission;
const Address = require('../config/database').Address;
const Country = require('../config/database').Country;

/*  User Actions    */
function authenticate(email, password) {
  //console.log("Email: ", email);
  return new Promise((resolve, reject) => {
    getUserByEmail(email)
      .then((user) => {
        if (user !== null && bcrypt.compareSync(password, user.password)) {
          // authentication successful
          //clear password
          user = restrict.removeFields(user.dataValues, ['password']);
          //user.password = null;
          resolve(user);
        } else {
          // authentication failed
          resolve(null);
        }
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}

function getUserById(id) {
  return new Promise((resolve, reject) => {
    User.findById(id, {
        include: [{
          model: Role
        }, {
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((user) => {
        //console.log(user);
        //clear password
        //user.password = null;
        user = restrict.removeFields(user.dataValues, ['password']);
        resolve(user);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
};

function getUserByEmail(email) {
  return new Promise((resolve, reject) => {
    User.findOne({
        where: {
          emailAddress: email
        },
        include: [{
          model: Address,
          include: [{
            model: Country
          }]
        }, {
          model: Role,
          include: [{
            model: Permission,
            through: {
              attributes: []
            }
          }]
        }]
      })
      .then((user) => {
        resolve(user);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
};


function getUserByEmailOrUsername(email, username) {
  return new Promise((resolve, reject) => {
    User.findOne({
        where: {
          $or: [{
              emailAddress: email
            },
            {
              username: username
            }
          ]
        }
      })
      .then((user) => {
        resolve(user);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

function getUsersByRole(roleId) {
  return new Promise((resolve, reject) => {
    User.findAll({
        where: {
          roleId: roleId
        },
        include: [{
          model: Role
        }, {
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((users) => {
        resolve(users);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
};

function getAllUsers() {
  return new Promise((resolve, reject) => {
    User.findAll({
        include: [{
          model: Role
        }, {
          model: Address,
          include: [{
            model: Country
          }]
        }]
      })
      .then((users) => {
        resolve(users);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
};


function createUser(userData) {
  return new Promise((resolve, reject) => {
    console.log("New Data: ", userData);
    let address = null;
    // create new Address
    Address.create(userData.address)
      .then((addr) => {
        address = addr.dataValues;
        address.country = addr.getCountry();
        console.log('New Address', address);
        delete userData.address;
        userData.addressId = address.id;
        userData.emailAddress = userData.emailAddress.toLowerCase();
        userData.password = bcrypt.hashSync(userData.password, 10);
        //create new user object
        return User.create(userData);
      })
      .then((user) => {
        user =
          restrict.removeFields(user.dataValues, ['password']);
        user.addressId = address.id;
        user.address = address;
        console.log('New User', user);
        resolve(user);
      })
      .catch((err) => {
        console.error(err);
        reject(err.name + ': ' + err.message);
      });
  });
}

function updateUser(id, data) {
  return new Promise((resolve, reject) => {
    // console.log('New Data', data);
    let address = null;
    if (data.password !== null) {
      data.password = bcrypt.hashSync(data.password, 10);
    } else {
      delete data.password;
    }
    if (data.addressId === null) {
      Address.create(data.address)
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          console.log('New Address', address);
          data.addressId = address.id;
          return User.findById(id);
        })
        .then((user) => {
          return user.setAddress(address.id);
        })
        .then((user) => {
          delete data.addressId;
          delete data.address;
          return user.update(data);
        })
        .then((user) => {
          user =
            restrict.removeFields(user.dataValues, ['password']);
          user.address = address;
          console.log('Updated User', user);
          resolve(user);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    } else {
      Address.findById(data.addressId, {
          include: [{
            model: Country
          }]
        })
        .then((addr) => {
          return addr.update(data.address);
        })
        .then((addr) => {
          address = addr.dataValues;
          address.country = addr.getCountry();
          // console.log('Updated Address', address);
          return User.findById(id);
        })
        .then((user) => {
          delete data.addressId;
          delete data.address;
          return user.update(data);
        })
        .then((user) => {
          user =
            restrict.removeFields(user.dataValues, ['password']);
          user.address = address;
          console.log('Updated User', user);
          resolve(user);
        })
        .catch((err) => {
          console.error(err);
          reject(err);
        });
    }
  });
}

function deleteUser(id) {
  return new Promise((resolve, reject) => {
    User.findById(id)
      .then((user) => {
        return user.update({
          active: false
        });
      })
      .then((user) => {
        return user.destroy();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}

function activateOrDeactivateUser(id) {
  return new Promise((resolve, reject) => {
    User.findById(id)
      .then((user) => {
        return user.update({
          active: !user.active
        });
      })
      .then((data) => {
        resolve(data);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}

function changePassword(data) {
  return new Promise((resolve, reject) => {
    let userId = data.userId;
    let oldPassword = data.oldPassword;
    let newPassword = data.newPassword;
    User.findById(userId)
      .then((user) => {
        if (!user) {
          reject("User Not Found");
        } else if (!bcrypt.compareSync(oldPassword, user.password)) {
          reject("Wrong Old password given");
        } else {
          return user.update({
            password: bcrypt.hashSync(newPassword, 10)
          });
        }
      })
      .then((user) => {
        resolve(user);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}


/*   Role Actions */
function getAllRoles() {
  return new Promise((resolve, reject) => {
    Role.findAll()
      .then((roles) => {
        resolve(roles);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function createRole(data) {
  let newRole = data.role;
  let permissions = data.permissions;
  return new Promise((resolve, reject) => {
    Role.create(newRole)
      .then((role) => {
        return role.setPermissions(permissions);
      })
      .then((role) => {
        resolve(role);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function updateRole(id, data) {
  let roleData = data.role;
  delete roleData.permissions;
  let permissions = data.permissions;
  return new Promise((resolve, reject) => {
    Role.findById(id)
      .then((role) => {
        return role.update(roleData);
      })
      .then((role) => {
        return role.removePermissions();
      })
      .then((role) => {
        return role.setPermissions(permissions);
      })
      .then((role) => {
        resolve(role);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}

function getRoleById(id) {
  return new Promise((resolve, reject) => {
    Role.findById(id, {
        include: [{
          model: Permission
        }]
      })
      .then((role) => {
        resolve(role);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function getRoleBySlug(slug) {
  return new Promise((resolve, reject) => {
    Role.findOne({
        where: {
          slug: slug
        }
      })
      .then((role) => {
        resolve(role);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function deleteRole(id) {
  return new Promise((resolve, reject) => {
    Role.findById(id)
      .then((role) => {
        return role.update({
          active: false
        });
      })
      .then((role) => {
        return role.destroy();
      })
      .then((role) => {
        console.log("Deleted: ", role);
        resolve(role);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}


/*   Permission Actions */
function getAllPermissions() {
  return new Promise((resolve, reject) => {
    Permission.findAll()
      .then((permissions) => {
        resolve(permissions);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function createPermission(data) {
  return new Promise((resolve, reject) => {
    Permission.create(data)
      .then((permission) => {
        resolve(permission);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function updatePermission(id, data) {
  return new Promise((resolve, reject) => {
    Permission.findById(id)
      .then((permission) => {
        return permission.update(data);
      })
      .then((newPermission) => {
        resolve(newPermission);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}

function getPermissionById(id) {
  return new Promise((resolve, reject) => {
    Permission.findById(id)
      .then((permission) => {
        resolve(permission);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function getPermissionBySlug(slug) {
  return new Promise((resolve, reject) => {
    Permission.findOne({
        where: {
          slug: slug
        }
      })
      .then((permission) => {
        resolve(permission);
      })
      .catch((err) => {
        reject(err.name + ': ' + err.message);
      })
  });
}

function deletePermission(id) {
  return new Promise((resolve, reject) => {
    Permission.findById(id)
      .then((permission) => {
        return permission.destroy();
      }).then((data) => {
        //console.log("Delete result: ", data);
        resolve(data);
      }).catch((err) => {
        reject(err.name + ': ' + err.message);
      });
  });
}



// users
module.exports.authenticate = authenticate;
module.exports.getAllUsers = getAllUsers;
module.exports.getUsersByRole = getUsersByRole;
module.exports.getUserById = getUserById;
module.exports.getUserByEmail = getUserByEmail;
module.exports.getUserByEmailOrUsername = getUserByEmailOrUsername;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
module.exports.activateOrDeactivateUser = activateOrDeactivateUser;
module.exports.changePassword = changePassword;
// permissions
module.exports.getAllPermissions = getAllPermissions;
module.exports.getPermissionById = getPermissionById;
module.exports.getPermissionBySlug = getPermissionBySlug;
module.exports.createPermission = createPermission;
module.exports.updatePermission = updatePermission;
module.exports.deletePermission = deletePermission;
// roles
module.exports.getAllRoles = getAllRoles;
module.exports.getRoleById = getRoleById;
module.exports.getRoleBySlug = getRoleBySlug;
module.exports.createRole = createRole;
module.exports.updateRole = updateRole;
module.exports.deleteRole = deleteRole;