const config = require('../config/config');
const restrict = require('../config/restrict');
const Subject = require('../config/database').Subject;
const User = require('../config/database').User;
const Lesson = require('../config/database').Lesson;
const Instructor = require('../config/database').Instructor;
const LessonModule = require('../config/database').LessonModule;

// module.exports.findById = findById;
// module.exports.findBySlug = findBySlug;
// module.exports.findAll = findAll;
// module.exports.create = create;
// module.exports.update = update;
// module.exports.delete = remove;
// module.exports.numberOfSubjects = numberOfSubjects;
// module.exports.getInstructorsBySubjectSlug = getInstructorsBySubjectSlug;

module.exports.findById = function (id) {
    return new Promise((resolve, reject) => {
        Subject.findById(id, {
            include: [{
                model: Lesson,
                include: [{
                    model: Instructor
                }, {
                    model: LessonModule,
                    as: 'lessonModules'
                }]
            }]
        }).then((subject) => {
            //console.log(subject.lessons);
            resolve(subject);
        }).catch((err) => {
            console.error(err);
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.findBySlug = function (slug) {
    return new Promise((resolve, reject) => {
        Subject.findOne({
            where: {
                slug: slug
            },
            include: [{
                model: Lesson,
                include: [{
                    model: Instructor
                }, {
                    model: LessonModule,
                    as: 'lessonModules'
                }]
            }]
        }).then((subject) => {
            resolve(subject);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.findAll = function () {
    return new Promise((resolve, reject) => {
        Subject.findAll({
            include: [{
                model: Lesson,
                include: [{
                    model: Instructor
                }, {
                    model: LessonModule,
                    as: 'lessonModules'
                }]
            }]
        }).then((subjects) => {
            resolve(subjects);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.create = function (data) {
    return new Promise((resolve, reject) => {
        Subject.create({
            title: data.title,
            slug: data.slug,
            description: data.description,
            userId: data.userId
        }).then((subject) => {
            resolve(subject);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.update = function (id, data) {
    return new Promise((resolve, reject) => {
        Subject.findById(id).then((subject) => {
            subject.update({
                title: data.title,
                slug: data.slug,
                description: data.description,
                userId: data.userId,
                createdAt: data.createdAt,
                updatedAt: data.updatedAt
            }).then((subject) => {
                resolve(subject);
            }).catch(err => {
                //failed to update
                console.error(err);
                reject(err.name + ': ' + err.message);
            });
        }).catch((err) => {
            //server error
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.delete = function (id) {
    return new Promise((resolve, reject) => {
        Subject.findById(id).then((subject) => {
            subject.destroy().then((subject) => {
                resolve(subject);
            }).catch((err) => {
                reject(err.name + ': ' + err.message);
            });
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.numberOfSubjects = function () {
    return new Promise((resolve, reject) => {
        Subject.findAndCountAll().then(data => {
            let num = data.count;
            //console.log("Subjects: ", num);
            resolve(num);
        }).catch(err => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.getInstructorsBySubjectSlug = function (slug) {
    return new Promise((resolve, reject) => {
        Subject.findOne({
            where: {
                slug: slug
            },
            include: [{
                model: Lesson,
                include: [{
                    model: Instructor,
                    through: {
                        attributes: []
                    }
                }]
            }]
        }).then((subject) => {
            let instructors = [];
            //console.log(subject);
            let lessons = subject.lessons;
            delete subject.lessons;
            //console.log(subject);
            for (var i = 0, len1 = lessons.length; i < len1; i++) {
                let lesson = lessons[i];
                lesson.subject = subject;
                for (var j = 0, len2 = lesson.instructors.length; j < len2; j++) {
                    if (instructors.find((x) => {
                            return x.id === lesson.instructors[j].id;
                        }) === undefined) {
                        //delete password
                        //delete instructor["password"];
                        let data = restrict.removeFields(lesson.instructors[j].dataValues, ['password']);
                        //console.log(data);
                        let instructor = data;
                        instructor.lessons = [];
                        instructor.lessons.push(lesson);
                        instructors.push(instructor);
                    } else {
                        instructors[index].lessons.push(lesson);
                    }
                }
            }
            return instructors;
        }).then((instructors) => {
            console.log(instructors);
            resolve(instructors);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}