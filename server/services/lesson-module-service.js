const LessonModule = require('../config/database').LessonModule;
const Lesson = require('../config/database').Lesson;
const LessonFile = require('../config/database').LessonFile;
const LessonVideo = require('../config/database').LessonVideo;
const Subject = require('../config/database').Subject;
const Instructor = require('../config/database').Instructor;

function findById(id) {
    return new Promise((resolve, reject) => {
        LessonModule.findById(id, {
            include: [{
                model: Lesson,
                include: [{
                    model: Subject,
                    model: Instructor
                }]
            }, {
                model: LessonFile,
                as: 'lessonModuleFiles'
            }, {
                model: LessonVideo,
                as: 'lessonModuleVideos'
            }]
        }).then((lessonModule) => {
            resolve(lessonModule);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function findByLesson(lessonId) {
    return new Promise((resolve, reject) => {
        LessonModule.findAll({
            where: {
                lessonId: lessonId
            },
            include: [{
                model: Lesson,
                include: [{
                    model: Subject,
                    model: Instructor
                }]
            }]
        }).then((lessonModules) => {
            resolve(lessonModules);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function findByInstructor(instructorId) {
    return new Promise((resolve, reject) => {
        LessonModule.findAll({
            where: {
                instructorId: instructorId
            }
        }).then((lessonModules) => {
            resolve(lessonModules);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function findAll() {
    return new Promise((resolve, reject) => {
        LessonModule.findAll({
            include: [{
                model: Lesson,
                include: [{
                    model: Subject,
                    model: Instructor
                }]
            }]
        }).then((lessonModules) => {
            resolve(lessonModules);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function create(data) {
    return new Promise((resolve, reject) => {
        LessonModule.create({
            title: data.title,
            description: data.description,
            scheduleDate: data.scheduleDate,
            startTime: data.startTime,
            duration: data.duration,
            lessonId: data.lessonId,
            userId: data.userId
        }).then((lessonModule) => {
            resolve(lessonModule);
        }).catch((err) => {
            console.error('Error: ', err);
            reject(err.name + ': ' + err.message);
        });
    });
}


function update(id, data) {
    return new Promise((resolve, reject) => {
        LessonModule.findById(id).then((lessonModule) => {
            console.log(lessonModule);
            return lessonModule.update({
                title: data.title,
                description: data.description,
                scheduleDate: data.scheduleDate,
                startTime: data.startTime,
                duration: data.duration,
                userId: data.userId
            });
        }).then((lessonModule) => {
            resolve(lessonModule);
        }).catch((err) => {
            //server error
            console.error(err);
            reject(err.name + ': ' + err.message);
        });
    });
}

function remove(id) {
    return new Promise((resolve, reject) => {
        LessonModule.findById(id).then((lessonModule) => {
            return lessonModule.destroy();
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

module.exports.findById = findById;
module.exports.findByLesson = findByLesson;
module.exports.findByInstructor = findByInstructor;
module.exports.findAll = findAll;
module.exports.create = create;
module.exports.update = update;
module.exports.delete = remove;