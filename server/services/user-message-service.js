const UserMessage = require('../config/database').UserMessage;

function create(data) {
    return new Promise((resolve, reject) => {
        UserMessage.create(data).then((message) => {
            resolve((message));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function update(id, data) {
    return new Promise((resolve, reject) => {
        UserMessage.findById(id).then((message) => {
            return message.update(data);
        }).then((message) => {
            resolve(message);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function remove(id) {
    return new Promise((resolve, reject) => {
        UserMessage.findById(id).then((message) => {
            return message.destroy();
        }).then((data) => {
            resolve(data);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getAllMessages() {
    return new Promise((resolve, reject) => {
        UserMessage.findAll().then((messages) => {
            resolve((messages));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getById(id) {
    return new Promise((resolve, reject) => {
        UserMessage.findById(id).then((message) => {
            resolve((message));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getBySenderId(id) {
    return new Promise((resolve, reject) => {
        UserMessage.findAll({
            where: {
                senderId: id
            }
        }).then((messages) => {
            resolve((messages));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getBySenderEmail(email) {
    return new Promise((resolve, reject) => {
        UserMessage.findAll({
            where: {
                senderEmail: email
            }
        }).then((messages) => {
            resolve((messages));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getByReceiverId(id) {
    return new Promise((resolve, reject) => {
        UserMessage.findAll({
            where: {
                receiverId: id
            }
        }).then((messages) => {
            resolve((messages));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getByReceiverEmail(email) {
    return new Promise((resolve, reject) => {
        UserMessage.findAll({
            where: {
                receiverEmail: email
            }
        }).then((messages) => {
            resolve((messages));
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}



module.exports.createUserMessage = create;
module.exports.updateUserMessage = update;
module.exports.deleteUserMessage = remove;
module.exports.getAllMessages = getAllMessages;
module.exports.getById = getById;
module.exports.getBySenderId = getBySenderId;
module.exports.getBySenderEmail = getBySenderEmail;
module.exports.getByReceiverId = getByReceiverId;
module.exports.getByReceiverEmail = getByReceiverEmail;