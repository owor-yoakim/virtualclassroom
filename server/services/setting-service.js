const Setting = require('../config/database').Setting;

function findById(id) {
  return new Promise((resolve, reject) => {
    Setting.findById(id).then((setting) => {
      resolve(setting);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function findByKey(key) {
  return new Promise((resolve, reject) => {
    Setting.findOne({
      where: {
        key: key
      }
    }).then((setting) => {
      resolve(setting);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function findAll() {
  return new Promise((resolve, reject) => {
    Setting.findAll().then((settings) => {
      resolve(settings);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function create(data) {
  return new Promise((resolve, reject) => {
    Setting.create({
      key: data.key,
      value: data.value
    }).then((setting) => {
      resolve(setting);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

function update(id, data) {
  return new Promise((resolve, reject) => {
    findById(id).then((setting) => {
      //setting.key = data.key;
      //setting.value = data.value;
      //setting.update();
      resolve(setting);
    }).catch((err) => {
      reject(err.name + ': ' + err.message);
    });
  });
}

module.exports.findById = findById;
module.exports.findByKey = findByKey;
module.exports.findAll = findAll;
module.exports.create = create;
module.exports.update = update;