const config = require('../config/config');
const restrict = require('../config/restrict');
const Lesson = require('../config/database').Lesson;
const Subject = require('../config/database').Subject;
const Instructor = require('../config/database').Instructor;
const LessonModule = require('../config/database').LessonModule;
const LessonComment = require('../config/database').LessonComment;
const LessonFile = require('../config/database').LessonFile;

function findById(id) {
    return new Promise((resolve, reject) => {
        Lesson.findById(id, {
            include: [{
                model: Subject
            }]
        }).then((lesson) => {
            resolve(lesson);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getLessonInstructors(lessonId) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.getInstructors();
        }).then((instructors) => {
            console.log(instructors);
            instructors = restrict.removeFieldsFromDataItems(instructors, ['password']);
            resolve(instructors);
        }).catch((err) => {
            reject(err);
        });
    });
}

function getLessonModules(lessonId) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.getLessonModules();
        }).then((modules) => {
            //console.log(modules);
            resolve(modules);
        }).catch((err) => {
            //console.error(err);
            reject(err);
        });
    });
}

function findBySlug(slug) {
    return new Promise((resolve, reject) => {
        Lesson.findOne({
            where: {
                slug: slug
            },
            include: [{
                model: Subject
            }]
        }).then((lesson) => {
            resolve(lesson);
        }).catch((err) => {
            reject(err);
        });
    });
}

function findBySubject(subjectId) {
    return new Promise((resolve, reject) => {
        Lesson.findAll({
            where: {
                subjectId: subjectId
            },
            include: [{
                model: Subject
            }, {
                model: Instructor,
                through: {
                    attributes: []
                }
            }, {
                model: LessonModule,
                as: 'lessonModules'
            }]
        }).then((lessons) => {
            resolve(lessons);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function findAll() {
    return new Promise((resolve, reject) => {
        Lesson.findAll({
            include: [{
                model: Subject
            }]
        }).then((lessons) => {
            resolve(lessons);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function create(data) {
    return new Promise((resolve, reject) => {
        Lesson.create({
            title: data.title,
            slug: data.slug,
            description: data.description,
            outline: data.outline,
            outcome: data.outcome,
            subjectId: data.subjectId,
            userId: data.userId
        }).then((lesson) => {
            return lesson.setPrerequisites(data.prerequisites);
        }).then((data) => {
            resolve(data);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function update(id, data) {
    return new Promise((resolve, reject) => {
        let lessonObj = null;
        Lesson.findById(id).then(lesson => {
            lesson.update({
                title: data.title,
                slug: data.slug,
                description: data.description,
                outline: data.outline,
                outcome: data.outcome,
                subjectId: data.subjectId,
                userId: data.userId
            }).then((lesson) => {
                lessonObj = lesson;
                return lesson.removePrerequisites();
            }).then((result) => {
                return lessonObj.setPrerequisites(data.prerequisites);
            }).then((data) => {
                resolve(data);
            }).catch(err => {
                //failed to update
                console.error(err);
                reject(err.name + ': ' + err.message);
            });
        }).catch(err => {
            //server error
            reject(err.name + ': ' + err.message);
        });
    });
}

function updateInstructorLesson(id, data) {
    return new Promise((resolve, reject) => {
        let lessonObj = null;
        Lesson.findById(id).then(lesson => {
            lesson.update({
                title: data.title,
                slug: data.slug,
                description: data.description,
                outline: data.outline,
                outcome: data.outcome
            }).then((lesson) => {
                resolve(lesson);
            }).catch((err) => {
                //failed to update
                console.error(err);
                reject(err.name + ': ' + err.message);
            });
        }).catch(err => {
            //server error
            reject(err.name + ': ' + err.message);
        });
    });
}

function remove(lessonId) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.destroy();
        }).then((result) => {
            //console.log("Deleted: ", result);
            resolve(result);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function numberOfLessons(subjectId = 0) {
    return new Promise((resolve, reject) => {
        let options = {};
        if (subjectId > 0) {
            options.where = {
                subjectId: subjectId
            };
        }
        Lesson.findAndCountAll(options).then((data) => {
            let num = data.count;
            //console.log("Lessons: ", num);
            resolve(num);
        }).catch((err) => {
            reject(err.name + ': ' + err.message);
        });
    });
}

function getLessonEnrolments(lessonId) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.getEnrolments();
        }).then((enrolments) => {
            resolve(enrolments);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function addEnrolment(lessonId, enrolment) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.addEnrolment(enrolment);
        }).then((data) => {
            resolve(data);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function getLessonComments(lessonId) {
    return new Promise((resolve, reject) => {
        LessonComment.findAll({
            where: {
                lessonId: lessonId
            },
            include: [{
                model: Lesson
            }]
        }).then((lessonComments) => {
            resolve(lessonComments);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function getLessonModuleComments(lessonModuleId) {
    return new Promise((resolve, reject) => {
        LessonComment.findAll({
            where: {
                lessonModuleId: lessonModuleId
            }
        }).then((lessonModuleComments) => {
            resolve(lessonModuleComments);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function createLessonComment(lessonId, comment) {
    return new Promise((resolve, reject) => {
        LessonComment.create({
            lessonId: lessonId,
            lessonModuleId: comment.lessonModuleId || null,
            name: comment.name,
            email: comment.email,
            body: comment.body
        }).then((result) => {
            resolve(result);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function getLessonComment(lessonId, commentId) {
    return new Promise((resolve, reject) => {
        LessonComment.findById(commentId).then((lessonComment) => {
            resolve(lessonComment);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function updateLessonComment(lessonId, commentId, data) {
    return new Promise((resolve, reject) => {
        LessonComment.findById(commentId).then((lessonComment) => {
            return lessonComment.update({
                name: data.name,
                email: data.email,
                body: data.body
            });
        }).then((result) => {
            resolve(result);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function deleteLessonComment(lessonId, commentId) {
    return new Promise((resolve, reject) => {
        LessonComment.findById(commentId).then((lessonComment) => {
            return lessonComment.destroy();
        }).then((result) => {
            resolve(result);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function getPrerequisites(lessonId) {
    return new Promise((resolve, reject) => {
        Lesson.findById(lessonId).then((lesson) => {
            return lesson.getPrerequisites({
                includeIgnoreAttributes: false
            });
        }).then((prerequisites) => {
            //console.log(prerequisites);
            resolve(prerequisites);
        }).catch((err) => {
            console.error(err);
            reject(err);
        });
    });
}

function countBySubject(subjectId) {
    return numberOfLessons(subjectId);
}

function uploadFile(file) {
    return new Promise((resolve, reject) => {
        LessonFile.create(file)
            .then((lessonFile) => {
                // console.log(lessonFile);
                resolve(lessonFile);
            })
            .catch((err) => {
                console.error(err);
                reject(err);
            });
    });
}

module.exports.findById = findById;
module.exports.findBySlug = findBySlug;
module.exports.findBySubject = findBySubject;
module.exports.findAll = findAll;
module.exports.create = create;
module.exports.update = update;
module.exports.updateInstructorLesson = updateInstructorLesson;
module.exports.delete = remove;
module.exports.numberOfLessons = numberOfLessons;
module.exports.countBySubject = countBySubject;
module.exports.getLessonEnrolments = getLessonEnrolments;
module.exports.addEnrolment = addEnrolment;
module.exports.getLessonInstructors = getLessonInstructors;
module.exports.getLessonModules = getLessonModules;
module.exports.getPrerequisites = getPrerequisites;
module.exports.uploadFile = uploadFile;
module.exports.getLessonComments = getLessonComments;
module.exports.getLessonModuleComments = getLessonModuleComments;
module.exports.getLessonComment = getLessonComment;
module.exports.createLessonComment = createLessonComment;
module.exports.updateLessonComment = updateLessonComment;
module.exports.deleteLessonComment = deleteLessonComment;