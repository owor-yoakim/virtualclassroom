var config = require('../config/config');
const Enrolment = require('../config/database').Enrolment;
const Student = require('../config/database').Student;
const Lesson = require('../config/database').Lesson;

/**
 * @description Method to get a given enrolment
 * @param {number} studentId 
 * @param {number} lessonId 
 * @returns {Promise<Enrolment>} enrolment
 */
function getEnrolment(studentId, lessonId) {
  return new Promise((resolve, reject) => {
    Enrolment.findOne({
      where: {
        studentId: studentId,
        lessonId: lessonId
      },
      include: [{
          model: Student
        },
        {
          model: Lesson
        }
      ]
    }).then((enrolment) => {
      resolve(enrolment);
    }).catch((err) => {
      reject(err);
    });
  });
}
/**
 * @description Method to get the enrolments of a given student
 * @param {number} studentId 
 * @returns {Promise<Enrolment[]>} enrolments
 */
function getEnrolmentsByStudent(studentId) {
  return new Promise((resolve, reject) => {
    Enrolment.findAll({
      where: {
        studentId: studentId
      },
      include: [{
        model: Lesson
      }, {
        model: Student
      }]
    }).then((enrolments) => {
      resolve(enrolments);
    }).catch((err) => {
      reject(err);
    });
  });
}

/**
 * @description Method to get the enrolments in a given lesson
 * @param {number} lessonId 
 * @returns {Promise<Enrolment[]>} enrolments
 */
function getEnrolmentsByLesson(lessonId) {
  return new Promise((resolve, reject) => {
    Enrolment.findAll({
      where: {
        lessonId: lessonId
      },
      include: [{
        model: Lesson
      }, {
        model: Student
      }]
    }).then((enrolments) => {
      resolve(enrolments);
    }).catch((err) => {
      reject(err);
    });
  });
}

/**
 * @description Method to get all active enrolments
 * @returns {Promise<Enrolment[]>} enrolments
 */
function getActiveEnrolments() {
  return new Promise((resolve, reject) => {
    Enrolment.findAll({
      where: {
        active: true
      },
      include: [{
        model: Lesson
      }, {
        model: Student
      }]
    }).then((enrolments) => {
      resolve(enrolments);
    }).catch((err) => {
      reject(err);
    });
  });
}

/**
 * @description Method to get all inactive enrolments
 * @returns {Promise<Enrolment[]>} enrolments
 */
function getInactiveEnrolments() {
  return new Promise((resolve, reject) => {
    Enrolment.findAll({
      where: {
        active: false
      },
      include: [{
        model: Lesson
      }, {
        model: Student
      }]
    }).then((enrolments) => {
      resolve(enrolments);
    }).catch((err) => {
      reject(err);
    });
  });
}

/**
 * @description Method to get all enrolments
 * @returns {Promise<[Enrolment]>} enrolments
 */
function getAllEnrolments() {
  return new Promise((resolve, reject) => {
    Enrolment.findAll({
      include: [{
        model: Lesson
      }, {
        model: Student
      }]
    }).then((enrolments) => {
      resolve(enrolments);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}

/**
 * @description Method to create a new enrolment
 * @param {Enrolment} data 
 * @returns {Promise<Enrolment>} enrolment
 */
function createEnrolment(data) {
  return new Promise((resolve, reject) => {
    Enrolment.create(data).then((enrolment) => {
      resolve(enrolment);
    }).catch((err) => {
      reject("Error: You are already enrolled for this lesson.");
    })
  });
}



/**
 * @description Method to update an enrolment
 * @param {number} studentId 
 * @param {number} lessonId
 * @param {any} data
 * @returns {any}
 */
function updateEnrolment(studentId, lessonId, data) {
  return new Promise((resolve, reject) => {
    Enrolment.findOne({
      where: {
        studentId: studentId,
        lessonId: lessonId
      }
    }).then((enrolment) => {
      return enrolment.update(data);
    }).then((affectedRows) => {
      resolve(affectedRows);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}


/**
 * @description Method that activates or deactivates (toggles) the enrolment
 * @param {number} studentId 
 * @param {number} lessonId 
 * @returns {any} data
 */
function activateOrDeactivate(studentId, lessonId) {
  return new Promise((resolve, reject) => {
    Enrolment.findOne({
      where: {
        studentId: studentId,
        lessonId: lessonId
      }
    }).then((enrolment) => {
      return enrolment.update({
        active: !enrolment.active
      });
    }).then((data) => {
      resolve(data);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}

/**
 * @description Method to delete an enrolment
 * @param {number} studentId 
 * @param {number} lessonId 
 * @returns {number} affectedRows
 */
function deleteEnrolment(studentId, lessonId) {
  return new Promise((resolve, reject) => {
    let enrolmentObj = null;
    Enrolment.findOne({
      where: {
        studentId: studentId,
        lessonId: lessonId
      }
    }).then((enrolment) => {
      enrolmentObj = enrolment;
      return enrolment.update({
        active: false
      });
    }).then((data) => {
      return enrolmentObj.destroy();
    }).then((affectedRows) => {
      resolve(affectedRows);
    }).catch((err) => {
      console.error(err);
      reject(err);
    });
  });
}

module.exports.getEnrolment = getEnrolment;
module.exports.getActiveEnrolments = getActiveEnrolments;
module.exports.getInactiveEnrolments = getInactiveEnrolments;
module.exports.getAllEnrolments = getAllEnrolments;
module.exports.getEnrolmentsByLesson = getEnrolmentsByLesson;
module.exports.getEnrolmentsByStudent = getEnrolmentsByStudent;
module.exports.createEnrolment = createEnrolment;
module.exports.updateEnrolment = updateEnrolment;
module.exports.activateOrDeactivate = activateOrDeactivate;
module.exports.deleteEnrolment = deleteEnrolment;