'use strict';

class Enrolment {
    constructor(clientId,lessonId,enrolmentDate,active,subscriptionId) {
      this.clientId = Number(clientId);
      this.lessonId = Number(lessonId);
      this.enrolmentDate = new Date(enrolmentDate).toISOString();
      this.active = Boolean(active);
      this.subscriptionId = Number(subscriptionId);
    }
}

module.exports = Enrolment;