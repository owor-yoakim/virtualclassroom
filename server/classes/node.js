class Node {
    constructor(data) {
        this.data = data;
        this.parent = null;
        this.left = null;
        this.right = null;
    }

    insertNode(node) {
        if (node.data.student.id < this.data.student.id) {
            if (this.left === null) {
                node.parent = this;
                node.data.stream = this.data.stream;
                this.left = node;
                return node;
            }
            return this.left.insertNode(node);
        } else {
            if (this.right === null) {
                node.parent = this;
                node.data.stream = this.data.stream;
                this.right = node;
                return node;
            }
            return this.right.insertNode(node);
        }
    }

    searchNode(data) {
        if (this.data.student.id === data.student.id) {
            return this;
        }
        if (data.student.id < this.data.student.id) {
            return this.left.searchNode(data);
        }
        if (data.student.id > this.data.student.id) {
            return this.right.searchNode(data);
        }
        return null;
    }
}

class ClassroomSessionTree {
    constructor(sessionId) {
        // unique id of this session
        this.sessionId = sessionId;
        // session moderator
        this.moderator = null;
        // hash map for carrying nodes in this session
        this.nodes = {}; 
        // start time of this session
        this.startTime = Date.now();
    }

    addNode(data) {
        var node = new Node(data);
        if (this.moderator === null) {
            this.moderator = node;
        } else if(this.moderator.left === null) {
            this.moderator.left = node;
            this.moderator.left.data.stream = this.moderator.data.stream;
            this.nodes[node.data.student.username] = this.moderator.left;
        } else if (this.moderator.right === null) {
            this.moderator.right = node;
            this.moderator.right.data.stream = this.moderator.data.stream;
            this.nodes[node.data.student.username] = this.moderator.right;
        } else {
            node = this.moderator.insertNode(node);
            this.nodes[node.data.student.username] = node;
        }
    }

    removeNode(node) {
        
    }

    searchNode(data) {
        var node = null;
        if (this.moderator.left !== null) {
            node = this.moderator.left.searchNode(data);
            if (node !== null) {
                return node;
            }
        }
        
        if (this.moderator.right !== null) {
            node = this.moderator.right.searchNode(data);
        }
        return node;
    }
}

// testing

var data1 = {
    stream: new MediaStream(),
    student: {
        id: Math.floor((Math.random() * 1000000) + 1),
        username: Date.now().toString()
    },
    lesson: {
        id: Math.floor((Math.random() * 1000000) + 1),
        slug: 'mathematics'
    }
}

var data2 = {
    stream: new MediaStream(),
    student: {
        id: Math.floor((Math.random() * 1000000) + 1),
        username: Date.now().toString()
    },
    lesson: {
        id: Math.floor((Math.random() * 1000000) + 1),
        slug: 'mathematics'
    }
}

var session = new ClassroomSessionTree('mathematics');
console.log(session);
session.addNode(data1);
console.log(session);
session.addNode(data2);
console.log(session);

