var express = require('express');
var router = express.Router();

const restrictStudent = require('../config/restrict').restrictStudent;
const restrictInstructor = require('../config/restrict').restrictInstructor;

const StudentController = require('../controllers/StudentController');
const AccountController = require('../controllers/AccountController');
const SubjectController = require('../controllers/SubjectController');
const LessonController = require('../controllers/LessonController');
const LessonModuleController = require('../controllers/LessonModuleController');
const PageController = require('../controllers/PageController');
const InstructorController = require('../controllers/InstructorController');
const UserMessageController = require('../controllers/UserMessageController');
const CountryController = require('../controllers/CountryController');
const ChatController = require('../controllers/ChatController');
const SettingController = require('../controllers/SettingController');
const EnrolmentController = require('../controllers/EnrolmentController');
const TestController = require('../controllers/TestController');

//BaseURL = ['/site/api']

//test route
router.get('/test', TestController.apiTest);

/*  Account session management routes  */
router.post('/login', AccountController.login);
router.post('/instructorLogin', InstructorController.login);
router.post('/logout', AccountController.logout);
router.post('/checkSession', AccountController.checkSession);

router.get('/account/student/getById/:studentId', restrictStudent, StudentController.getById);
router.get('/account/instructor/getById/:instructorId', restrictInstructor, InstructorController.getById);

/*  Account Maintenance routes  */
router.post('/register', AccountController.register);
router.post('/resetPassword', AccountController.resetPassword);
router.put('/updateProfile', restrictStudent, AccountController.updateProfile);
router.put('/account/student/changePassword', restrictStudent, AccountController.changeStudentPassword);
router.put('/account/instructor/changePassword', restrictInstructor, AccountController.changeInstructorPassword);

/*  Pages route   */
router.get('/page/:pageSlug', PageController.getPageContent);

/*   enrolment routes       */
router.get('/enrolment/all', EnrolmentController.getAllEnrolments);
router.post('/enrolment/create', EnrolmentController.createEnrolment);
router.get('/enrolment/getByStudent/:studentId', EnrolmentController.getEnrolmentsByStudent);
router.get('/enrolment/getByLesson/:lessonId', EnrolmentController.getEnrolmentsByLesson);

/*  Subject routes  */
router.get('/subject/all', SubjectController.getAll);
router.get('/subject/getById/:id', SubjectController.getById);
router.get('/subject/getBySlug/:slug', SubjectController.getBySlug);
router.get('/subject/count', SubjectController.numberOfSubjects);
router.get('/subject/getInstructorsBySubjectSlug/:subjectSlug', SubjectController.getInstructorsBySubjectSlug);

/*  Lesson routes  */
router.get('/lesson/all', LessonController.getAll);
router.get('/lesson/getById/:id', LessonController.getById);
router.get('/lesson/getBySlug/:slug', LessonController.getBySlug);
router.get('/lesson/getBySubject/:subjectId', LessonController.getBySubject);
router.get('/lesson/count', LessonController.numberOfLessons);
router.get('/lesson/countBySubject/:subjectId', LessonController.countBySubject);
router.get('/lesson/getPrerequisites/:lessonId', LessonController.getPrerequisites);
router.get('/lesson/:lessonId/module/all', LessonController.getLessonModules);
router.get('/lesson/getLessonInstructors/:lessonId', LessonController.getLessonInstructors);
router.post('/lesson/upload', restrictStudent, LessonController.uploadFile);
router.put('/lesson/update/:lessonId', restrictInstructor, LessonController.updateInstructorLesson);

// lesson modules
router.get('/lesson/:lessonId/module/all', LessonController.getLessonModules);
router.get('/lesson/:lessonId/module/:lessonModuleId', LessonModuleController.getById);
router.get('/lesson/:lessonId/module/:lessonModuleId/comment/all', LessonController.getLessonModuleComments);


// lesson comments
router.get('/lesson/:lessonId/comment/all', LessonController.getLessonComments);
router.get('/lesson/:lessonId/comment/:commentId', LessonController.getLessonComment);
router.post('/lesson/:lessonId/comment/create', LessonController.createLessonComment);


/*  instructor routes  */
router.get('/instructor/all', InstructorController.getAll);
router.get('/instructor/getById/:instructorId', InstructorController.getById);
router.get('/instructor/getLessons/:instructorId', InstructorController.getLessons);
router.get('/instructor/getSchedules/:instructorId', InstructorController.getSchedules);
router.get('/instructor/getInstructorsBySubjectSlug/:subjectSlug', InstructorController.getInstructorsBySubjectSlug);


/*   user-messge routes   */
router.get('/user-message/:id', restrictStudent, UserMessageController.getById);
router.get('/user-message/getBySenderId/:senderId', restrictStudent, UserMessageController.getBySenderId);
router.get('/user-message/getBySenderEmail/:senderEmail', restrictStudent, UserMessageController.getBySenderEmail);
router.get('/user-message/getByReceiverId/:receiverId', restrictStudent, UserMessageController.getByReceiverId);
router.get('/user-message/getByReceiverEmail/:receiverEmail', restrictStudent, UserMessageController.getByReceiverEmail);
router.post('/user-message', restrictStudent, UserMessageController.createUserMessage);
router.put('/user-message/:id', restrictStudent, UserMessageController.updateUserMessage);

//countries routes
router.get('/country/all', CountryController.getAll);

/* chat routes  */
//get all chats
router.get('/chat/all', ChatController.getAllChats);
router.post('/chat/upload', restrictStudent, ChatController.uploadFile);
//create a new chat
router.post('/chat/create', ChatController.createChat);
//get a chat with id as chatId
router.get('/chat/getById/:chatId', ChatController.getChatById);
//get all the chats for the specified lesson
router.get('/chat/getByLesson/:lessonId', ChatController.getLessonChats);

/*  Settings routes */
//get all settings
router.get('/setting', SettingController.getAll);
//create a new setting
router.post('/setting', SettingController.create);
//get a setting with id as id
router.get('/setting/getById/:id', SettingController.getById);
//get a setting with key as key
router.get('/setting/getByKey/:key', SettingController.getByKey);
//update the setting info of the setting with id as id
router.put('/setting/:id', SettingController.update);


//Export the routes
module.exports = router;