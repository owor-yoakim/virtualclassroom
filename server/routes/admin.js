var express = require('express');
var router = express.Router();
const restrict = require('../config/restrict').restrictAdmin;

const AdminController = require('../controllers/AdminController');
const StudentController = require('../controllers/StudentController');
const SubjectController = require('../controllers/SubjectController');
const LessonController = require('../controllers/LessonController');
const EnrolmentController = require('../controllers/EnrolmentController');
const PageController = require('../controllers/PageController');
const UserController = require('../controllers/UserController');
const RoleController = require('../controllers/RoleController');
const PermissionController = require('../controllers/PermissionController');
const InstructorController = require('../controllers/InstructorController');
const CountryController = require('../controllers/CountryController');
const LessonModuleController = require('../controllers/LessonModuleController');


//BaseURL = ['/admin/api']

//Account routes
router.post('/login', AdminController.login);

router.post('/logout', AdminController.logout);

router.post('/checkSession', AdminController.checkSession);

router.post('/changePassword', restrict, AdminController.changePassword);

router.post('/resetPassword', AdminController.resetPassword);

//Users Routes
router.get('/user/all', UserController.getAll);
router.get('/user/getByRole/:roleId', UserController.getByRole);
router.get('/user/getById/:userId', UserController.getById);
router.get('/user/getByEmail/:email', UserController.getByEmail);
router.post('/user/create', UserController.createUser);
router.put('/user/update/:userId', UserController.updateUser);
router.delete('/user/delete/:userId', UserController.deleteUser);
router.put('/user/activateOrDeactivate/:userId', UserController.activateOrDeactivateUser);


//Roles Routes
router.get('/role/all', RoleController.getAll);
router.get('/role/getById/:roleId', RoleController.getById);
router.get('/role/getBySlug/:slug', RoleController.getBySlug);
router.post('/role/create', RoleController.createRole);
router.put('/role/update/:roleId', RoleController.updateRole);
router.delete('/role/delete/:roleId', RoleController.deleteRole);


//Permissions Routes
router.get('/permission/all', restrict, PermissionController.getAll);
router.get('/permission/getById/:permissionId', restrict, PermissionController.getById);
router.get('/permission/getBySlug/:slug', restrict, PermissionController.getBySlug);
router.post('/permission/create', restrict, PermissionController.createPermission);
router.put('/permission/update/:permissionId', restrict, PermissionController.updatePermission);
router.delete('/permission/delete/:permissionId', restrict, PermissionController.deletePermission);

//Subject Routes
router.get('/subject/all', SubjectController.getAll);
router.get('/subject/getById/:id', SubjectController.getById);
router.get('/subject/getBySlug/:slug', SubjectController.getBySlug);
router.post('/subject/create', SubjectController.create);
router.put('/subject/update/:id', SubjectController.update);
router.post('/subject/activate/:id', SubjectController.activate);
router.post('/subject/deactivate/:id', SubjectController.deactivate);
router.delete('/subject/delete/:id', SubjectController.delete);
router.get('/subject/count', SubjectController.numberOfSubjects);

//Lesson Routes
router.get('/lesson/all', LessonController.getAll);
router.get('/lesson/getById/:id', LessonController.getById);
router.get('/lesson/getBySlug/:slug', restrict, LessonController.getBySlug);
router.get('/lesson/getBySubject/:subjectId', restrict, LessonController.getBySubject);
router.post('/lesson/create', LessonController.create);
router.put('/lesson/update/:id', restrict, LessonController.update);
router.post('/lesson/delete/:id', restrict, LessonController.delete);
router.post('/lesson/activate/:id', restrict, LessonController.activate);
router.post('/lesson/deactivate/:id', restrict, LessonController.deactivate);
router.get('/lesson/count', restrict, LessonController.numberOfLessons);
router.get('/lesson/countBySubject/:subjectId', restrict, LessonController.countBySubject);

//lesson modules
router.get('/lesson/:lessonId/module/all', LessonModuleController.getByLesson);
router.get('/lesson/:lessonId/module/:lessonModuleId', LessonModuleController.getById);
router.post('/lesson/:lessonId/module/create', restrict, LessonModuleController.create);
router.put('/lesson/:lessonId/module/update/:lessonModuleId', restrict, LessonModuleController.update);
router.delete('/lesson/:lessonId/module/delete/:lessonModuleId', restrict, LessonModuleController.delete);

// lesson comments
router.get('/lesson/:lessonId/comment/all', LessonController.getLessonComments);
router.get('/lesson/:lessonId/module/:lessonModuleId/comment/all', LessonController.getLessonModuleComments);
router.get('/lesson/:lessonId/comment/:commentId', LessonController.getLessonComment);
router.post('/lesson/:lessonId/comment/create', LessonController.createLessonComment);
router.put('/lesson/:lessonId/comment/update/:commentId', LessonController.updateLessonComment);
router.delete('/lesson/:lessonId/comment/delete/:commentId', restrict, LessonController.deleteLessonComment);

//Enrolment Routes
router.get('/enrolment/all', EnrolmentController.getAllEnrolments);
router.get('/enrolment/getOne/:studentId/:lessonId', restrict, EnrolmentController.getEnrolment);
router.get('/enrolment/getByStudent/:studentId', restrict, EnrolmentController.getEnrolmentsByStudent);
router.get('/enrolment/getByLesson/:lessonId', restrict, EnrolmentController.getEnrolmentsByLesson);
router.post('/enrolment/create', restrict, EnrolmentController.createEnrolment);
router.put('/enrolment/update/:studentId/:lessonId', restrict, EnrolmentController.updateEnrolment);
router.delete('/enrolment/delete/:studentId/:lessonId', restrict, EnrolmentController.deleteEnrolment);
router.put('/enrolment/activateOrDeactivate/:studentId/:lessonId', restrict, EnrolmentController.activateOrDeactivate);

//student routes
router.get('/student/all', StudentController.getAllStudents);
router.get('/student/getById/:studentId', restrict, StudentController.getById);
router.post('/student/create', restrict, StudentController.createStudent);
router.put('/student/update/:studentId', restrict, StudentController.updateStudent);
router.put('/student/activateOrDeactivate/:studentId', restrict, StudentController.activateOrDeactivateStudent);
router.delete('/student/delete/:studentId', restrict, StudentController.deleteStudent);

//instructor routes
router.get('/instructor/all', restrict, InstructorController.getAll);
router.get('/instructor/getById/:instructorId', restrict, InstructorController.getById);
router.post('/instructor/create', restrict, InstructorController.createInstructor);
router.put('/instructor/update/:instructorId', restrict, InstructorController.updateInstructor);
router.put('/instructor/:instructorId/assignLesson/:lessonId', restrict, InstructorController.assignLesson);
router.put('/instructor/:instructorId/removeLesson/:lessonId', restrict, InstructorController.removeLesson);
router.put('/instructor/activateOrDeactivate/:instructorId', restrict, InstructorController.activateOrDeactivateInstructor);
router.delete('/instructor/delete/:instructorId', restrict, InstructorController.deleteInstructor);

//pages routes
router.post('/page', restrict, PageController.create);
router.put('/page/:id', restrict, PageController.update);
router.put('/page/:id/publish', restrict, PageController.publish);
router.put('/page/:id/unpublish', restrict, PageController.unpublish);
router.delete('/page/id/delete', restrict, PageController.deletePage);

//countries routes
router.get('/country/all', CountryController.getAll);
router.get('/country/getById/:countryId', CountryController.getById);
router.get('/country/getByCode/:code', CountryController.getByCode);


module.exports = router;